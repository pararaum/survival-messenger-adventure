# Survival Messenger Adventure #

Game engine and a survival style adventure. This is based on the
visual-novel game "Survival Messenger", have a look at
<https://csdb.dk/release/?id=190583>. In this game you wake up with no
knowledge of prior events and have to figure out what happened and how
to escape the island.

On the top of the screen some nice graphics will show you the game
environment, while on the bottom you can interact with the game
world. From time to time your phone (after you got it working) will
beep and provide you another means of communications. Who are these
people? Can you figure it out, too?

## From Survial Messenger to the Adventure ##

Once upon time there was [Survival
Messenger](https://csdb.dk/release/?id=190304), a game in which was
released on the 7th Klagenfurt Game Jam. Some people liked the idea
and joined forces to create a more sophisticated version of this
game. The style chosen was a graphics adventure with a mobile-phone
window.

Development began in Mai 2020 and continued until October 2020.

## Assembly Release ##

The game was released on the 12th october 2020 at the [Assembly
Online](https://www.assembly.org/online20/ "Assembly Online 2020"). A
promotional video is also available
[here.](https://archive.assembly.org/2020/online-gamedev/survival-messenger-adventure-by-the-7th-division-and-vintage-computing-carinthia). If you like you can [watch the prizegiving ceremony.](https://www.twitch.tv/videos/775131357?t=0h53m15s).

# Building #

## Prerequisites ##

The makefiles are prepared for cross development. You will need the
following packages installed on your machine:

 * cc65 [cc65 on github](https://cc65.github.io/ "cc65")
 * lz4 [LZ4 on github](http://lz4.github.io/lz4/ "LZ4")
 * VICE [vice on sourceforge](https://vice-emu.sourceforge.io/ "VICE")

On a Debian-based system a `sudo apt install cc65 vice lz4` will do.

## Compiling ##

There are two different versions you can compile. One is the native
version (without the graphics or the mobile-phone window) which can be
used to explore/debug the adventure immediately and the C64 version
which is the release version.

### C64 version ###

The C64 version is build by issuing:

	make c64

The disk image is produced in the "c64" directory. It can be played in
an emulator by calling `x64 c64/adventure.d64`.

There seems to be a problem with the lz4 compressor and the
decompressor stub in some versions. We are looking into this issue but
version r128 by Yann Collet from 2016 seems to work.

### Native version ###

The native version can be compiled with a simple command:

	make

The adventure can be run using the `./adventure` command.

### Updating graphics ###

There are 50 graphics in the game, which are stored in the `pictures` directory.
Pictures there are stored in Koala format, which can be viewed and edited with 
many programs, for example, Koala Paint on C64 or with Multipaint on Linux or PC.
Note that only a part of the image is used in the game, this area is cut out automatically.

To prepare the pictures for the game, call the Windows batch script `makepics.bat` in directory
`piccompressor`. The script lists all files after the process because the load buffer was
set to a maximum of 9 blocks, so no file should be larger than 2277 bytes. Unless your
Koala pic contains complete random structures, color optimization and the exomizer compression
should be able to keep the files under that limit.

After picture conversion, the gfx library has to rebuild, go into directory `graphicroutines`
and call the batch script `makegfxtools.bat`. After this, go up one directory level and call
`make c64` again to build your executable with the new graphics.