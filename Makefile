CFLAGS = -Wall -Wextra -O3 -DNDEBUG -g
VPATH = src
OBJS = gamedata.o engine.o data_saveload.o data_structures.o qcode.o default_actions.o gamedata.objects.o gamedata.messages.o gamedata.locations.o
C64OBJS = c64_irqmoiety.o general_io_c64.o gfxtools.o c64_muzak.o loaderunderio.o c64_mobilemoiety.o c64_gamedatamoiety.o c64_toolfunctions.o
MCONF = survival.cfg

adventure: $(OBJS) general_io.o
	$(CC) $(CFLAGS) -o $@ $+

intro:	intro.o intro.main.o intro.muzak.o intro.initialise_bitmap_screen.o intro.gfx_data.o intro.scroller.o crc8.o
	cl65 -v -m $@.map -Ln $@.label -C intro.cfg $+
#	../pucrunch intro intro.prg
#	../exomizer sfx systrim -o intro.prg intro
	cat copy_n_uncompress.stub > $@.prg
	lz4 -BD -9 < $@ >> $@.prg
#	(cat copy_n_uncompress.stub; lz4 -BD -9 < $@) > $@.prg

sfxplayer.prg:	sfxplayer
	./pucrunch $+ $@

sfxplayer:	sfxplayer.o c64_muzak.o
	cl65 -v -m $@.map -Ln $@.label -C sfxplayer.cfg $+

.PHONY: c64 wilstest
c64:
	test -d c64 || mkdir c64
	cp -al *.stub *.sid *.bin *.cfg src/gfxtools.prg Makefile src c64
	cp pictures.d64 c64/adventure.d64
	cp pictures/PararaumsPhotos/cathedral_cove.kla c64
	$(MAKE) -C c64 AS=ca65 CC=cl65 CFLAGS="-O -DNDEBUG" adventure.prg
	$(MAKE) -C c64 AS=ca65 CC=cl65 CFLAGS="-O -DNDEBUG" intro
	$(MAKE) -C instructions all
	c1541 -attach c64/adventure.d64 -write c64/intro.prg adventure
	c1541 -attach c64/adventure.d64 -write instructions/instructions.prg

adventure.lz4:	$(OBJS) $(C64OBJS)
	$(CC) -C $(MCONF) -o adventure -m adventure.map -Ln adventure.label $+
	lz4 -BD -9 < adventure > adventure.lz4

adventure.prg:	adventure.lz4
#	cat copy_n_uncompress.stub > adventure.prg
#	lz4 -BD -9 < adventure >> adventure.prg
#	cat copy_n_uncompress.stub adventure.lz4 > adventure.prg
	exomizer sfx systrim -B -Di_perf=-1 -P16 -n adventure -o adventure.prg
#	../pucrunch adventure adventure.prg

.PHONY: clean distclean doc
clean:
	rm -f sfxplayer sfxplayer.prg
	rm -f adventure *.o *.label *.map
	rm -rf c64
	rm -rf intro intro.prg
	$(MAKE) -C instructions clean

distclean:
	$(MAKE) clean
	rm -fr doc/*

doc:
	doxygen
