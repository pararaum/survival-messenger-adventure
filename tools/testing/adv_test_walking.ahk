; win + t

FileRead, str, directions_walk_test.txt
testcommands := StrSplit(str, "`n")

#t::
  stop =0 
  SetKeyDelay, 100
  For i,e In testcommands
  {
     typeString(e)
     Sleep, 5000
     if stop = 1 
       Break
  }
  typeString("End of test")
  Return

Esc::stop=1
  Return

typeString(str)
{
  Loop, Parse, str 
  {
    send {%A_LoopField% down}
    Sleep,100
    send {%A_LoopField% up}
  }
  Return
}

  Loop, Parse, Word