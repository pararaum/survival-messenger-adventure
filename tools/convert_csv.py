#! /usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Convert a CSV file to C-source, as needed for the definition of the
locations.
"""
# Format is
# map name	Room #	long description	short description	E	N	W	S


import sys
import csv
import openpyxl
import packstring

def rdch(s):
    """Replace characters which are non petscii and could be dangerous

    Please make a law against all this unicode crap!

    @param s: string to be sanitised
    @return: sanitised string

    """
    s = s.replace('…', "...")
    s = s.replace('’', "'")
    return s

class Converter(object):
    def __init__(self, fnam):
        """Initialise

        @param fnam file name of workbook
        """
        self.workbook = openpyxl.load_workbook(open(fnam, "rb"), data_only=True)
        self.worksheet = None
        self.minrow = 1
        self.maxrow = None

    def compress_str(self, txt):
        ctxt = packstring.compress(txt)
        ctxts = packstring.bitarr2str(ctxt)
        return ctxts

    def _adjust_minmaxrows(self):
        self.minrow = self.worksheet.min_row
        self.maxrow = self.worksheet.max_row

    def _convert(self, row):
        raise NotImplementedError("convert()")

    def get(self, row, col):
        val = self.worksheet.cell(row=row, column=col).value
        try:
            val = rdch(val)
        except (AttributeError, ValueError):
            pass
        return val

    def convert_all(self):
        res = []
        for row in range(self.minrow, self.maxrow + 1):
            res.append(self._convert(row))
        return '\n'.join(res)


class ConvertLocations(Converter):
    def __init__(self, fnam):
        Converter.__init__(self, fnam)
        self.worksheet = self.workbook["Rooms"]
        self._adjust_minmaxrows()
        self.minrow = 3
        self.descriptions = []
        
    def _convert(self, row):
        desc = self.get(row, 4)
        if desc is not None:
            # compressed description
            cdesc = self.compress_str(desc)
            self.descriptions.append((row, cdesc))
            desctxt = "description_%d" % row
        else:
            desctxt = "NULL"
        return format("""{ /* %s %s */ %s, %s /*%s*/, "%s", { %s, %s, %s, %s } },""") % (self.get(row, 1), self.get(row, 3), self.get(row, 2), desctxt, desc, self.get(row, 5), self.get(row, 11), self.get(row, 12), self.get(row, 13), self.get(row, 14))

    def convert_all(self):
        ret = Converter.convert_all(self)
        ret2 = '\n'.join("static const uint8_t description_%d[] = { %s };" % (i, e) for i, e in self.descriptions)
        return ret2 + '\n\n' + ret
        
        

class ConvertObjects(Converter):
    def __init__(self, fnam):
        Converter.__init__(self, fnam)
        self.worksheet = self.workbook["Objects"]
        self._adjust_minmaxrows()
        self.minrow = 3
        self.descriptions = []

    def _convert(self, row):
        desc = self.get(row, 6)
        if desc is not None:
            # compressed description
            cdesc = self.compress_str(desc)
            self.descriptions.append((row, cdesc))
            desctxt = "objdescription_%d" % row
        else:
            desctxt = "NULL"
        return format("""{ /* %s */ "%s", %s, %s, %s, %s },""") % (self.get(row, 1), self.get(row, 2), self.get(row, 3), self.get(row, 5), desctxt, self.get(row, 7))
    def convert_all(self):
        ret = Converter.convert_all(self)
        ret2 = '\n'.join("static const uint8_t objdescription_%d[] = { %s };" % (i, e) for i, e in self.descriptions)
        return ret2 + '\n\n' + ret


class ConvertMessages(Converter):
    def __init__(self, fnam):
        Converter.__init__(self, fnam)
        self.worksheet = self.workbook["Messages"]
        self._adjust_minmaxrows()
        self.minrow = 2
        self.msgnames = []

    def _convert(self, row):
        """Convert messages
        
        Convert from csv to C code.
        
        @param arr of lines
        """
        # message id
        mid = int(self.get(row, 1))
        txt = self.get(row, 2)
        ctxt = packstring.compress(txt)
        ctxts = packstring.bitarr2str(ctxt)
        mname = "message_%03d" % mid
        self.msgnames.append(mname)
        return format("""static const uint8_t %s[] /* %5.3f:1 %s */ = { %s };""") % (mname, len(txt)*8.0/float(len(ctxt)), txt, ctxts)

    def convert_all(self):
        ret = Converter.convert_all(self)
        ret2 = ", ".join(self.msgnames)
        return "%s\nconst uint8_t *messages[] = { %s };" % (ret, ret2)


def main(argv):
    """Main function.

    @param argv: arguments from command line
    """
    mode = argv[1]
    fname = argv[2]
    if mode.startswith("loc"):
        convf = ConvertLocations(fname)
    elif mode.startswith("obj"):
        convf = ConvertObjects(fname)
    elif mode.startswith("mes"):
        convf = ConvertMessages(fname)
    else:
        print("Mode '%s' unknown." % mode, file=sys.stderr)
        sys.exit(5)
    try:
        inpf = open(fname)
    except IndexError:
        inpf = sys.stdin
    print(convf.convert_all())

if __name__ == "__main__":
    main(sys.argv)
