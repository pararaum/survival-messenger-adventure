@echo off
setlocal ENABLEDELAYEDEXPANSION

rem this script creates 
rem - a .d64 disk image with all game graphics
rem - a file gfxtools of around 3.3K which is located at address $BE00 
rem - two test programs on a copy of the graphics disk
rem
rem Aug-2020 V0.4
rem Wilfried Elmenreich
rem Code under CC-BY-4.0 license

rem Define if we are going to use the Track/Sector Loading (1) or not (0)
rem This must be set consistentyl with the value in file constants.inc
rem When LOAD_BY_TS is off, there will be no dirart
set LOAD_BY_TS=0

echo ;-- this file is generated, change only makegfxtools.bat -- > loader_type.inc
echo LOAD_BY_TS=!LOAD_BY_TS! >> loader_type.inc

set c1541="C:\Users\Wilfried\Documents\software\commodore\WinVICE-3.2-x86-r34842\c1541"
set d64tool="..\tools\d64tool"

if not exist %d64tool%.exe (
  wget -O %d64tool%.exe https://csdb.dk/release/download.php?id=247519
)

echo -- making new disk image for graphics --------------------------
%c1541% -format adventure,id d64 "graphics-files.d64"
if "!LOAD_BY_TS!"=="0" (
  c1541 -attach "graphics-files.d64" -write loader_type.inc plotzholta1
  c1541 -attach "graphics-files.d64" -write loader_type.inc plotzholta2
)

echo -- generate prgs if necessary ----------------------------------
if not exist "..\piccompressor\croppedpics\01.prg" (
  pushd "..\piccompressor"
  call makepics.bat
  popd
)

echo -- copying compressed images to disk ---------------------------
for %%i in ("..\piccompressor\croppedpics\*.prg") do (
  echo | set /p dummyName=%%~nxi,
  rem echo %%~ni
  %c1541% -attach "graphics-files.d64" -write "%%i" "%%~ni" > nul
)

if "!LOAD_BY_TS!"=="0" (
  c1541 -attach "graphics-files.d64" -delete plotzholta1
  c1541 -attach "graphics-files.d64" -delete plotzholta2
)


rem 
echo.
if "!LOAD_BY_TS!"=="1" (
echo -- ghosting images ---------------------------------------------
echo exporting track/sector list
%d64tool% -e graphics-files.d64 > tracksectors.inc
echo removing directory entries
%d64tool% -rd 0 143 graphics-files.d64 pictures.d64 > nul
echo -- adding dirart -----------------------------------------------
%c1541% -attach "pictures.d64" -bwrite dirart\1801.blk 18 1
%c1541% -attach "pictures.d64" -bwrite dirart\1804.blk 18 4
%c1541% -attach "pictures.d64" -bwrite dirart\1807.blk 18 7
%c1541% -attach "pictures.d64" -bwrite dirart\1810.blk 18 10
%c1541% -attach "pictures.d64" -bwrite dirart\1813.blk 18 13
) else (
  copy graphics-files.d64 pictures.d64
)

echo -- building gfx tools ------------------------------------------
cl65 -C c64-asm.cfg -o gfxtools.prg --start-addr $BE00 -Ln gfxtools-labels.txt  jumptable.s graphicinit.s scrolldown.s scrolldown_text.s print2bmp3.s drawmobilephone.s input.s decodeandshowimage.s loaderhandler.s exostreamdecr1.s show_image.s irq-catcher.s LAMAlib.lib 
if errorlevel 1 (
  echo BUILD FAILED.
  exit /b %errorlevel%
)
echo assembling gfxtools.prg done

echo -- building test files -----------------------------------------
cl65 -t c64 test_gfx_lib.c -o test_gfx_lib.prg -C c64-compact.cfg --mapfile test_gfx_lib.map
cl65 -t c64 test_fileio.c -o test_fileio.prg -C c64-compact.cfg --mapfile test_fileio.map

@rem exomizer sfx basic -B test_gfx_lib.prg ..\piccompressor\imgviewer.prg gfxtools.prg -o test.prg

@rem sfx 58235
exomizer sfx sys -B test_gfx_lib.prg dloadAE00.prg,$D000 gfxtools.prg -o test.prg

exomizer sfx sys -B test_fileio.prg dloadAE00.prg,$D000 gfxtools.prg -o testio.prg

echo -- making test disk --------------------------------------------
cp pictures.d64 advpictest.d64

%c1541% -attach "advpictest.d64" -write "test.prg" "test"
%c1541% -attach "advpictest.d64" -write "testio.prg" "testio"

if "%~1"=="" goto noargument
if "%~1"=="install" (
 echo -- copying pictures.d64 and gfxtools to game build -------------
 copy pictures.d64 ..
 copy gfxtools.prg ..\src
)

:noargument
echo -- cleaning up -------------------------------------------------
rm test_gfx_lib.prg test_fileio.prg *.o

echo -- done --------------------------------------------------------
