; ------------------------------------------------------------------------
; May-2020 V0.21
; Wilfried Elmenreich
; Code under CC-BY-4.0 license
;
; Given the address of a compressed pic in AX, this routine
; decrunches it into a multicolor bitmap image of given width and height
; X0 and Y0 is the position of the image on the screen in characters
; WIDTH and HEIGHT are the dimensions of the image in characters
;
; Compressed images are to be created using the cropkoala tool
; and exomizer with options "raw -P0 -B -c -m 1024"
;
; This file is intended to be assembled and linked with the cc65 toolchain.
; It needs to be linked with exostreamdecr1.s that comes with exomizer
; ------------------------------------------------------------------------

.import init_decruncher
.import get_decrunched_byte
.export get_crunched_byte

.export buffer_start_hi:	absolute
.export buffer_len_hi:		absolute

.export decodeandshowimage

.import jumptable_lo,jumptable_hi


; ----- configuration -----
X0 = 0
Y0 = 0
WIDTH = 26
HEIGHT = 14
BLANKOUT = 1

.include "constants.inc"
; --- end configuration ---

imagelen= WIDTH * HEIGHT * 10    ;8 bytes and 2 color bytes for each cursor block

basebmptr=bitmap+Y0*320+X0*8

basescrptr=screen+Y0*40+X0

basecolptr=$d800+Y0*40+X0

; -------------------------------------------------------------------
; cropped koala bitmap viewer
; expects address of crunched image in AX
; -------------------------------------------------------------------

zp_ptr = $f9  ;f9/fa

decodeandshowimage:
	ldy 1
	sty rcv1+1

	ldy #$36
	sty 1	;make RAM below BASIC ROM visible
	
	sta _byte_lo
	stx _byte_hi
	jsr init_decruncher

	ldy #HEIGHT
	sty linecount
	sty linecount+1
	sty linecount+2

.IF BLANKOUT = 1
	; -----------------------------------
	; blank out colors to avoid
	; seeing a half-decrunched image
	; -----------------------------------
	blankcolor=0

	lda #blankcolor
	ldx #WIDTH
lbl_loop:
	.REPEAT HEIGHT, I
	sta basescrptr+I*40-1,x
	sta basecolptr+I*40-1,x
	.ENDREP
	dex
	bne lbl_loop
.ENDIF

	; -----------------------------------
	; decrunch and save cropped bitmap
	; -----------------------------------
.scope
	lda #<basebmptr
	sta bmptr+1
	lda #>basebmptr
	sta bmptr+2

yloop:
	ldx #00
	stx xstor+1
xloop:
	jsr get_decrunched_byte
xstor:	ldx #00
bmptr:	sta bitmap,x
	inx
	stx xstor+1
	cpx #WIDTH*8
	bne xloop

	clc
	lda bmptr+1
	adc #<320
	sta bmptr+1
	lda bmptr+2
	adc #>320
	sta bmptr+2

	dec linecount
	bne yloop
.endscope

	; -----------------------------------
	; decrunch and save cropped screen
	; -----------------------------------
.scope
	lda #<basescrptr
	sta scptr+1
	lda #>basescrptr
	sta scptr+2

yloop:
	ldx #00
	stx xstor+1
xloop:
	jsr get_decrunched_byte
xstor:	ldx #00
scptr:	sta screen,x
	inx
	stx xstor+1
	cpx #WIDTH
	bne xloop

	clc
	lda scptr+1
	adc #<40
	sta scptr+1
	lda scptr+2
	adc #>40
	sta scptr+2

	dec linecount+1
	bne yloop
.endscope

	; -----------------------------------
	; decrunch and save cropped colorram vals
	; -----------------------------------
.scope
	lda #<basecolptr
	sta colptr+1
	lda #>basecolptr
	sta colptr+2

yloop:
	ldx #00
	stx xstor+1
xloop:
	jsr get_decrunched_byte
xstor:	ldx #00
colptr:	sta $d800,x
	inx
	stx xstor+1
	cpx #WIDTH
	bne xloop

	clc
	lda colptr+1
	adc #<40
	sta colptr+1
	lda colptr+2
	adc #>40
	sta colptr+2

	dec linecount+2
	bne yloop
.endscope


rcv1:	lda #$37
	sta 1
	rts

linecount:
.byte 00,00,00


; -------------------------------------------------------------------
get_crunched_byte:
; -------------------------------------------------------------------
_byte_lo = * + 1
_byte_hi = * + 2
	lda $ffff		; needs to be set correctly before
				; decrunch_file is called.
	inc _byte_lo
	bne _byte_skip_hi
	inc _byte_hi
_byte_skip_hi:
	rts
; end_of_data needs to point to the address just after the address
; of the last byte of crunched data.
; -------------------------------------------------------------------

buffer_len_hi	= 2
buffer_start_hi = >$0400
