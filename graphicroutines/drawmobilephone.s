;---------------------------------------------------
; Apr-2020 V0.1
; Wilfried Elmenreich
; Code under CC-BY-4.0 license
;---------------------------------------------------

.include "constants.inc"

.export drawmobilephone

zptr1 = $A7

bmptr = zptr1

WIDTH=PHONE_WIDTH
HEIGHT=PHONE_HEIGHT

colormix=$bc ;dark gray and mid gray

;---------------------------------------------------
; Draws a mobile phone starting at column A (0..24)
; Usage: POKE780,column:SYS ....
;---------------------------------------------------

drawmobilephone:
	lda #40-PHONE_WIDTH
	sta scrptr+1
	
	;multiply A by 8 and store in ptr
	asl
	asl
	asl
	sta bmptr

	;init high bytes of base address

	lda #>bitmap
	sta bmptr+1

	lda #>screen
	sta scrptr+2


	;fill color


	ldy #HEIGHT 
loopy:
	ldx #WIDTH-1
	lda #colormix   
loopx:
scrptr:	sta screen,x
	dex
	bpl loopx
	
	clc
	lda scrptr+1
	adc #40
	sta scrptr+1
	lda scrptr+2
	adc #00
	sta scrptr+2

	dey
	bne loopy


	;fill first line with $55 and $aa
	
	ldy #WIDTH*8-1
loopAA:	dey
	lda #$aa
	sta (bmptr),y
	dey
	ldx #06
	lda #$55
loop55:
	sta (bmptr),y
	dey
	dex
	bne loop55
	cpy #$ff
	bne loopAA


	;top corners need handwork

	ldx #topcorner_pos-topcorner_vals-1
topcorners:
	lda topcorner_vals,x
	ldy topcorner_pos,x
	sta (bmptr),y
	dex
	bpl topcorners


	;draw the sides

	jsr nextline    ;advance pointer to next line

	ldx #HEIGHT-DRAW_LOWER_FRAME-1
loopsides:
	ldy #WIDTH*8-1

	lda #$25
rightside:
	sta (bmptr),y
	dey
	cpy #WIDTH*8-1-8
	bne rightside

	lda #00
emptymiddle:
	sta (bmptr),y
	dey
	cpy #7
	bne emptymiddle

	lda #$16
leftside:
	sta (bmptr),y
	dey
	bpl leftside

	jsr nextline    ;advance pointer to next line

	dex
	bne loopsides


.if DRAW_LOWER_FRAME=1

	;fill last line with $55 and $aa
	
	ldy #WIDTH*8-1
lastlineloop:
	ldx #06
	lda #$55
lloop55:
	sta (bmptr),y
	dey
	dex
	bne lloop55

	lda #$aa
	sta (bmptr),y
	dey
	dey
	cpy #$ff
	bne lastlineloop


	;bottom corners need handwork

	ldx #botcorner_pos-botcorner_vals-1
botcorners:
	lda botcorner_vals,x
	ldy botcorner_pos,x
	sta (bmptr),y
	dex
	bpl botcorners
.endif
	rts


nextline:
	clc
	lda bmptr
	adc #<320
	sta bmptr
	lda bmptr+1
	adc #>320
	sta bmptr+1
	rts

topcorner_vals:
	.byte $00,$05,$05,$15,$15,$15,$16,$16, $6a,$66,$6a, $69,$69,$69, $40,$54,$54,$A5,$25
	;.byte $00,$05,$15,$15,$55, $6a,$66,$6a, $69,$69,$69, $00,$50,$54,$54,$55

topcorner_pos:
	.byte 00,01,02,03,04,05,06,07, WIDTH*4-6,WIDTH*4-5,WIDTH*4-4 ,WIDTH*4+2,WIDTH*4+3,WIDTH*4+4, WIDTH*8-8,WIDTH*8-7,WIDTH*8-6,WIDTH*8-2,WIDTH*8-1

botcorner_vals:
	.byte $16,$16,$15,$15,$15,$05,$05,$00, $56, $56,$6a, $95,$a9, $95, $25,$A5,$54,$54,$40
	;.byte $55,$15,$15,$05,$00, $56, $56,$6a, $95,$a9, $95, $55,$54,$54,$50,$00

botcorner_pos:
	.byte 00,01,02,03,04,05,06,07, WIDTH*4-12,WIDTH*4-5,WIDTH*4-4, WIDTH*4+3,WIDTH*4+4,WIDTH*4+12, WIDTH*8-8,WIDTH*8-7,WIDTH*8-3,WIDTH*8-2,WIDTH*8-1
