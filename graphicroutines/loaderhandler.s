.include "constants.inc"

.export disable_fastloader

.if LOAD_BY_TS=0
  .export load_file
.else
  .export load_file_ts
.endif

.import rampdiskptr

;Dreamload

LdCfgInitCodeStart 	= $AE00
LdLoc			= $BC00		;2 pages $3d00
LdZp_PresetAE		= $fe
;LdLAE		   	= $ae		;2 byte load address, defined in constants.inc

dload_install   = LdCfgInitCodeStart
dload_loadfile  = LdLoc
dload_address	= LdLAE
SpeederOff      = LdLoc+15

dload_load_ts   = LdLoc+$79

loaderInstallBackup = $D000

manage_mem1 = 1
statusmsg = 0

;----------------------------------------------------------
; Disable fastloader to allow normal file operations
;----------------------------------------------------------

disable_fastloader:
	lda loaderinstalled
	beq loadernotinstalled  ;loader is not installed - nothing to do
	lda #00
	sta loaderinstalled
	jsr SpeederOff
	delay_ms 2000 ;wait 2s to led drive reset
loadernotinstalled:
	rts

;----------------------------------------------------------
; Two possible code variants depending on if we are loading
; by filename or via track/sector number of first block
;----------------------------------------------------------

.if LOAD_BY_TS=0

;----------------------------------------------------------
; Load file, install fastloader if necessary
; expects pointer to null-terminated filename in AX
; if Y < 0x80 then loading to address in PRG file
; if Y >= 0x80 then loading to pointer in $ae/ad
;----------------------------------------------------------

load_file:
	sta fnlo+1
	sta filenametmp+1
	stx fnhi+1
	stx filenametmp+2
	sty LdZp_PresetAE

.else

;----------------------------------------------------------
; Load file, install fastloader if necessary
; expects track of first block in X and sector in Y
; if A < 0x80 then loading to address in PRG file
; if A >= 0x80 then loading to pointer in $ae/ad
;----------------------------------------------------------

load_file_ts:
	sta LdZp_PresetAE
	stx sm_track+1
	sty sm_sector+1
.endif

	lda loaderinstalled
	beq reinstall
	jmp loadfile

	;we need to install dreamload
reinstall:
	;transfer from $D000 RAM -> dload_install

.if manage_mem1 = 1
	lda 1
	sta rcv1+1
.endif

.if IRQS_WHEN_ROM_OFF=0
	sei
.endif
	lda #%00110100
	sta 1

	ldy #00
restore_loadinstall:
.repeat $0e,I
	lda loaderInstallBackup+I*$100,y
	sta dload_install+I*$100,y
.endrep
	iny
	bne restore_loadinstall

	lda #$36
	sta 1

	lda #$08
	sta $ba
        jsr dload_install 
	cli
.if manage_mem1 = 1
rcv1:	lda #$37
	sta 1
.endif
        ;bcs fastloader_install_error
        bcc fastloadinstallok ;success!

fastloader_install_error:

.if statusmsg = 1
        primm "error installing fastloader!"
.endif

.FEATURE string_escapes
        primm "\n\x96Could not install loader, disable extra drives and other fastloaders before running the game!"
	waitkey
	jsr 64738
        rts

fastloadinstallok:
.if statusmsg = 1
	primm "Fastloader installed!"
.endif

	inc loaderinstalled

.if ENABLE_RAMDISK=1
	lda #$ff
	sta pic1_in_mem
	sta pic2_in_mem
.endif


loadfile:
.if statusmsg = 1
	primm "Loading image"
.endif

.if LOAD_BY_TS=0

	ldy #00 
countlenloop:
	iny
filenametmp:	
	lda $affe,y
	bne countlenloop

filenameend:

.endif

.if ENABLE_RAMDISK=1

	lda lastloaded
	eor #1
	sta lastloaded
	tax
	lda #0
	sta dload_address
	lda pic_buff_hiaddr,x
	sta dload_address+1
.else
	lda #<BEGIN_LOADBUFFER
	sta dload_address
	lda #>BEGIN_LOADBUFFER
	sta dload_address+1
.endif

.if LOAD_BY_TS=0

	tya	;A contains length of filename
fnlo:   	ldx #00
fnhi:    	ldy #00

	jmp dload_loadfile ;load the file
.else
sm_track: ldx #00
sm_sector: ldy #00
	jmp dload_load_ts ;load the file starting at track/sector given in X/Y
.endif

	;rts

loaderinstalled:
	.byte 00

.if ENABLE_RAMDISK=1
.export lastloaded,pic_buff_hiaddr,pic1_in_mem,pic2_in_mem

pic1_in_mem: .byte $ff
pic2_in_mem: .byte $ff
lastloaded: .byte 00
pic_buff_hiaddr: .byte >BEGIN_LOADBUFFER, >BEGIN_RAMDISK_PIC2
.endif