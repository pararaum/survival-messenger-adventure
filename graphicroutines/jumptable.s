.import clrgfx,disablebitmapprint,drawmobilephone,gotoxy,graphic_init,graphic_off,home_pos,printcharbmp,scrolldown_mobilephone,scrolldown_text,input_sr
.import selectwindow1,selectwindow2,waitkeycursor,load_file,disable_fastloader,show_image

.export jumptable

;---------------------------------------------------
; Jump table to the main functions
;---------------------------------------------------

jumptable:
	;base
	jmp graphic_init
	
	;base+3
	jmp graphic_off
	
	;base+6
	jmp clrgfx
	
	;base+9
	jmp home_pos
	
	;base+12
	jmp gotoxy
	
	;base+15
	jmp scrolldown_mobilephone
	
	;base+18
	jmp scrolldown_text
	
	;base+21
	jmp drawmobilephone
	
	;base+24
	jmp show_image ;Decode and show graphic
	
	;base+27
	jmp input_sr

	;base+30
	jmp selectwindow1

	;base+33
	jmp selectwindow2

	;base+36
	jmp waitkeycursor

	;base+39
	;jmp load_file
	jmp 64738

	;base+42
	jmp disable_fastloader