#include <conio.h>
#include <stdio.h>
#include <stdlib.h>
#include "gfx_functions.h"

char data1[12] = "Firstdata";
char data2[12] = "Seconddata";
char data3[12] = "\0";
char data4[12] = "\0";

void std_write(unsigned char * file_name);
void std_read(unsigned char * file_name);

void waitkey()
{
    poke(198,0);
    while(peek(198)==0) ;    
}

int main (void)
{
    disable_loader();
    set_bgcolor(0);
    set_bordercolor(0);     
    
    gfx_init();
    gfx_clrscr();
    set_bgcolor(0);
    gfx_drawmobile();
    
    gfx_showimage(1);     
    gfx_setcursor(1,23);

    set_textcolor(10);
    printf ("Press any key to save testfile!\n");
    waitkeycursor();   
    selectwindow2();
    set_textcolor(12);    
    printf ("Turning off loader\n");
    disable_loader();
    printf ("Saving\n");
    selectwindow1();
    set_textcolor(12);        
    std_write("testfile");

    set_textcolor(10);
    printf ("Press any key to view next image!\n");
    waitkeycursor();       
    gfx_showimage(1);        

    printf ("Press any key to load testfile!\n");
    waitkeycursor();   
    selectwindow2();
    set_textcolor(12);        
    printf ("Turning off loader\n");
    disable_loader();
    printf ("Loading\n");
    selectwindow1();
    set_textcolor(12);        
    std_read("testfile");  
    
    set_textcolor(3);        
    printf("\ndata3 : %s\n",data3);
    printf("data4 : %s\n",data4);

    set_textcolor(10);    
    printf ("Press any key to view next image!\n");
    waitkeycursor();                  
    gfx_showimage(3); 

    set_textcolor(2);    
    printf("This is the end.");       
    waitkeycursor();
    
    gfx_off(); 
        
    return EXIT_SUCCESS;
}

void std_write(unsigned char * file_name)
    {
        FILE *file;
        unsigned char n;

        printf("Opening data file...\n\r");
        _filetype = 's';
        if(file = fopen(file_name, "w"))
            {
                printf("Writing...\n\r");
                n = fwrite(data1, sizeof(unsigned char)*11, 1, file);
                n = n + fwrite(data2, sizeof(unsigned char)*11, 1, file);

                if(n != 2)
                {
                    printf("Error: File could not be written.\n\r");
                    fclose(file);
                }
                else
                {
                    printf("Done.\n\r");
                    fclose(file);       
                }
            }
        else
            {
                printf("File could not be opened\n\r");
            }
    }

void std_read(unsigned char * file_name)
    {
        FILE *file;
        unsigned char n;

        printf("Opening data file...\n\r");
        _filetype = 's';
        if(file = fopen(file_name, "r"))
        {
            printf("Reading...\n\r");
            n = fread(data3, sizeof(unsigned char)*11, 1, file);
            n = n + fread(data4, sizeof(unsigned char)*11, 1, file);

            if(n != 2)
            {
                printf("Error while reading!\n\r");
                fclose(file);
            }
            else
            {
                printf("Done.\n\r");
                fclose(file);
            }
        }
        else
        {
            printf("File could not be opened\n\r");
        }
    }
