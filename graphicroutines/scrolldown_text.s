;---------------------------------------------------
; June-2020 V1.3
; Wilfried Elmenreich
; Code under CC-BY-4.0 license
;---------------------------------------------------
; Scroll down part of a bitmap image
; It scrolls bitmap and the colors in $D800
;---------------------------------------------------

; ----- configuration -----
.include "constants.inc"

LINESTART=TEXTAREA_FIRSTLINE
COLUMNSTART=TEXTAREA_FIRSTCOLUMN
WIDTH=TEXTAREA_WIDTH
HEIGHT=TEXTAREA_HEIGHT
; --- end configuration ---


.export scrolldown_text

scrolldown_text:
.if IRQS_WHEN_ROM_OFF=0
	sei
.endif
	lda $01
	sta rcv1+1
	lda #$35
	sta 1

.IF HEIGHT<9

;---------------------------------------------------
; Implementation with unrolled loop
;---------------------------------------------------

	ldx #WIDTH*8
loop:
.REPEAT HEIGHT-1,I
	lda bitmap+(LINESTART+I+1)*320+COLUMNSTART*8-1,x
	sta bitmap+(LINESTART+I)*320+COLUMNSTART*8-1,x
.ENDREP
	dex
	bne loop

	ldx #WIDTH
loop2:	
.REPEAT HEIGHT-1,I
	lda $d800+(LINESTART+I+1)*40+COLUMNSTART-1,x
	sta $d800+(LINESTART+I)*40+COLUMNSTART-1,x
.ENDREP
	dex
	bne loop2

;---------------------------------------------------

.ELSE

;---------------------------------------------------
; Implementation with loop and calculations
;---------------------------------------------------

basebmptr=bitmap+LINESTART*320+COLUMNSTART*8-1
basecolptr=colram+LINESTART*40+COLUMNSTART-1

	lda #<basebmptr
	sta bmp_target
	lda #>basebmptr
	sta bmp_target+1

	lda #<basecolptr
	sta col_target
	lda #>basecolptr
	sta col_target+1
	
	ldy #HEIGHT-1
yloop:  clc
	lda bmp_target
	adc #<320
	sta bmp_source
	lda bmp_target+1
	adc #>320
	sta bmp_source+1

	lda col_target
	adc #40
	sta col_source
	lda col_target+1
	adc #00
	sta col_source+1

;copy bitmap part
	ldx #WIDTH*8
xloop:

bmp_source = * + 1	
	lda bitmap,x

bmp_target = * + 1	
	sta bitmap,x

	dex
	bne xloop

;copy color part
	ldx #WIDTH
xloop2:	
col_source = * + 1	
	lda $d800,x
col_target = * + 1	
	sta $d800,x
	dex
	bne xloop2

;new target address = old source address

	lda bmp_source
	sta bmp_target
	lda bmp_source+1
	sta bmp_target+1
	lda col_source
	sta col_target
	lda col_source+1
	sta col_target+1

	dey
	bne yloop

;---------------------------------------------------

.ENDIF


;delete last line
	lda #00
	ldx #WIDTH*8 
delloop:
	sta bitmap+(LINESTART+HEIGHT-1)*320+COLUMNSTART*8-1,x
	dex       
	bne delloop 

rcv1:	lda #00
	sta $01
.if IRQS_WHEN_ROM_OFF=0
	cli
.endif
	rts


