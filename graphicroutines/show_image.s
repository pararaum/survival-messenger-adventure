;---------------------------------------------------
; Apr-2020 V0.1
; Wilfried Elmenreich
; Code under CC-BY-4.0 license
;---------------------------------------------------

.include "constants.inc"

.import decodeandshowimage

.if LOAD_BY_TS=0
	.import load_file
.else
	.import load_file_ts
.endif

.export show_image

;---------------------------------------------------
; show_image
; image number given in A
; checks if we have the image in the cache
; otherwise calls loader
; the displays image
;---------------------------------------------------

show_image:
	cmp currentimg
	bne skip
	rts	;same image number as before, nothing to do

skip:
	sta currentimg

.if ENABLE_RAMDISK=1
.import pic1_in_mem,pic2_in_mem

	cmp pic1_in_mem
	beq found_p1
	cmp pic2_in_mem
	bne load_it

found_p2:
	ldy #01
	bne st_and_go

found_p1: ldy #00
st_and_go:
	sty lastloaded
	jmp display
.endif

;---------------------------------------------------
;we need to load the image
;---------------------------------------------------

load_it:
.if ENABLE_RAMDISK=1
	sta rcva+1
.endif
	ldy #00


.if LOAD_BY_TS=0

; *** make filename

digitloop:
	cmp #$0a
	bcc below9
	iny
	sbc #$0a
	bpl digitloop

below9:	clc
	adc #$30
	sta filename+1
	tya
	adc #$30
	sta filename

	lda #<filename
	ldx #>filename
	ldy #$80
	jsr load_file
.else
	tax
	lda tracks,x
	ldy sectors,x
	tax
	lda #$80
	jsr load_file_ts
.endif

.if ENABLE_RAMDISK=1
.import lastloaded,pic_buff_hiaddr
	ldy lastloaded
rcva:	lda #00		;will be overwritten with pic number
	sta pic1_in_mem,y

display:
	lda #0
	ldx pic_buff_hiaddr,y
.else
	lda #<BEGIN_LOADBUFFER
	ldx #>BEGIN_LOADBUFFER
.endif
	jmp decodeandshowimage


currentimg: .byte $ff

filename: .byte 00,00,00

.if LOAD_BY_TS=1
	.include "tracksectors.inc"
.endif