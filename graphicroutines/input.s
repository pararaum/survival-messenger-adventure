;---------------------------------------------------
; Input of a string
; May-2020 V0.1
; Wilfried Elmenreich
; Code under CC-BY-4.0 license
;
; Returns the address of null-terminated string in A/X and length in Y
;---------------------------------------------------

.export input_sr,waitkeycursor
.import bmpaddratcursor,coladdratcursor

.include "LAMAlib.inc"

MAXLEN=49
zptr = $A7

CURSOR=219  ;underscore


input_sr:
	sta store_text+1
	stx store_text+2
	ldy #0

lbl_loop:
	sty rcvy+1

	;wait for keypress
 	jsr waitkeycursor

rcvy:	ldy #00
	beq skipdelete	;if string length is 0 we don't have anything to delete

	cmp #20
	beq delete
skipdelete:
	cmp #13
	beq return

	cpy #MAXLEN
	bcs lbl_loop

	cmp #32
	beq printc

	cmp #'0'
	bcc lbl_loop
	cmp #'9'+1
	bcc printc

	cmp #'a'
	bcc lbl_loop
	cmp #'z'+1
	bcc printc	

	cmp #'A'
	bcc lbl_loop
	cmp #'Z'+1
	bcs lbl_loop

printc:  
	jsr store_text
	jsr CHROUT
	iny
	jmp lbl_loop


delete:
	dey
	lda #157	;left
	jsr CHROUT
	jmp lbl_loop

cursor_delete_moves:
.byte 32,157,157,CURSOR

return: 	;return with address of null-terminated string in A/X and length in Y
	lda #00	
	ldx #20
store_text:
	sta $200,y
	rts

;---------------------------------------------------
; Waits for a keypress and shows a blinking cursor
;---------------------------------------------------

waitkeycursor:
	lda coladdratcursor
	sta zptr
	lda coladdratcursor+1
	sta zptr+1
	ldy #00
	lda $286
	sta (zptr),y

	lda bmpaddratcursor
	sta zptr
	lda bmpaddratcursor+1
	sta zptr+1

blinkloop:

	jsr delay_ms_or_key

	;print full cursor block
	ldy #7
	lda #$ff
loopfc:	sta (zptr),y
	dey
	bpl loopfc


	jsr delay_ms_or_key

	;print empty cursor block
	ldy #7
	lda #00
loopfc2:	sta (zptr),y
	dey
	bpl loopfc2
	
	jsr GETIN
	beq blinkloop	
	ldx #00
	rts

;---------------------------------------------------
; Delay A ms or exit early if a key has been pressed
;---------------------------------------------------
delay_ms_or_key:
	lda #$80
loop1:
	ldx #00
loop2:
	ldy 198
	bne exit	;exit if a keypress is immanent
	dex
	bne loop2

	sec
	sbc #01
	bcs loop1
exit:
	rts

