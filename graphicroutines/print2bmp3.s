;---------------------------------------------------
; May-2020 V0.5
; Wilfried Elmenreich
; Code under CC-BY-4.0 license
; Prints characters to a multicolor bitmap
; this version also handles control characters
; and supports windowing
;---------------------------------------------------

.include "constants.inc"

.export printcharbmp, gotoxy, enablebitmapprint, disablebitmapprint, home_pos
.export selectwindow1,selectwindow2
.export bmpaddratcursor:=bmptr+1,coladdratcursor:=colptr+1

.import scrolldown_mobilephone,scrolldown_text

zptr = $A7

support_fileio = 1

window_conf:
cursorx: .byte TEXTAREA_FIRSTCOLUMN
cursory: .byte TEXTAREA_FIRSTLINE
currentcolor: .byte 1
homex:   .byte TEXTAREA_FIRSTCOLUMN
homey:   .byte TEXTAREA_FIRSTLINE
lastcolumn:   .byte TEXTAREA_FIRSTCOLUMN+TEXTAREA_WIDTH-1
lastline:.byte TEXTAREA_FIRSTLINE+TEXTAREA_HEIGHT-1
scrolldown: jmp scrolldown_text


;---------------------------------------------------
; Redirects operating system print to these routines here
;---------------------------------------------------
enablebitmapprint:
	poke $0326,<printcharbmp
	poke $0327,>printcharbmp
      	jmp home_pos

;---------------------------------------------------
; Reset operating system print to default
;---------------------------------------------------
disablebitmapprint:
	poke $0326,<$F1CA
	poke $0327,>$F1CA
      	rts

;---------------------------------------------------
; Switch between two window sets
;---------------------------------------------------

selectwindow1:
	jsr save_cursor_pos
	ldy #9
cploop1:
	lda window_conf1,y
	sta window_conf,y
	dey
	bpl cploop1
	lda #00
	sta currentconf

	lda currentcolor
	sta textcolor
	ldx cursorx
	ldy cursory
	jmp gotoxy

selectwindow2:
	jsr save_cursor_pos
	ldy #9
cploop2:
	lda window_conf2,y
	sta window_conf,y
	dey
	bpl cploop2
	lda #10
	sta currentconf

	lda currentcolor
	sta textcolor
	ldx cursorx
	ldy cursory
	jmp gotoxy

save_cursor_pos:
	ldx currentconf
	lda cursorx
	sta window_conf1,x
	lda cursory
	sta window_conf1+1,x
	lda textcolor
	sta window_conf1+2,x
	rts
	
window_conf1:
	.byte 	TEXTAREA_FIRSTCOLUMN, TEXTAREA_FIRSTLINE, 4 ,TEXTAREA_FIRSTCOLUMN, TEXTAREA_FIRSTLINE, TEXTAREA_FIRSTCOLUMN+TEXTAREA_WIDTH-1, TEXTAREA_FIRSTLINE+TEXTAREA_HEIGHT-1
	jmp scrolldown_text
	
window_conf2:
	.byte 	41-PHONE_WIDTH, PHONE_HEIGHT-1-DRAW_LOWER_FRAME, 3 , 41-PHONE_WIDTH, 1, 38, PHONE_HEIGHT-DRAW_LOWER_FRAME-1
	jmp scrolldown_mobilephone

currentconf: 
	.byte 00
;---------------------------------------------------
; poor man's replacement for CHROUT
;---------------------------------------------------

printcharbmp:

.if support_fileio = 1

	pha
	lda $9A		;get device number to write to
	cmp #$03	;screen?
	beq output_to_screen
	bcc datasetteorrsr232	;lower than 3?
	pla
	jmp $EDDD  	;output byte to serial bus
datasetteorrsr232:
	jmp $F1DB	;out byte to device 1 or 2
output_to_screen:
	pla
.endif

	sty rcvy+1
	stx rcvx+1

	tay
	and #%01100000
	bne printablechar  
	tya
	cmp #$0d
	bne noret
	jmp chr_return
noret:
	cmp #29
	bne noright
	jmp chr_right
noright:
	cmp #$11
	bne nodown
	jmp chr_down
nodown:
	cmp #$13
	bne nohome
	jmp home_pos
nohome:
	cmp #145
	bne noup
	jmp chr_up
noup:
	cmp #157
	bne noleft
	jmp chr_left
noleft:
	;must be a color code
	ldy #7
checkcolors:
	cmp color_chars,y
	bne nohit
	sty textcolor
	jmp rcvy
nohit:
	dey
	bpl checkcolors
	sec
	sbc #140
	sta textcolor
	jmp rcvy

color_chars:
.byte 144,5,28,159,156,30,31,158

printablechar:
;convert to screencode
	tya             	
	bmi above128
	and #$3f
above128:
	and #$7f

;calculate addr
	ldy #00
	sty zptr+1
	.REPEAT 3
	asl
	rol zptr+1
	.ENDREP	

;add charbase addr

	clc
	adc #<fat_charset
	sta zptr
	lda zptr+1
	adc #>fat_charset
	sta zptr+1

;copy char
	ldy #07
copy_char_loop:
	lda (zptr),y
bmptr:	sta bitmap,y
	dey
	bpl copy_char_loop

;copy color
	lda textcolor
colptr:	sta colram

	;fallthrough!

;---------------------------------------------------
;cursor right
;---------------------------------------------------
chr_right:

	lda cursorx
	cmp lastcolumn
	bcc ok1
	jmp chr_return
ok1:	inc cursorx

;update bitmapptr by advancing one char
	clc
	lda bmptr+1
	adc #8
	sta bmptr+1
	lda bmptr+2
	adc #0
	sta bmptr+2
;	beq home_pos	;when higbyte becomes 00, we have an overflow at the graphics area

;advance color pointer
	inc colptr+1
	bne gorcvy
	inc colptr+2

        jmp rcvy

;---------------------------------------------------
;cursor left
;---------------------------------------------------
chr_left:
;go back one char
	lda homex
	cmp cursorx
	bcs up_and_to_end_of_line	;we are already at the left side of the window
	dec cursorx

	sec
	lda bmptr+1
	sbc #8
	sta bmptr+1
	lda bmptr+2
	sbc #0
	sta bmptr+2
	cmp #$e0
	bcc home_pos_step1	;when higbyte becomes lower than $e0, we have an underflow at the graphics area

;advance color pointer
	ldy colptr+1
	bne nohi
	dec colptr+2
nohi:	dey
	sty colptr+1
        jmp rcvy

;go up and to end of line (we were already at the left side of the window)

up_and_to_end_of_line:
	lda homey
	cmp cursory
	bcs gorcvy	;we are already at the top of the window
	dec cursory	
	lda lastcolumn
	sta cursorx

	ldx cursorx
	ldy cursory
	jsr gotoxy	
gorcvy:
	jmp rcvy

;---------------------------------------------------
;cursor down
;---------------------------------------------------
chr_down:
	lda cursory
	cmp lastline
	bcc ok2	
	jsr scrolldown
	jmp rcvy
ok2:	inc cursory
	clc
	lda bmptr+1
	adc #<320
	sta bmptr+1
	lda bmptr+2
	adc #>320
	sta bmptr+2
	cmp #$02
home_pos_step1:		;intermediate branch point
	bcc home_pos	;when higbyte becomes $00 or $01, we have an overflow at the graphics area

;advance color pointer
	lda colptr+1
	adc #40
	sta colptr+1
	bcc rcvy
	inc colptr+1
        jmp rcvy

;---------------------------------------------------
;cursor up
;---------------------------------------------------
chr_up:
	lda homey
	cmp cursory
	bcs rcvy	;we are already at the top of the window
	dec cursory

	sec
	lda bmptr+1
	sbc #<320
	sta bmptr+1
	lda bmptr+2
	sbc #>320
	sta bmptr+2
	cmp #$e0
	bcc home_pos	;when higbyte becomes lower than $e0, we have an underflow at the graphics area

;update color pointer
	lda colptr+1
	sbc #40
	sta colptr+1
	bcs rcvy
	dec colptr+1
        jmp rcvy

;---------------------------------------------------
;set cursor to home position
;---------------------------------------------------
home_pos:
	ldx homex
	ldy homey
	jsr gotoxy	


;	;reset to home position
;	lda #<bitmap
;	sta bmptr+1
;	lda #>bitmap
;	sta bmptr+2
;
;	lda #<colram
;	sta colptr+1
;	lda #>colram
;	sta colptr+2




rcvy:	ldy #00
rcvx:	ldx #00
	clc
	rts

;---------------------------------------------------
;return to beginning of next line
;---------------------------------------------------
.export chr_return
chr_return:
	lda homex
	sta cursorx

	lda cursory
	cmp lastline
	bcc ok3
	jsr scrolldown
        jmp ok4
ok3:	inc cursory
ok4:
	ldx cursorx
	ldy cursory
	jsr gotoxy
	jmp rcvy	

;
;	;bmptr *4 / 256 -> divide by 64
;	lda bmptr+2 ;accu contains bmptr / 128
;	and #$1f
;	asl bmptr+1
;	rol
;
;	asl bmptr+1
;	rol 
;         ;accu contains now bmptr / 128
;         ;zptr contains now bmptr / 64
;         ;lets divide bmptr / 64 by 5, this gives us bmptr / 320, i.o.w. the current line
;	sta zptr
;	lsr
;	adc zptr
;	ror
;  	lsr
;  	lsr
;	adc zptr
;	ror
;	adc zptr
;	ror
;	lsr
;	lsr	;accu contains now linenr
;	tay
;	iny
;	stx rcvx+1
;	ldx #0
;	cpy #25
;	bne skip
;	ldy #0
;skip:   jsr gotoxy
;rcvx:	ldx #00
;        jmp rcvy

;---------------------------------------------------
; Places the graphical cursor to the value 
; in registers X and Y
;---------------------------------------------------

gotoxy: 
	stx cursorx
	sty cursory
	;lda #<bitmap
	;sta bmptr+1
	tya
	clc
	adc #>bitmap	;count 256 byte for each line, 64 remaining
	sta bmptr+2

	;multiply line (y)  by 64 (*256 / 4)
	lda #00
	sta zptr
	sty zptr+1

	.REPEAT 2
	lsr zptr+1
	ror zptr
	.ENDREP
	
	;add to bitmap addr
	clc
	lda #<bitmap
	adc zptr
	sta bmptr+1
	lda bmptr+2
	adc zptr+1
	sta bmptr+2

	;calculate color ram addr
	
	lsr zptr+1
	ror zptr	;zptr contains now Y*32

	clc
	txa		;use the column as low byte
	adc zptr
	sta colptr+1
	lda #>colram
	adc zptr+1
	sta colptr+2

	lsr zptr+1
	ror zptr	
	lsr zptr+1
	ror zptr	;zptr contains now Y*8

	lda colptr+1
	adc zptr
	sta colptr+1
	lda colptr+2
	adc zptr+1
	sta colptr+2	;colptr contains now the color ram addr

	;back to the bitmap addr
	;multply column (X) by 8

	stx zptr
	lda #0
	sta zptr+1
	
	.REPEAT 3
	asl zptr
	rol zptr+1
	.ENDREP

	;add to bitmap addr
	clc
	lda bmptr+1
	adc zptr
	sta bmptr+1
	lda bmptr+2
	adc zptr+1
	sta bmptr+2

	rts


;----------------------------------------------------
; A multicolor mode lower/uppercase charset by ComSha
;----------------------------------------------------

fat_charset:
.byte $0c,$33,$33,$3f,$30,$33,$0c,$00, $00,$00,$3f,$03,$3f,$33,$3f,$00, $30,$30,$3c,$33,$33,$33,$3c,$00, $00,$00,$3f,$33,$30,$33,$3f,$00  ;@abc
.byte $03,$03,$3f,$33,$33,$33,$3f,$00, $00,$00,$3f,$33,$3f,$30,$3f,$00, $0c,$33,$30,$3c,$30,$30,$30,$00, $00,$00,$3f,$33,$33,$3f,$03,$3f  ;defg
.byte $30,$30,$3f,$33,$33,$33,$33,$00, $0c,$00,$0c,$0c,$0c,$0c,$0c,$00, $03,$00,$03,$03,$03,$03,$33,$3f, $30,$30,$33,$33,$3c,$3f,$33,$00  ;hijk
.byte $3c,$0c,$0c,$0c,$0c,$0c,$0f,$00, $00,$00,$33,$3f,$3f,$33,$33,$00, $00,$00,$3c,$33,$33,$33,$33,$00, $00,$00,$3f,$33,$33,$33,$3f,$00  ;lmno
.byte $00,$00,$3f,$33,$33,$3f,$30,$30, $00,$00,$3f,$33,$33,$3f,$03,$03, $00,$00,$3f,$33,$30,$30,$30,$00, $00,$00,$3f,$30,$3f,$03,$3f,$00  ;pqrs
.byte $0c,$0c,$3f,$0c,$0c,$0f,$0f,$00, $00,$00,$33,$33,$33,$33,$3f,$00, $00,$00,$33,$33,$33,$3f,$0c,$00, $00,$00,$33,$33,$3f,$3f,$33,$00  ;tuvw
.byte $00,$00,$33,$3f,$0c,$3f,$33,$00, $00,$00,$33,$33,$33,$3f,$03,$3f, $00,$00,$3f,$03,$0c,$30,$3f,$00, $3c,$30,$30,$30,$30,$30,$3c,$00  ;xyz[
.byte $0c,$33,$30,$3c,$30,$33,$ff,$00, $3c,$0c,$0c,$0c,$0c,$0c,$3c,$00, $0c,$0c,$3f,$3f,$0c,$0c,$0c,$0c, $00,$30,$30,$ff,$ff,$30,$30,$00  ;\]^_
.byte $00,$00,$00,$00,$00,$00,$00,$00, $0c,$0c,$0c,$0c,$00,$00,$0c,$00, $33,$33,$33,$00,$00,$00,$00,$00, $33,$33,$ff,$33,$ff,$33,$33,$00  ; !"#
.byte $0c,$3f,$30,$3c,$03,$3c,$0c,$00, $33,$33,$03,$0c,$30,$33,$33,$00, $0c,$33,$0c,$3c,$33,$33,$0c,$00, $03,$0c,$0c,$00,$00,$00,$00,$00  ;$%&'
.byte $0c,$30,$30,$30,$30,$30,$0c,$00, $30,$0c,$0c,$0c,$0c,$0c,$30,$00, $00,$33,$0c,$3f,$0c,$33,$00,$00, $00,$0c,$0c,$3f,$0c,$0c,$00,$00  ;()*+
.byte $00,$00,$00,$00,$00,$0c,$0c,$30, $00,$00,$00,$3f,$00,$00,$00,$00, $00,$00,$00,$00,$00,$0c,$0c,$00, $00,$03,$03,$0c,$0c,$30,$30,$00  ;,-./
.byte $0c,$33,$33,$33,$33,$33,$0c,$00, $0c,$0c,$3c,$0c,$0c,$0c,$3f,$00, $3f,$33,$03,$3f,$30,$30,$3f,$00, $3f,$33,$03,$0f,$03,$33,$3f,$00  ;0123
.byte $03,$0f,$3f,$33,$3f,$03,$03,$00, $3f,$30,$3f,$03,$03,$33,$3f,$00, $3f,$33,$30,$3f,$33,$33,$3f,$00, $3f,$33,$03,$0f,$0c,$0c,$0c,$00  ;4567
.byte $3f,$33,$33,$3f,$33,$33,$3f,$00, $3f,$33,$33,$3f,$03,$33,$3f,$00, $00,$00,$0c,$00,$00,$0c,$00,$00, $00,$00,$0c,$00,$00,$0c,$0c,$30  ;89:;
.byte $00,$03,$0c,$30,$0c,$03,$00,$00, $00,$00,$3f,$00,$3f,$00,$00,$00, $00,$30,$0c,$03,$0c,$30,$00,$00, $3c,$33,$03,$0c,$0c,$00,$0c,$00  ;<=>?
.byte $00,$00,$00,$ff,$ff,$00,$00,$00, $0c,$3f,$33,$3f,$33,$33,$33,$00, $3c,$33,$33,$3c,$33,$33,$3c,$00, $3f,$33,$30,$30,$30,$33,$3f,$00  ;.ABC
.byte $3c,$3f,$33,$33,$33,$3f,$3c,$00, $3f,$30,$30,$3c,$30,$30,$3f,$00, $3f,$30,$30,$3c,$30,$30,$30,$00, $3f,$30,$30,$33,$33,$33,$3f,$00  ;DEFG
.byte $33,$33,$33,$3f,$33,$33,$33,$00, $3f,$0c,$0c,$0c,$0c,$0c,$3f,$00, $03,$03,$03,$03,$33,$3f,$0f,$00, $33,$33,$33,$3c,$3f,$33,$33,$00  ;HIJK
.byte $30,$30,$30,$30,$30,$30,$3f,$00, $33,$33,$3f,$3f,$33,$33,$33,$00, $3c,$3f,$33,$33,$33,$33,$33,$00, $3f,$33,$33,$33,$33,$33,$3f,$00  ;LMNO
.byte $3f,$33,$33,$3f,$30,$30,$30,$00, $3f,$33,$33,$33,$33,$3c,$0f,$00, $3f,$33,$33,$3f,$3c,$33,$33,$00, $3f,$33,$30,$3f,$03,$33,$3f,$00  ;PQRS
.byte $3f,$0c,$0c,$0c,$0c,$0c,$0c,$00, $33,$33,$33,$33,$33,$33,$3f,$00, $33,$33,$33,$33,$33,$3f,$0c,$00, $33,$33,$33,$33,$3f,$3f,$33,$00  ;TUVW
.byte $33,$33,$3f,$0c,$3f,$33,$33,$00, $33,$33,$33,$3f,$0c,$0c,$0c,$00, $3f,$03,$0f,$3c,$30,$30,$3f,$00, $00,$00,$00,$00,$00,$00,$00,$3f  ;XYZ_