;---------------------------------------------------
; June-2020 V0.3
; Wilfried Elmenreich
; Code under CC-BY-4.0 license
;---------------------------------------------------

.include "constants.inc"

.export graphic_init,clrgfx,graphic_off
.import enablebitmapprint,disablebitmapprint,irq_catcher_init

;---------------------------------------------------
; Turns multicolor hires graphics mode on
; Shifts VIC chip address space to $c000-$ffff
; Makes PRINT write on the graphics screen
;---------------------------------------------------

graphic_init:

	;lda #1
	;sta 646
	;jsr $e544	;clr/home, fill D800 with selected color

	lda #59   ;bit 5 on = Bitmap-Modus
	sta $D011 
	lda #216
	sta $D016

	lda #%00111000
	sta $d018	;screen at VIC bank + $0c00, bitmap at +$2000

	lda $dd00
	and #%11111100
	sta $dd00	;set VIC bank to $C000-$FFFF

.if IRQS_WHEN_ROM_OFF=1
	jsr irq_catcher_init
.endif

	jmp enablebitmapprint

	;rts

;---------------------------------------------------
; Cleans the graphic screen
;---------------------------------------------------

clrgfx: 
	;bg/fg color in y
	ldx #250
lbl_loop:	
	lda #00
	.repeat 32, I
	sta bitmap+I*250-1,x
	.endrepeat
	tya
	sta screen-1,x
	sta screen+249,x
	sta screen+499,x
	sta screen+749,x	;this does not overwrite sprite pointers
	dex
	bne lbl_loop
	rts

;---------------------------------------------------
; Turns graphic mode off
;---------------------------------------------------

graphic_off:

	lda #$1b
	sta $D011 
	lda #$c8
	sta $D016
	lda #$15
	sta $d018

	lda $dd00
	ora #%00000011
	sta $dd00

	jmp disablebitmapprint
	;rts

