#include <stdio.h>
#include <stdlib.h>
#include "gfx_functions.h"

void waitkey()
{
    poke(198,0);
    while(peek(198)==0) ;    
}

int main (void)
{
    int i,pic,c;
    unsigned char s[32];
     
    set_bgcolor(0);
    set_bordercolor(0);     
    
    gfx_init();
    gfx_clrscr();
    set_bgcolor(0);
    gfx_drawmobile();
    
    gfx_setcursor(1,23);
    printf ("Press any key to start!\n");
    waitkeycursor();   
    
    gfx_showimage(55); 
    waitkeycursor();   
    gfx_showimage(53);     
        
    selectwindow1();
    set_textcolor(10);
    printf("Incoming msg\n");
  
    selectwindow1();  
    printf ("What do you want to do?\n");    
    printf("?");
    input(s);
    
    set_textcolor(5);
    printf("You entered: %s\n",s);
    
    selectwindow2();
    for (i=1;i<=5;i++) {
      printf("%s\n",s);
    }
  
    waitkeycursor();   
    
    printf("\x93");
    
    selectwindow1();   
    for (i=54;i>=0;i--) {
        c=waitkeycursor();
        if ((c>='0') && (c<='9'))
           pic=c-'0';
        else
           pic=i;
        selectwindow1();
    	printf("Image%i,",pic);
    	selectwindow2();
    	printf("Oh, image%i...lovely!",pic);	
        gfx_showimage(pic);

    }
    gfx_scrolldowntext();
    gfx_scrolldownmobile();

    gfx_setcursor(0,24); 
    set_textcolor(2);
    printf("This is the end.");       
    waitkeycursor();
    
    gfx_off(); 
        
    return EXIT_SUCCESS;
}
