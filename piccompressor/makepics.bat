@echo off
setlocal ENABLEDELAYEDEXPANSION

del /q croppedpics

set pixelshuffler="..\tools\pixelshuffler"
set cropkoala="..\tools\cropkoala"

if not exist !pixelshuffler!.exe (
  wget -O "tmp.zip" https://csdb.dk/release/download.php?id=241984
  unzip tmp.zip pixelshuffler.exe
  move pixelshuffler.exe !pixelshuffler!.exe
  rm tmp.zip
)
if not exist !cropkoala!.exe (
  wget -O "tmp.zip" https://csdb.dk/release/download.php?id=241986
  unzip tmp.zip cropkoala.exe
  move cropkoala.exe !cropkoala!.exe
  rm tmp.zip
)

mkdir croppedpics

for %%i in ("..\pictures\*.kla") do (
  set filename=%%~ni
  %pixelshuffler% -bz -p 1d73f5aec842b960 -o "croppedpics\!filename:~0,2!.kla" -i "%%i"
)

for %%i in ("croppedpics\*.kla") do (
  %cropkoala% -x 112 -y 8 -w 208 -h 112 -o "croppedpics\%%~ni.ckla" -i "%%i"
)

for %%i in ("croppedpics\*.ckla") do (
  exomizer raw -P0 -B -c -m 512 "%%i",2 -o "croppedpics\%%~ni.exo" 
)

for %%i in ("croppedpics\*.exo") do (
  cat 2bytes.prg "%%i" > "croppedpics\%%~ni.prg" 	
)

cl65 -C c64-asm.cfg  --start-addr $A000 decompstreamandshow.s exostreamdecr1.s streamdataimporter.s -o imgviewer.prg 

del *.o

dir croppedpics\*.prg /os