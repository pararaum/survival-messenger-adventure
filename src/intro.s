	.include	"crc8.i"
	.import	_main
	.import	__BSS_LOAD__
	.import __BSS_SIZE__
	.macpack cbm

CLEAR_VALUE = 0
CRCTABLE := $0400+18*40
	.export	CRCTABLE

UNALTEREDC64 = $40		; $FA is the value for an unaltered C64.

	.segment "LOADADDR"
	.export __LOADADDR__
__LOADADDR__:	.word $0801	; If this changes make sure to check the "crunch" and "run" section of the Makefile!

	.segment "STARTUP"
	.word	end_of_basic 	; Link to next line
	.word	2020		; Line number
	.byte	$9e,$20,$32,$30,$36,$34
end_of_basic:	.byte	$00,$00,$00
	nop
	nop
	cld
	jsr	clearbss
	jsr	make_crctable
	lda	#$44
	sta	CRC
	ldy	#6*2		; 6 BASIC vectors
@l2:	lda	$0300,y
	jsr	update_crc
	dey
	bpl	@l2
	ldy	#16*2		; 16 Vectors
@l1:	lda	$314,y
	jsr	update_crc
	dey
	bpl	@l1
	lda	CRC
	cmp	#UNALTEREDC64
	beq	@ok
	;jsr	WARNING		; removed this because it triggered on some unaltered C64s
@ok:	jmp	_main

WARNING:
	ldx	#0
@l2:	lda	@text,x
	beq	@l1
	jsr	$ffd2
	inx
	bne	@l2
@l1:	jsr	$ffe4		; GETIN [https://sta.c64.org/cbm64krnfunc.html]
	beq	@l1
	rts
@text:	.byte	$93,$11,$11,"WARNING! YOUR KERNAL VECTORS HAVE BEEN",$d
	.byte	"MODIFIED!",$20
	.byte	"THIS MAY CAUSE HAVOC!",$D
	.byte	$11,$11
	.byte	"PRESS ANY KEY TO CONTINUE!",0

clearbss:
	lda	#<__BSS_LOAD__ ; Set pointer to the beginning of BSS
	sta	bssptr+1
	lda	#>__BSS_LOAD__
	sta	bssptr+2
	lda	#CLEAR_VALUE	; Clear value
	ldx	#0		; Clear X
	ldy	#>__BSS_SIZE__ ; Number of pages to clear
	beq	less_than_256
bssptr:	sta	$aaaa,x		; Clear a page
	dex
	bne	bssptr
	inc	bssptr+2	; Increment to next page.
	dey			; Decrement page counter
	bne	bssptr		; Loop
less_than_256:
	;;  See https://github.com/cc65/cc65/blob/master/libsrc/common/zerobss.s
	lda	bssptr+2	; Get high byte.
	sta	restptr+2	; Copy to the pointer to clear the rest.
	lda	#<__BSS_LOAD__ ; Pointer LO byte.
	sta	restptr+1
	lda	#CLEAR_VALUE	; Clear value
	ldx	#<__BSS_SIZE__ ; Remaining number of bytes.
	beq	out		  ; If zero then we are done.
restptr:	sta	$AAAA,x
	dex
	bne	restptr
out:
	nop
	nop
	nop
	rts
