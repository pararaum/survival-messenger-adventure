	.export	static_image_colourram
	.export	static_image_screenram
	.export static_image_bitmap
	.export static_image_background
	.export spritescroller

	.segment	"UNDERIO"
static_image_screenram:
	.incbin	"cathedral_cove.kla",2+8000,1000
	.res	16		; empty
	;; This is $ce00 (/ (- #xce00 #xc000) 64)56
	.byte	56,57,58,59,60,61,62,63

	.segment	"SPRTMEM"
spritescroller:
	.res	8*64

	.segment	"BITMAP"
static_image_bitmap:
	.incbin	"cathedral_cove.kla",2,8000
	
	.data
static_image_colourram:
	.incbin	"cathedral_cove.kla",2+8000+1000,1000

static_image_background:
	.incbin	"cathedral_cove.kla",2+8000+1000+1000,1
