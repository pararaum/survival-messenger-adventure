#include <assert.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "qcode.h"
#include "general_io.h"
#include "data_structures.h"

void qcode_display_img(uint8_t i) {
  gio_show_image(i);
}


counterval_t qcode_get_counter(uint8_t i) {
  return counters[i];
}


void qcode_set_counter(uint8_t i, counterval_t x) {
  counters[i] = x;
}


void qcode_take_unconditional(unsigned int i) {
  assert(i < objects_N);
  objects[i].pos = OBJ_INVENTORY;
}


Object_t *qcode_object_ptr(unsigned int i) {
  Object_t *ret = NULL;
  assert(i < objects_N);
  ret = &objects[i];
  return ret;
}


void qcode_object_name(unsigned int j) {
  const char *s;
  Object_t *optr = qcode_object_ptr(j);

  if(optr->adjective != PO_UNKNOWN) {
    s = find_adjective_by_no(optr->adjective);
    assert(s != NULL); // This should never happen and must be bad game data.
    gio_putstr(s);
    gio_putchar(' ');
  }
  gio_putstr(optr->name);
}


unsigned int qcode_object_position(unsigned int i) {
  assert(i < objects_N);
  return objects[i].pos;
}



bool qcode_object_available(unsigned int i) {
  assert(i < objects_N);
  return (objects[i].pos == OBJ_INVENTORY) || (objects[i].pos == here);
}


bool qcode_object_carried(unsigned int i) {
  assert(i < objects_N);
  return objects[i].pos == OBJ_INVENTORY;
}


bool qcode_object_here(unsigned int i) {
  assert(i < objects_N);
  return objects[i].pos == here;
}


void qcode_bring_object(unsigned int obj, unsigned int pos) {
  assert(obj < objects_N);
  objects[obj].pos = pos;
}


void qcode_message(unsigned int i) {
  assert(i < messages_N);
  // No free is necessary as messages are decrunched in a static buffer.
  gio_putstr(uncompress_text(messages[i]));
}


void qcode_message_string(const char *s) {
  // Word wrapping considers the underscore '_' as a whitespace (and
  // breaks the string there). We do not need to do here any copying,
  // etc. This is done in the I/O function.
  gio_putstr(s);
}


void qcode_describe_long(unsigned int i) {
  const uint8_t *desc;

  assert(i < locations_N);
  desc = locations[i].desc_long;
  gio_putstr(uncompress_text(desc));
}


void qcode_describe_short(unsigned int i) {
  assert(i < locations_N);
  gio_putstr(locations[i].desc_short);
}


void qcode_list_exits_only(unsigned int ri) {
  const Location_t *p = &locations[0] + ri; //ith location/room
  uint8_t i;
  uint8_t j;
  bool any = false;

  // Loop over all movement verbs.
  for(i = 0; i <= VERB_MAXMOVE; ++i) {
    // Location has a connection?
    if(p->exits[i] != LOC_NOCONN) {
      if(any) { // If there was a previous verb separate by comma.
	gio_putchar(',');
	gio_putchar(' ');
      }   
      any = true;
      // Now find first name...
      for(j = 0; j < verbs_N; ++j) {
        if(verbs[j].no == i) {
          //...and print it.
          gio_putstr(verbs[j].verb);
          break; //Only first occurence.
        }
      }
    }
  }
  // If there are no obvious exits print "None".
  if(!any) {
    gio_putstr("None");
  }
}


void qcode_exits(unsigned int ri) {
  qcode_message(MESS_EXITS);
  gio_space();
  qcode_list_exits_only(ri);
}


const Ignorable_t *find_ignorable(const char *s) {
  const Ignorable_t *ptr;

  for(ptr = &ignorables[0]; ptr->ignorable != NULL; ++ptr) {
    if(strcmp(ptr->ignorable, s) == 0) {
      return ptr;
    }
  }
  return NULL;
}

const char *find_adjective_by_no(uint8_t i) {
  uint8_t j;

  for(j = 0; j < adjectives_N; ++j) {
    if(adjectives[j].no == i) {
      return adjectives[j].adjective;
    }
  }
  return NULL;
}



void qcode_visible_objects(uint8_t ri) {
  unsigned int i;
  const char *as;
  bool any = false;

  gio_putstr("You can see:");
  for(i = 0; i < objects_N; ++i) {
    if(objects[i].pos == ri) {
      if(any) {
	gio_putchar(',');
	gio_putchar(' ');
      } else {
	gio_putchar(' ');
	any = true;
      }
      as = find_adjective_by_no(objects[i].adjective);
      assert((PO_UNKNOWN == objects[i].adjective) || (as != NULL));
      if(as != NULL) {
	gio_putstr(as);
	gio_putchar(' ');
      }
      gio_putstr(objects[i].name);
    }
  }
  if(!any) {
    gio_putchar(' ');
    gio_putstr("nothing.");
  }
  gio_nl();
}

void qcode_describe_object(unsigned int i) {
  const uint8_t *desc;

  assert(i < objects_N);
  desc = objects[i].desc;
  gio_putstr(uncompress_text(desc));
}


void qcode_exchange_object(uint8_t x, uint8_t y) {
  uint8_t tmp = objects[x].pos;
  objects[x].pos = objects[y].pos;
  objects[y].pos = tmp;
}


// #############################################################################


static int stack[14];
static uint8_t stackpos;

static int sttop(void) {
  assert(stackpos < sizeof(stack) / sizeof(int));
  return stack[stackpos];
}

static void stpush(int i) {
  assert(stackpos > 0);
  stack[--stackpos] = i;
}

static int stpop(void) {
  int i = stack[stackpos];

  assert(stackpos < sizeof(stack) / sizeof(int));
#ifndef NDEBUG
  stack[stackpos] = 128; // Middle value to help debugging. It can be recognised.
#endif
  ++stackpos;
  return i;
}

static void stinit(void) {
#ifndef NDEBUG
  uint8_t i;
  for(i = 0; i < sizeof(stack) / sizeof(int); ++i) {
    stack[i] = -1; // Value to signal non-used elements.
  }
#endif
  stackpos = sizeof(stack) / sizeof(int);
}

#define BROKEN_QCODE    fprintf(stderr, "broken qcode: %s %d %d\n", token, x, y); abort();

static uint8_t qcode_machine_find_matching_parenthesis(char **tokptr, uint8_t begin, uint8_t end) {
  while(begin < end) {
    if(tokptr[begin][0] == ')') {
      return begin;
    } else if(tokptr[begin][0] == '(') {
      begin = qcode_machine_find_matching_parenthesis(tokptr, begin + 1, end);
    }
    ++begin;
  }
#ifndef NDEBUG
  fprintf(stderr, "Tokens exhausted while looking for matching parenthesis.\n");
#endif
  abort();
}

/*! \brief final call in qcode machine
 *
 * As long this is zero the loop continues but the 'f' command sets
 * this variable to one and signals the breaking of the loop and call
 * stack unrolling.
 */
static int8_t qcode_machine_final;

/*! \brief This is the QCode machine
 *
 * \warning Stack has to be initialised elsewhere! See run function!
 * \warning check the local scope variable qcode_machine_final!
 * 
 * \param tokptr array of token pointers
 * \param begin index of first token
 * \param end index after last token
 * \return 0 = not finished, 1 = finished
 */
static int qcode_machine(uint8_t depth, char **tokptr, uint8_t begin, uint8_t end) {
  int x, y, r;
#ifdef DEBUG
  unsigned int i;
#endif
  char *token;

  for(;!qcode_machine_final && (begin < end); ++begin) {
    token = tokptr[begin];
#ifdef DEBUG
    printf("QCODE: '%s'@%d, begin=%d, end=%d, stackpos=%d: ", token, depth, (int)begin, (int)end, (int) stackpos);
    for(i = 0; i < sizeof(stack) / sizeof(int); ++i) {
      printf(" %d", (int) (stack[i]));
    }
    putchar('\n');
#endif
    switch(token[0]) {
    case '': //EOF
      token = NULL; // We ended...
      continue;
      break;

    case '0':
    case '1':
    case '2':
    case '3':
    case '4':
    case '5':
    case '6':
    case '7':
    case '8':
    case '9':
      stpush(atoi(token));
      break;

    case '#': // Display an image(?).
      x = stpop();
      qcode_display_img(x);
      break;

    case '\'': // Direct string to make programming easier.
      qcode_message_string(token + 1);
      break;

    case '!': //logical not
      x = stpop();
      stpush(!x);
      break;

    case '(': // Open a group.
      // Push the current position plus one onto the stack. The
      // function is called recursively and we need to start execution
      // within the parenthesis.
      stpush(begin + 1);
      begin = qcode_machine_find_matching_parenthesis(tokptr, begin + 1, end);
      stpush(begin); // Push the end on the stack.
       // No skipping of the closing parenthesis is necessary as the
       // loop restarts and it will be skipped automatically..
      break;

    case ')': // Close group
      if(depth == 0) {
	// At this depth this is an error.
	BROKEN_QCODE;
      } else {
	BROKEN_QCODE;
      }
      break;

    case 'y': // Execute conditional
      x = stpop(); // End of execution group.
      y = stpop(); // Beginning of execution group.
      assert(x > y);
      switch(token[1]) {
      case '?':
	// Get the condition from the stack.
	if(stpop()) {
	  stpush(qcode_machine(depth + 1, tokptr, y, x));
	}
	break;
      case '~':
	// Get the condition from the stack.
	if(!stpop()) {
	  stpush(qcode_machine(depth + 1, tokptr, y, x));
	}
	break;
      default:
	BROKEN_QCODE;
      }
      begin = x + 1;
      break;
 
    case '&': // logical and of top two values on stack
      x = stpop();
      y = stpop();
      stpush(x && y);
      break;

    case '|': // logical or of top two values on stack
      x = stpop();
      y = stpop();
      stpush(x || y);
      break;

    case '+':
      x = stpop();
      y = stpop();
      stpush(x + y);
      break;

    case '-': //	sub
      x = stpop();
      y = stpop();
      stpush(y - x);
      break;

    case '=': // compare, cmp, equal
      x = stpop();
      y = stpop();
      stpush(x == y);
      break;

    case '<': // compare, cmp, less then
      x = stpop();
      y = stpop();
      stpush(x < y);
      break;

    case '>': // compare, cmp, greater then
      x = stpop();
      y = stpop();
      stpush(x > y);
      break;

    case 'b': //Bring object to: <room> <object> b
      y = stpop();
      x = stpop();
      qcode_bring_object(y, x);
      break;

    case 'c': // Set counter to value: <value> <counter> c
      y = stpop();
      if((y >= 256) || (y < 0)) {
	BROKEN_QCODE;
      }
      x = stpop();
      qcode_set_counter(y, x);
      break;

//      //******************************************** game commands
    case 'd': // describe rooms, objects, etc
      x = stpop(); // Number of object, room, ...
      switch(token[1]) {
      case 'n': // Object name
	qcode_object_name(x);
	break;
      case 'o': // Object
	qcode_describe_object(x);
	break;
      case 'r': // Room, long
	qcode_describe_long(x);
	break;
      case 's': // Room, short
	qcode_describe_short(x);
	break;
      default:
	abort();
      }
      break;

    case 'e': // Print exits
      qcode_exits(stpop());
      break;

    case 'f': // Finalise engine, will return a positive bool and finish the qcode interpretation.
      qcode_machine_final = 1;
      return true;
      break;

    case 'h': // Move player to a new location, changes here.
      here = stpop();
      break;

    case 'i': // Visit a room.
      x = stpop();
      room_visit(x);
      break;

    case 'm': //write message
      x = stpop();
      qcode_message(x);
      break;

    case 'n': //New line...
      gio_nl();
      break;

    case 'p': // Print a character following this one.
      switch(token[1]) {
      case 0:
	BROKEN_QCODE;
	break;
      default:
	gio_putchar(token[1]);
      }
      break;

    case 's': // Set a marker to a value: <value (true/false)> <marker> s
      x = stpop(); // Marker number
      y = stpop(); // Value
      set_marker(x, y);
      break;

    case 't': // Take object.
      x = stpop(); // Object number.
      qcode_take_unconditional(x);
      break;

    case 'v': // Print visible (and obvious) objects
      qcode_visible_objects(stpop());
      break;

    case 'w': // white space
      gio_space();
      break;

    case 'x': // Exchange objects.
      x = stpop(); // Object number
      y = stpop(); // Object number
      qcode_exchange_object(x, y);
      break;

      // ###

    case 'A': //adj1 or adj2
      switch(token[1]) {
      case '1':
	stpush(adj1);
	break;
      case '2':
	stpush(adj2);
	break;
      case 'o':
	x = stpop();
	stpush(qcode_object_ptr(x)->adjective);
	break;
      default:
	BROKEN_QCODE;
      }
      break;

    case 'C': // Get counter value
      x = stpop();
      if((x >= 256) || (x < 0)) {
	BROKEN_QCODE;
      }
      stpush(qcode_get_counter(x));
      break;

    case 'D': // Duplicate top-most element of stack.
      stpush(sttop());
      break;

    case 'H': //here
      stpush(here);
      break;

    case 'I': // Check if a room has been visited.
      x = stpop();
      stpush(room_visited(x));
      break;

    case 'V': //verb
      stpush(verb);
      break;

//    case pcode_load_adverb: //adverb
//      L.stack.push(adv.adverb);
//      break;
    case 'O': //obj1 or 2 or other object functions
      switch(token[1]) {
      case '1': // Get object number of obj1 or obj2.
	stpush(obj1);
	break;
      case '2':
	stpush(obj2);
	break;
      case 'a': // Object available?
	x = stpop(); // Object number from stack.
	stpush(qcode_object_available(x));
	break;
      case 'c': // Object carried?
        x = stpop(); // Object number from stack.
        stpush(qcode_object_carried(x));
	break;
      case 'p': // Get object position.
        x = stpop(); // Object number from stack.
        stpush(qcode_object_position(x));
	break;
      default:
	abort();
      }
      break;

//    case pcode_load_obj_weight:
//      L.check_min_elements(1);
//      x = L.stack.top();
//      L.stack.pop();
//      L.stack.push(adv.objects.at(x).weight);
//      break;
//      //******************************************** store
    case 'S': // Get switch or marker.
      y = stpop();
      if((y > 255) || (y < 0)) {
	BROKEN_QCODE;
      }
      stpush(get_marker(y));
      break;
    default:
      fprintf(stderr, "Unknown Qcode '%s' $%02X.\n", token, (int) (token[0]));
      abort();
    }
    if(token == NULL) {
      fprintf(stderr, "Premature Qcode end.\n");
      abort();
    }
  }
  r = 0;
  // Default is to return false, if there is an element on the stack
  // then use its value. Popping is not necessary as the stack will be
  // initialised on the next run anyway.
  if(stackpos < sizeof(stack) / sizeof(int)) {
    r = sttop();
  }
  return r;
}


int qcode_machine_run(const char *s) {
  char *buf;
  char *bufptr;
  char **tokptr;
  uint8_t idx = 0;
  const unsigned int maxTokPtr = 64;
  int ret = 0;

  stinit();
  qcode_machine_final = 0;
  tokptr = malloc(sizeof(char *) * maxTokPtr);
#if defined(DEBUG) && defined(__C64__)
  printf("buf=%p, avail=%u\n", buf, _heapmemavail());
#endif
  buf = strdup(s);
  if(buf != NULL) {
    bufptr = strtok(buf, " ");
    while(bufptr != NULL) {
      if(idx == 64) {
	printf("%s>64\n", s);
	abort(); // Too many tokens;
      }
      tokptr[idx++] = strdup(bufptr);
      bufptr = strtok(NULL, " ");
    }
    free(buf); // Not needed any longer, free now.
    ret = qcode_machine(0, tokptr, 0, idx);
    do {
      free(tokptr[--idx]);
    } while(idx != 0);
  }
  free(tokptr);
  return ret;
}


bool qcode_save_as(const char *name) {
  unsigned int size = 0;
  void *data;

  data = generate_save_data(NULL, &size);
  if(data) {
    gio_write_savedata(data, size, name);
  } else {
    return false;
  }
  free(data);
  return true;
}


bool qcode_save(void) {
  char buf[44];
  char *ptr;
  long l;

  ptr = gio_readline(buf, sizeof(buf) - 1, "Game slot? ");
  if(ptr == NULL) {
    l = 0;
  } else {
    l = atol(buf);
  }
  sprintf(buf, "game.%08lx", l);
  return qcode_save_as(buf);
}


bool qcode_load_from(const char *name) {
  void *data;
  unsigned int size = get_size_save_data();
  bool ret = false;

  data = malloc(size);
  if(data) {
    if(gio_read_savedata(data, size, name)) {
      if(interpret_save_data(data)) {
	ret = true;
      } else {
	puts("Error while loading!");
      }
    }
  }
  free(data);
  return ret;
}


bool qcode_load(void) {
  char buf[44];
  char *ptr;
  long l;

  ptr = gio_readline(buf, sizeof(buf) - 1, "Game slot? ");
  if(ptr == NULL) {
    l = 0;
  } else {
    l = atol(buf);
  }
  sprintf(buf, "game.%08lx", l);
  return qcode_load_from(buf);
};

