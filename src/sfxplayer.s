;;; make AS=ca65 AFLAGS="-I~/Emu/c64/coding/c64playground/includeCC65/" sfxplayer,prg

	CLEAR_VALUE=$00
	GETIN =	$FFE4

	.import	__BSS_LOAD__
	.import __BSS_SIZE__
	.macpack cbm

	.segment "MUZAK"
_muzak_sid:
	.incbin	"empty.sid", $7c+$2

	.segment "LOADADDR"
	.export __LOADADDR__
__LOADADDR__:	.word $0801	; If this changes make sure to check the "crunch" and "run" section of the Makefile!

	.segment "STARTUP"
	.word	end_of_basic 	; Link to next line
	.word	2020		; Line number
	.byte	$9e,$20,$32,$30,$36,$34
end_of_basic:	.byte	$00,$00,$00
	nop
	nop
	cld
	jsr	clearbss
	jsr	_main
	jmp	64738

clearbss:
	lda	#<__BSS_LOAD__ ; Set pointer to the beginning of BSS
	sta	bssptr+1
	lda	#>__BSS_LOAD__
	sta	bssptr+2
	lda	#CLEAR_VALUE	; Clear value
	ldx	#0		; Clear X
	ldy	#>__BSS_SIZE__ ; Number of pages to clear
	beq	less_than_256
bssptr:	sta	$aaaa,x		; Clear a page
	dex
	bne	bssptr
	inc	bssptr+2	; Increment to next page.
	dey			; Decrement page counter
	bne	bssptr		; Loop
less_than_256:
	;;  See https://github.com/cc65/cc65/blob/master/libsrc/common/zerobss.s
	lda	bssptr+2	; Get high byte.
	sta	restptr+2	; Copy to the pointer to clear the rest.
	lda	#<__BSS_LOAD__ ; Pointer LO byte.
	sta	restptr+1
	lda	#CLEAR_VALUE	; Clear value
	ldx	#<__BSS_SIZE__ ; Remaining number of bytes.
	beq	out		  ; If zero then we are done.
restptr:	sta	$AAAA,x
	dex
	bne	restptr
out:	rts


	.code
setup_irq:
	lda     #$7f
        sta     $dc0d   ;disable timer interrupts which can be generated by the two CIA chips
        sta     $dd0d   ;the kernal uses such an interrupt to flash the cursor and scan the keyboard,
                        ;so we better stop it.
        lda     $dc0d   ;by reading this two registers we negate any pending CIA irqs.
        lda     $dd0d   ;if we don't do this, a pending CIA irq might occur after we finish setting up our irq.
                        ;we don't want that to happen.
        ;; https://www.c64-wiki.com/wiki/Raster_interrupt
        lda     #$7f
        and     $d011           ; Set MSB of raster to zero
        sta     $d011
        lda     #50-1           ; Top of screen minus one rasterline
        sta     $d012
        ;; http://unusedino.de/ec64/technical/project64/mapping_c64.html
        ; 53274         $D01A          IRQMASK
        ; IRQ Mask Register
        ; 
        ; Bit 0:  Enable Raster Compare IRQ (1=interrupt enabled)
        ; Bit 1:  Enable IRQ to occure when sprite collides with display of
        ;   normal
        ;         graphics data (1=interrupt enabled)
        ; Bit 2:  Enable IRQ to occur when two sprites collide (1=interrupt
        ;   enabled)
        ; Bit 3:  Enable light pen to trigger an IRQ (1=interrupt enabled)
        ; Bits 4-7:  Not used
        lda     #%00000001
        sta     $d01a
	lda	$314
	sta	oldirq
	lda	$314+1
	sta	oldirq+1
	lda	#<irq_routine
	sta	$314
	lda	#>irq_routine
	sta	$315
	rts

restore_irq:
	lda	oldirq
	sta	$314
	lda	oldirq+1
	sta	$315
	rts

irq_routine:
	dec	$d020
	jsr	_muzak_play
	inc	$d020
	asl	$d019
	jmp	(oldirq)

	.bss
oldirq:	.res	2

	.code
sfx_initial:
	.incbin	"powerup01.bin"


sfx_00:	.scope
	;-config
TONE	= $c0
TONE1	= TONE+0
TONE2	= TONE+12
;-
	.byte $00	;attack / decay
	.byte $f9	;sustain / release
	.byte $08	;pulse width in reverse order, no pulsewidth modulation
	;wavetable: note/waveform pairs
	;values $01-$81 are waveforms, values $82-$DF are absolute notes D-0 - B-7
	.byte $c0,$41
	.byte TONE1
	.byte TONE1
	.byte TONE2
	.byte TONE2
	.byte TONE1
	.byte TONE1
	.byte TONE2
	.byte TONE2
	.byte TONE1
	.byte TONE1
	.byte TONE2
	.byte TONE2
	;endmarker
	.byte $00
	.endscope
	;; ⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡

sfx_01:	.scope
;-config
TONE	= $c0
TONE1	= TONE+0
TONE2	= TONE+4
TONE3	= TONE+7
TONE4	= TONE+12
;-
	.byte $00	;attack / decay
	.byte $f9	;sustain / release
	.byte $08	;pulse width in reverse order, no pulsewidth modulation
	;wavetable: note/waveform pairs
	;values $01-$81 are waveforms, values $82-$DF are absolute notes D-0 - B-7
	.byte $c0,$41
	.byte TONE1
	.byte TONE1
	.byte TONE1
	.byte TONE1
	.byte TONE2
	.byte TONE2
	.byte TONE2
	.byte TONE2
	.byte TONE3
	.byte TONE3
	.byte TONE3
	.byte TONE3
	.byte TONE4
	.byte TONE4
	.byte TONE4
	.byte TONE4
	;endmarker
	.byte $00
	.endscope
	;; ⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡

sfx_02:	.scope
;-config
TONE	= $c0
TONE1	= TONE+12
TONE2	= TONE+0
;-
	.byte $00	;attack / decay
	.byte $f9	;sustain / release
	.byte $08	;pulse width in reverse order, no pulsewidth modulation
	;wavetable: note/waveform pairs
	;values $01-$81 are waveforms, values $82-$DF are absolute notes D-0 - B-7
	.byte $c0,$41
	.byte TONE1
	.byte TONE1
	.byte TONE1
	.byte TONE1
	.byte TONE1
	.byte TONE1
	.byte TONE1
	.byte TONE1
	.byte TONE2
	.byte TONE2
	.byte TONE2
	.byte TONE2
	.byte TONE2
	.byte TONE2
	.byte TONE2
	.byte TONE2
	;endmarker
	.byte $00
	.endscope
	;; ⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡

sfx_03:	.scope
;-config
TONE	= $c0
TONE1	= TONE+12
TONE2	= TONE+7
TONE3	= TONE+4
TONE4	= TONE+0
;-
	.byte $00	;attack / decay
	.byte $f9	;sustain / release
	.byte $08	;pulse width in reverse order, no pulsewidth modulation
	;wavetable: note/waveform pairs
	;values $01-$81 are waveforms, values $82-$DF are absolute notes D-0 - B-7
	.byte $c0,$41
	.byte TONE1
	.byte TONE1
	.byte TONE1
	.byte TONE1
	.byte TONE2
	.byte TONE2
	.byte TONE2
	.byte TONE2
	.byte TONE3
	.byte TONE3
	.byte TONE3
	.byte TONE3
	.byte TONE4
	.byte TONE4
	.byte TONE4
	.byte TONE4
	;endmarker
	.byte $00
	.endscope
	;; ⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡

sfx_04:	.scope

TONE	= $b0+12
TONE00	= TONE+0
TONE01	= TONE+1
TONE02	= TONE+2
TONE03	= TONE+3
TONE04	= TONE+4
TONE05	= TONE+5
TONE06	= TONE+6
TONE07	= TONE+7
TONE08	= TONE+8
TONE09	= TONE+9
TONE10	= TONE+10
TONE11	= TONE+11
TONE12	= TONE+12

DUR_8	= 3
DUR_4	= DUR_8*2
;-

	.byte $00	;attack / decay
	.byte $f0	;sustain / release
	.byte $f8	;pulse width in reverse order, no pulsewidth modulation
	
	;wavetable: note/waveform pairs
	;values $01-$81 are waveforms, values $82-$DF are absolute notes D-0 - B-7
	.byte $c0,$11

	.repeat DUR_4+DUR_8
	.byte TONE00
	.byte TONE00+12
	.endrepeat

	.repeat DUR_8
	.byte TONE02
	.byte TONE02+12
	.endrepeat

	.repeat DUR_4
	.byte TONE04
	.byte TONE04+12
	.endrepeat

	.repeat DUR_4
	.byte TONE07
	.byte TONE07+12
	.endrepeat

	.repeat DUR_4
	.byte TONE12
	.byte TONE12+12
	.endrepeat

	.repeat DUR_4
	.byte TONE07
	.byte TONE07+12
	.endrepeat

	.repeat DUR_4
	.byte TONE12
	.byte TONE12+12
	.endrepeat
	
	;endmarker
	.byte $00

	.endscope
	;; ⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡


sfx_05:	.scope
TONE	= $b0
TONE00	= TONE+0
TONE01	= TONE+1
TONE02	= TONE+2
TONE03	= TONE+3
TONE04	= TONE+4
TONE05	= TONE+5
TONE06	= TONE+6
TONE07	= TONE+7
TONE08	= TONE+8
TONE09	= TONE+9
TONE10	= TONE+10
TONE11	= TONE+11
TONE12	= TONE+12

DUR_8	= 3
DUR_4	= DUR_8*2
WAVEFORM	= $11
;-
	.byte $00	;attack / decay
	.byte $f0	;sustain / release
	.byte $f8	;pulse width in reverse order, no pulsewidth modulation
	
	;wavetable: note/waveform pairs
	;values $01-$81 are waveforms, values $82-$DF are absolute notes D-0 - B-7
	.byte $c0,$11

	.repeat DUR_4+DUR_8
	.byte TONE12
	.byte TONE12+12
	.endrepeat

	.repeat DUR_8
	.byte TONE11
	.byte TONE11+12
	.endrepeat

	.repeat DUR_8
	.byte TONE10
	.byte TONE10+12
	.endrepeat

	.repeat DUR_8
	.byte TONE09
	.byte TONE09+12
	.endrepeat

	.repeat DUR_4+DUR_8
	.byte TONE08
	.byte TONE08+12
	.endrepeat

	.repeat DUR_8
	.byte TONE07
	.byte TONE07+12
	.endrepeat

	.repeat DUR_8
	.byte TONE06
	.byte TONE06+12
	.endrepeat

	.repeat DUR_8
	.byte TONE05
	.byte TONE05+12
	.endrepeat

	.repeat DUR_4+DUR_8
	.byte TONE04
	.byte TONE04+12
	.endrepeat

	.repeat DUR_8
	.byte TONE03
	.byte TONE03+12
	.endrepeat

	.repeat DUR_8
	.byte TONE02
	.byte TONE02+12
	.endrepeat

	.repeat DUR_8
	.byte TONE01
	.byte TONE01+12
	.endrepeat

	.repeat DUR_4+DUR_4
	.byte TONE00
	.byte TONE00+12
	.endrepeat

	;endmarker
	.byte $00
	.endscope
	;; ⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡


	
	.define SfxTable	sfx_initial,sfx_00,sfx_01,sfx_02,sfx_03,sfx_00,sfx_01,sfx_02,sfx_03,sfx_04,sfx_05
sfxTableLO:	.lobytes	SfxTable
sfxTableHI:	.hibytes	SfxTable

;;; 	void mobile_sound(int sfxno)
;;; Input: A/X sound effect number, if too large it will be adjusted.
;;; Output: -
;;; Modifies: A/X/Y
;;; Actually we directly ignore the high byte... (X)
_mobile_sound:
	cmp	#sfxTableHI-sfxTableLO ; Number of entries in the table aka sound effects.
	bcc	@cont		       ; OK, load address and continue
	;; Carry is set now, so subtract.
	sbc	#sfxTableHI-sfxTableLO
	bcs	_mobile_sound
@cont:
	tax			; Put sound-effect value into X.
	lda	sfxTableLO,x
	ldy	sfxTableHI,x
	ldx	#14		; Channel 3
	jmp	_muzak_sid+6	; Effect!

	.code
_muzak_init:
	lda	#$00
	jmp	_muzak_sid

_muzak_play:
	jmp	_muzak_sid+3

	.code

handle_keys:
	cmp	#'A'
	bcc	@out
	cmp	#'Z'+1
	bcs	@out
	sec
	sbc	#'A'
	jsr	_mobile_sound
@out:
	rts


main_loop:
@wait:	jsr	GETIN
	beq	@skip
	jsr	handle_keys
@skip:
	lda	#%00010000
	and	$dc00		; Joystick fire button
	bne	@wait
@out:	rts

screen_off:
	lda	#$5b
	sta	$d011		; Default text mode (screen control register 1)
	lda	#$08
	sta	$d016		; Single colour (screen control register 2)
	lda	#$15
	sta	$d018		; Default position for charset and screen, again.
	lda	#%00000011
	ora	$dd00
	sta	$dd00		; Default bank.
	rts

	.data
text:
	.byte	$93,$99
	.byte	"THE 7TH DIVISION SFX PLAYER",13,13
	.BYTE	"PRESS A TO Z FOR A SOUND EFFECT!"
	.byte	0
	.code
_main:
	sei
	lda	#0
	sta	$d020
	sta	$d021
	jsr	_muzak_init
	jsr	setup_irq
	cli
	lda	#<text
	ldy	#>text
	jsr	$ab1e		; STROUT
	jsr	main_loop
	rts

	
