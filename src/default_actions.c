#include <stdlib.h>
#include <assert.h>
#include <stdio.h>
#include <string.h>
#include "default_actions.h"
#include "data_structures.h"
#include "qcode.h"
#include "general_io.h"

bool action_take_object(unsigned int obj) {
  if(objects[obj].weight == OBJ_TOOHEAVY) {
    gio_putstr("You can not carry that!");
  } else if(objects[obj].pos == OBJ_INVENTORY) {
    gio_putstr("You already have that!");
  } else if(objects[obj].pos != here) {
    gio_putstr("I do not see that here.");
  } else {
    objects[obj].pos = OBJ_INVENTORY;
  }
  return true;
}


bool action_take_object1(void) {
  if((adj1 != ADJ_ANY) && (objects[obj1].adjective != adj1)) {
    gio_putstr("Your adjective noun combination confuses me!");
    return true;
  }
  return action_take_object(obj1);
}

bool action_drop_object1(void) {
  if(obj1 == EVENT_NOTP) {
    return false;
  }
  if(qcode_object_carried(obj1)) {
    qcode_bring_object(obj1, here);
  } else {
    gio_putstrln("You are not carrying that!");
  }
  return true;
}

bool action_inventory(void) {
  bool nothing = true;
  unsigned int j;

  gio_putstr("You carry:");
  for(j = 0; j < objects_N; ++j) {
    if(objects[j].pos == OBJ_INVENTORY) {
      if(!nothing) { // Not the first one, separate by comma.
	gio_putchar(',');
      }
      gio_putchar(' ');
      qcode_object_name(j);
      nothing = false;
    }
  }
  if(nothing) {
    gio_putchar(' ');
    gio_putstr("nothing");
  }
  return true;
}


bool action_default_high_pri_event(void) {
  bool visited;

  visited = room_visited(here);
#ifndef NDEBUG
  printf("here=%d visited=%d", here, (int)visited);
#endif
  if(visited) {
    gio_putstrln(locations[here].desc_short);
    qcode_exits(here);
    gio_nl();
  } else {
    room_visit(here);
    qcode_describe_long(here);
    gio_space();
    qcode_visible_objects(here);
    qcode_exits(here);
    gio_nl();
  }
  return true;
}
