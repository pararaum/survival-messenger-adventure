#ifndef __QCODE_H_2020__
#define __QCODE_H_2020__
#include <stdint.h>
#include <stdbool.h>
#include "data_structures.h"

/*! \file qcode.h
 *
 * These are the function which make up the QCODE (Quick-'P'-Code) for
 * managing interaction with the game environment. They will be
 * extended so that the whole configuration, etc. can be managed via
 * this codes.
 *
 * The qcode is a simple forth-like language. It is used in the
 * Event_t. The stack is currently limit to 32 values. The input
 * string is separated at white spaces and then each command (usually
 * a short one or two character sequence) is executed.
 *
 * \warning In debugging mode problems with the code will lead to an
 * abortion of execution. In No-Debug mode the code will silently
 * continue and corrupt the whole game.
 *
 * \section commanddescription Command description
 *
 * \subsection comdes_special Special commands
 *
 *  * Any number entered is pushed onto the stack. Examples are "0", "1", "43". 
 *  * The EOF character ends the input.
 *
 *  \subsubsection Grouping commands (action '(' and ')')
 *
 *  This command pushes the position of the first and the last qcode
 *  onto the stack. The qcode execution is skipped until the closing
 *  parenthesis. This can be used to generate condition code
 *  execution.
 *
 *  \subsubsection Execution of groups
 *
 *  Execute pcode in a subcontext.
 *
 *   * y? Execute group if condition is true
 *   * y~ Execute group if condition is false
 *
 *  As an example we would like to print message 6 is obj1 is here
 *  otherwise we print message 9. The corresponding qcode looks like
 *  this: "O1 Op H = ( 6 m f ) x? 9 m f". The 'f' code signalises to
 *  end further interpreting.
 * 
 * \subsection comdes_actions Action commands
 * 
 * \subsubsection comdes_actions_plus Addition (action '+')
 *
 * This adds the two top most items on the stack and puts the result
 * on the stack. No overflow checking is done.
 * 
 * \subsubsection comdes_actions_compare Comparison (action '=')
 *
 * This compares the two top most items on the stack and puts the
 * result as a boolean value onto the stack. Zero means false, one is
 * true.
 *
 * \subsubsection comdes_actions_describe Describe location (action 'd')
 *
 * Pop the location number from the stack and output its
 * description. To describe the current location and end
 * successfully use "H d f".
 *
 * \subsubsection comdes_actions_exits Print location exits (action 'e')
 *
 * Pop the location number from the stack and output its
 * exits. To describe the current location's exits end
 * successfully use "H e f".
 *
 * \subsubsection comdes_actions_finalise Finalise (action 'f')
 *
 * Finalise the current actions. No further parsing is done and the
 * action finishes successful. This will also stop any pattern
 * matching of further \ref Event_t events.
 *
 * \subsubsection comdes_actions_visit Visit a room (action 'i')
 *
 * Mark a room as visited. Room number is taken from top of stack.
 *
 * \subsubsection comdes_actions_message Print message (action 'm')
 *
 * Print a message. The message number is taken from the stack. Make sure that a corresponding message exists in \ref messages. 
 * 
 * \subsubsection comdes_actions_newline Print single newline character (action 'n')
 *
 * This ends the current line and prints a newline character. So if
 * you like to print a message followde be a carriage return you would
 * need a sequence like "99 m n".
 *
 * \subsubsection comdes_actions_markerset Set a marker (action 's')
 *
 * A marker is set to a boolean value, the topmost element on the
 * stack is the number of the marker and next element is the value
 * (converted to a boolean). So setting the 42th marker to false is
 * done via "0 42 s".
 *
 *
 * 
 * \subsection comdes_queries Queries to the game system
 *
 * \subsubsection comdes_queries_here Query player position (query 'H')
 *
 * Get current player position (room or location number) and push it
 * on the stack.
 * 
 * \subsubsection comdes_queries_verb Query current verb (query 'I')
 *
 * Check if a room was visited. The room number is taken from the top
 * of the stack.
 *
 * \subsubsection comdes_queries_verb Query current verb (query 'V')
 *
 * Get the current verb number and push it on the stack.
 * 
 * \subsubsection comdes_queries_object Query object information
 *
 * Here, multiple queries can be performed:
 *
 * \li O1: get the number of the first parsed object.
 * \li O2: get the number of the second parsed object.
 * \li Op: Pop object number from stack and push the current location of the object onto the stack.
 * 
 * \subsubsection comdes_queries_adjectiv Query adjective/adverb information
 *
 * Here, multiple queries can be performed:
 *
 * \li A1: get the number of the first parsed adjective.
 * \li A2: get the number of the second parsed adjective.
 * 
 */


/*! \brief Get counter value
 *
 * \param i which counter
 * \return value of counter
 */
counterval_t qcode_get_counter(uint8_t i);

/*! \brief Set counter value
 *
 * \param i which counter
 * \param x new value of counter
 */
void qcode_set_counter(uint8_t i, counterval_t x);

/*! \brief Find adjective name by number.
**
** As the adjectives are stored in an unsorted array of name to number
** pairs (which may have even holes in them) the adjective name has to
** be found for a adjective number. This function will search and
** return the adjective name.
**
** \param i adjective number
** \return name of adjective or NULL
*/
const char *find_adjective_by_no(uint8_t i);


/*! \brief Find an ignorable by name.
 *
 * \param s name of string
 * \return pointer to ignorable or NULL if the string does not correspond to an ignorable
 */
const Ignorable_t *find_ignorable(const char *s);


/*! \brief print a message
 *
 * It is asserted that i is smaller then the number of possible
 * message numbers. If compiled with NDEBUG the behaviour is undefined
 * if the number exceeds the maximum message number.
 *
 * \param i message number
 */
void qcode_message(unsigned int i);


/*! \brief print a message string
 *
 * Message strings are a special form of the strings in which
 * underscores will be replaced by spaces.
 *
 * \param s string to print
 */
void qcode_message_string(const char *s);


/*! \brief Describe an object
 *
 * \param i object number
 */
void qcode_describe_object(unsigned int i);


/*! \brief Describe a room
 *
 * This prints the long description associated with the room.
 * 
 * \param i room number
 */
void qcode_describe_long(unsigned int i);


/*! \brief print whole object name
 *
 * This will print the whole object name, this will be either "key" or
 * "golden key" if it has an adjective.
 *
 * \param i object number
 */
void qcode_object_name(unsigned int j);

/*! \brief Exchange the position of two objects.
 *
 * This function can be used quickly interchange two objects. IT is
 * best use for replacing the "normal lamp" with the "lit lamp",
 * whereas the lit lamp is previously in the nowhere position.
 *
 * \param x first object's number
 * \param y second object's number
 */
void qcode_exchange_object(uint8_t x, uint8_t y);


/*! \brief print exits of a location
 *
 * \param i room number
 */
void qcode_exits(unsigned int i);

/*! \brief print visible objects
 *
 * Print all the obvious objects.
 *
 * \param ri room number
 */
void qcode_visible_objects(uint8_t ri);

/*! \brief run the correspondig Qcode
 *
 * The qcode is a simplified version of a forth-like language. It can
 * be used to describe simple conditions and actions. For more complex
 * actions consider using an action function.
 *
 * The last value on the stack is returned, if there are no elements
 * on the stack a zero value (false) is returned. If the code was
 * ended with a 'f' action (fin) then one (true) is returned.
 *
 * \param s string with commands
 * \return 1 = successful run and end of action, 0 = not successful (e.g. condition not met), continue parsing.
 */
int qcode_machine_run(const char *s);

/*! Bring object to location
 *
 * This function moves an object to a room.
 *
 * \param obj object number
 * \param pos position to move object to
 */
void qcode_bring_object(unsigned int obj, unsigned int pos);


/*! \brief Is the object available?
 *
 * Is the object either in the player's possession or is it here?
 *
 * \param i object number
 */
bool qcode_object_available(unsigned int i);

/*! Is the object being carried?
 *
 * \param i object number
 */
bool qcode_object_carried(unsigned int i);


/*! Is the object being here?
 *
 * \param i object number
 */
bool qcode_object_here(unsigned int i);

/*! Save the game under a name
 *
 * \param fname file name to save the game under.
 * \return true on successful
 */
bool qcode_save_as(const char *fname);

/*! Save the game after asking for a number
 *
 * \return true on successful
 */
bool qcode_save(void);

/*! Load the game after asking for a number
 *
 * \return true on successful
 */
bool qcode_load(void);
#endif
