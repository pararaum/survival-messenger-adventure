#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "c64_mobilemoiety.h"
#include "gfx_functions.h"
#include "data_structures.h"

#define MAXPERSONALITIES 5

struct AnnoyPerLocation {
  uint8_t per;
  uint8_t loc;
  const char *txt;
};

struct AnnoyGeneral {
  uint8_t num;
  const char **txt;
};

extern volatile uint8_t irqcntr;
static uint8_t msgcntr = 0;
static unsigned short int lfsrval = 58;

#pragma data-name (push, "LOWCODE")
static const char *personalityW_annoying_messages[] = {
  "Search everywhere!",
  "Try harder!",
  "Be careful!",
  "Look closely!"
};

static const char *personalityL_annoying_messages[] = {
  "I have to pee.",
  "BRB...",
  "I am tired.",
  "Now a sensible action: go to bed!",
  "AFK",
  "Hello! Can you hear me?"
};


static const char *personalityM_annoying_messages[] = {
  "Brb, gotta defeat the boss",
  "Item obtained: Useless Message",
  "What will you do, Player 1?",
  "Level up!",
  "Nice inventory you got there.",
  "My controller died, c u l8er",
  "Where are the special effects?",
  "Think of a break, sitting in front of a screen all day is bad!",
  "Hint: Save often to avoid pain"
};

static const char *personalityV_annoying_messages[] = {
  "Maybe you should go home.",
  "I am scared.",
  "Don't be too brave!",
  "Don't always play the hero!"
};

static const char *personalityP_annoying_messages[] = {
  "What is going on here?",
  "What a douche!",
  "Turn me off!",
  "Turn me on!",
  "Load modules!",
  "Give up!",
  "Nonsense!",
  "My, my, my...",
  "Your failure is obvious to me!"
};
#pragma data-name (pop)

#pragma data-name (push, "LOWCODE")
static struct AnnoyGeneral all_annoying_messages[MAXPERSONALITIES] = {
  { sizeof(personalityW_annoying_messages)/sizeof(const char *), personalityW_annoying_messages },
  { sizeof(personalityL_annoying_messages)/sizeof(const char *), personalityL_annoying_messages },
  { sizeof(personalityM_annoying_messages)/sizeof(const char *), personalityM_annoying_messages },
  { sizeof(personalityV_annoying_messages)/sizeof(const char *), personalityV_annoying_messages },
  { sizeof(personalityP_annoying_messages)/sizeof(const char *), personalityP_annoying_messages }
};
#pragma data-name (pop)

static struct AnnoyPerLocation location_messages[] = {
  { 0, 4, "Rub the lamp?" },
  { 0, 14, "Who puts a sign to a trap?" },
  { 0, 16, "That bush..." },
  { 0, 19, "Looks dangerous!" },
  { 0, 20, "One man's trash..." },
  { 0, 33, "What a view!" },
  { 0, 42, "Beautiful!" }, 
  { 0, 45, "What's in that box?" },
  { 1, 0, "Oh, I am confused." },
  { 1, 1, "I like palm trees. But I hate palm oil!" },
  { 1, 3, "Let's build a sand castle!" },
  { 1, 6, "I am sailing... I am sailing..." },
  { 1, 9, "Oh, a sandbox. Let's play." },
  { 1, 15, "Uff! So many trees.." },
  { 1, 18, "Let's go back." },
  { 1, 22, "I think I get lost here." },
  { 1, 29, "To go, or not to go. Or how to go?" },
  { 1, 31, "Ah, a hotel!" },
  { 2, 7, "Oh, I see so many silicon waver. In raw state." },
  { 2, 15, "I wonder if you can collect herbs" },
  { 2, 17, "Seems like we entered another biome!" },
  { 2, 18, "Is this a beach level?" },
  { 2, 20, "Let's interact with an NPC!" },
  { 2, 25, "Nic ar ivewr." },
  { 2, 26, "I feel like a Game Over is coming soon." },
  { 3, 3, "Please be careful, there could be lots of crabs. Or worse, jelly fish!" },
  { 3, 10, "I told you so!" },
  { 3, 16, "Monkeys can be very dangerous." },
  { 3, 27, "I'm scared of the dark." },
  { 3, 29, "Don't go over the bridge, it's not solid!" },
  { 3, 33, "Don't go near to the cliff!" },
  { 3, 39, "Don't eat the mushrooms!" },
  { 3, 42, "I'm glad you didn't go swimming." },
  { 3, 44, "Don't go there!" },
  { 4, 20, "Under quarantine!" },
  { 4, 21, "You will die here!" },
  { 4, 22, "Do not breathe!" },
  { 4, 23, "Sleep!" },
  { 4, 40, "Sound of Music" },
  { 4, 44, "Wash your smelly feet!" },
  { 4, 46, "Do not touch things you do not understand!" },
  { 255, 255 }
};

#pragma code-name (push, "LOWCODE")
static unsigned short int getlfsr(void) {
  if(lfsrval & 1) {
    lfsrval = (lfsrval >> 1) ^ 0x84E1;
  } else {
    lfsrval >>= 1;
  }
  return lfsrval;
}
#pragma code-name (pop)

static void putwrapped(const char *text) {
  char *ptr;
  size_t l;
  uint8_t column = 0;
  char *buf = strdup(text);

  if(buf == NULL) {
    puts(text);
  } else {
    ptr = strtok(buf, " _");
    while(ptr != NULL) {
      l = strlen(ptr) + 1;
      column += l;
      if(column > 12) { // We need wrapping.
	putchar('\n');
	column = l;
      } else if(column == 12) { // Exact fill, eleven previous characters and a space.
	column = 0;
      } 
      printf("%s ", ptr);
      ptr = strtok(NULL, " _");
    }
  }
  free(buf);
  if(column != 0) {
    putchar('\n');
  }
}


#pragma code-name (push, "HIGHMEM")

void mobile_messagehandler(void) {
  const char *s;
  struct AnnoyPerLocation *ptr;
  uint8_t personality;

  if(get_marker(MOBILEPHONEMARKER)) {
    personality = irqcntr % MAXPERSONALITIES;
    selectwindow2();
    s = NULL;
    if(msgcntr == 0) {
      for(ptr = &location_messages[0]; ptr->loc != 255; ++ptr) {
	if(ptr->loc == here) {
	  set_textcolor(3 + ptr->per);
	  s = ptr->txt;
	  putwrapped(s);
	  msgcntr = 1;
	}
      }
    } else {
      if(msgcntr++ >= 3) {
	msgcntr = 0;
      }
    }
    if(s == NULL) {
      set_textcolor(3 + personality);
      s = all_annoying_messages[personality].txt[getlfsr() % all_annoying_messages[personality].num];
      putwrapped(s);
    }
    mobile_sound(getlfsr());
    selectwindow1();
  } else {
    // This will get really random values.
    lfsrval = (lfsrval + irqcntr) | 0x2000; // Zero as a value should never be allowed.
#if 0
    selectwindow2();
    printf("%04X\n", getlfsr());
    selectwindow1();
#endif
  }
}

#pragma code-name (pop)

void mobile_off(void) {
  uint8_t i;

  set_marker(MOBILEPHONEMARKER, 0);
  selectwindow2();
  for(i = 0; i < 25; ++i) {
    putchar('\n');
  }
  selectwindow1();
}
