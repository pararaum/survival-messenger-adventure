#ifndef __DATA_STRUCUTRES_H_2020
#define __DATA_STRUCUTRES_H_2020
#include <stdint.h>
#include <stdbool.h>

/*! \file 
 *
 * Data structure definition and all the important constants.
 */

/* Event constants */
#define EVENT_NOTP 255 //!< Must not be present!
#define EVENT_MAYBE 254 //!< Anything, may even be missing.
#define EVENT_ANY 253 //!< Anything, but must be present.
#define EVENT_ANYROOM 255 //!< Only valid for the location, if location is EVENT_ANYROOM then this event is handled in all locations.

/* Parse Object Defines */
#define PO_UNKNOWN 255 //!< This is the default value the parser uses and means that no corresponing object, etc. could be found which matched the text.

/* Object definition */
#define OBJ_NOROOM 255 //!< The object has no room.
#define OBJ_INVENTORY 254 //!< The object is in the player's possession.
#define OBJ_TOOHEAVY 255 //!< The objects weight is the maximum and can not be carried therefore.

/* Location constants */
#define LOC_NOCONN 255 //!< Location for no connection.

/* Message constants */
#define MESS_WHATNOW 0 //!< The message to be printed when user input is needed.
#define MESS_NOUNDERSTANING 1 //!< The message saying that the input is not understood.
#define MESS_EXITS 2 //!< The message "Exits are..."

/* Verb constants */
#define VERB_MAXMOVE 3 //!< Maximum verb number which corresponds to a movement.

/* Adjective/Adverb constants */
#define ADJ_ANY 255 //!< This is the value to be used in find_object() is the adjective is not important.

/* Marker constants */
#define MARKER_GAME_ENDED 255 //!< This marker is used to signal the end of the game. If it is set to true then the game loop ends.

extern uint8_t here; //!< position of player
extern uint8_t verb; //!< current verb
extern uint8_t adverb; //!< current adverb
extern uint8_t obj1; //!< first object or PO_UNKNOWN
extern uint8_t adj1; //!< first adjective or PO_UNKNOWN
extern uint8_t obj2; //!< second object or PO_UNKNOWN
extern uint8_t adj2; //!< second adjective or PO_UNKNOWN


/*! \brief QCode is simply a string
 *
 * We use a typedef to be able to change it more easily.
 */
typedef const char *QCode_t;

/*! \brief Action function pointer
 *
 * This action function is called if the event preconditions
 * match. Then the function still has to check it own conditions and
 * then run its actions.
 *
 * \return 1 = successful run, 0 = not successful or conditions did not match.
 */
typedef bool (*action_function_t)(void);


/*! \brief Action function pointer for HPC
 *
 * This action function is called if the event preconditions
 * match. Then the function still has to check it own conditions and
 * then run its actions.
 *
 * \return 1 = successful run, 0 = not successful or conditions did not match, 2 = restart loop immediately.
 */
typedef int8_t (*action_function_hpc_t)(void);


/*! \brief Event type
 *
 * Events are stored in this structure. It will be used to match all
 * the given parse objects and coditions and the actions will be
 * performed if matched.
 *
 * Event matching is done via comparison to the parsed objects, verb,
 * adverbs, etc. You have to provide a verb number to be matched and
 * the other parameters have to match or can be set to EVENT_NOTP (in
 * this case they have to be missing from the entered text). Another
 * posibility is to used EVENT_ANY. This will match to any object,
 * etc. but is has to be present to be matched.
 *
 * If matching was successful then the actionfun is executed. If there
 * is a qcode string is is also executed using qcode_machine_run. If
 * any of the aforementioned functions returned a true (not equal to
 * zero) value then the parsing and matching of the event is deemed
 * successull and further parsing stops.
 */
typedef struct Event {
  uint8_t location; //!< matching the current locations number or EVENT_ANY.
  uint8_t verb; //!< matching verb number or one of EVENT_MAYBE or EVENT_ANY.
  uint8_t adverb; //!< matching verb number or one of EVENT_MAYBE or EVENT_ANY.
  uint8_t obj1; //!< matching object one number or one of EVENT_MAYBE or EVENT_ANY.
  uint8_t adj1; //!< matching adjective/adverb one number or one of EVENT_MAYBE or EVENT_ANY.
  uint8_t obj2; //!< matching object two number or one of EVENT_MAYBE or EVENT_ANY.
  uint8_t adj2; //!< matching adjective/adverb two number or one of EVENT_MAYBE or EVENT_ANY.
  action_function_t actionfun; //!< Action function pointer call will be executed if matched..
  const char *qcode; //!< Simplified qcode will be executed if matched. See qcode.h.
} Event_t;

/*! \brief Verb structure
 *
 * The list of \ref verbs will contain this mapping. If exits must be
 * printed then the first found occurence of the verb is used to
 * display the name of corresponding verb. Remember that the first
 * \ref VERB_MAXMOVE verbs are used for movement.
 */
typedef struct Verb {
  const char *verb; //!< name of the verb
  uint8_t no; //!< verb number
} Verb_t;

/*! \brief Adjective/Adverb structure
 *
 * Adjectives can be used to describe objects, like in "golden key" or
 * "silver key". They also double as adverbs and prapositions and
 * other stuff. The parser can be misused in different ways.
 */
typedef struct Adjective {
  const char *adjective; //!< adjective name
  uint8_t no; //!< adjective number
} Adjective_t;


/*! \brief Ignorable items for the parser
 *
 * Parser items which can be ignored during parsing. This are things
 * like the word "the" which an be used by the player in sentences
 * like "take the sword". If they occur then it is ok.
 */
typedef struct Ignorable {
  const char *ignorable; //!< ignorable name
} Ignorable_t;


/*! \brief Object structure
 *
 * Describe all objects with name and a long description. The name is
 * used to identify the object. In pos is the current position, this
 * is the room number where the object can be found. If it is \ref
 * OBJ_NOROOM then the object is not accessible, if it is \ref
 * OBJ_INVENTORY it is in the player's inventory.
 */
typedef struct Object {
  const char *name; //!< the name of the object to recognise
  uint8_t pos; //!< the initial position of the object
  uint8_t weight; //!< if <0 then object can not be carried
  const uint8_t *desc; //!< a description to print
  uint8_t adjective; //!< The corresponding adjective if multiple objects... Set to PO_UNKNOWN if no adjective is associated with this object.
} Object_t;


/*! \brief Describe a Location
 *
 * The short description is printed when the room is visited for the
 * second time, if it is visited for the first time then the long
 * description is printed. The long description can be printed with
 * other functions if the player wishes so.
 */
typedef struct Location {
  uint8_t image; //!< Image number to be displayed for this room.
  const uint8_t *desc_long; //!< A long and thorough description of the room.
  const char *desc_short; //!< A short description printed on further visits.
  uint8_t exits[VERB_MAXMOVE + 1]; //!< The \ref VERB_MAXMOVE exits according to the first six verbs.
} Location_t;

typedef int8_t counterval_t; //!< The type used for the counters.

extern const Verb_t verbs[]; //!< A list of verbs. Defined as a string to number map.
extern unsigned int verbs_N; //!< Must be set to number of elements!
extern const Adjective_t adjectives[]; //!< A list of adjectives or adverbs or other constructs.
extern unsigned int adjectives_N; //!< Must be set to number of elements!
extern const Ignorable_t ignorables[]; //!< List of ignorable words for the parser. Must end in NULL.
extern Object_t objects[]; //!< The list of all known objects.
extern unsigned int objects_N; //!< Must be set to number of elements!
extern const Location_t locations[]; //!< All game locations.
extern unsigned int locations_N; //!< Must be set to number of elements!
extern const uint8_t *messages[]; //!< All the messages needed in the game. The first few messages have a special meaning, see MESS_WHATNOW, MESS_EXITS, etc.
extern unsigned int messages_N; //!< Must be set to number of elements!
extern const Event_t events[]; //!< All the low-priority events the game should handle.
extern unsigned int events_N; //!< Must be set to number of elements!
extern counterval_t counters[256]; //!< 8Bit counters for many purposes.
/* \brief Welcome text at the start of the game.
 *
 * This must be defined in gamedata and it will pe printed at the
 * first start of the game. As the C64 version has the problem that
 * strings longer than 255 Characters, we use an array of strings
 * which will be printed separated by a white space.
 *
 * \warning End this array with a NULL pointer!
 */
extern const char *welcome_text[];
// For internal purpose, be careful!
extern uint8_t markers[256 / 8]; //!< Storage area for markers.
extern uint8_t markers_room_visited[256 / 8]; //!< Storage area for visited locations.


/*! \brief Action function for high priority events
 *
 * This function is called before the input is read. This can be
 * either NULL or point to your own code. The function is executed
 * first just before the the high_priority_event_qcode strings are
 * interpreted.
 */
extern action_function_hpc_t high_priority_event_actionfun;

/*! \brief QCode for high priority events
 *
 * This is an array of strings of QCode_t and will be executed until
 * either NULL is reached or one of the qcode action strings return a
 * true (or 1) value.
 */
extern QCode_t high_priority_event_qcode[];

/*! \brief Get marker status
 *
 * See \ref clear_all_markers().
 * 
 * \param i which marker
 * \return value of marker
 */
bool get_marker(uint8_t i);

/*! \brief Set marker status
 *
 * \param i which marker
 * \param x value of marker
 */
void set_marker(uint8_t i, bool x);

/*! \brief Clear all markers to false.
 *
 * See \ref clear_all_markers().
 * 
 * This is the default after the start of the engine. Markers are
 * simple boolean stores. They can be used to store a state for later
 * retrieval. Usage examples are is a lamp lit or not.
 *
 * \warning! This will also clear all visited locations!
 */
void clear_all_markers(void);

/*! \brief Was the room visited ?
 *
 * \param r which room
 * \return true = room visited, false = room not visited
 */
bool room_visited(uint8_t r);

/*! \brief Set room to visited
 *
 * \param r which room
 */
void room_visit(uint8_t r);


/*! \brief Generate a savable binary blob of the current game state.
 *
 * This data is neither nicely formatted nor portable.
 *
 * \param ptr pointer to memory for writing the game data or NULL (in which case memory is allocated)
 * \param size pointer to a variable which is set to the current game-data size
 * \return pointer to the generated data, will be identical to _ptr if ptr is not NULL
 */
void *generate_save_data(void *_ptr, unsigned int *size);

/*! \brief Interpret the saved data binary-blob.
 *
 * This interpretes the data provided by the pointer and tries to
 * interpret it. All global game variables are updated.
 *
 * \param _ptr pointer to save-game data
 * \return true if data could be interpreted
 */
bool interpret_save_data(void *_ptr);

/*! \brief Get the necessary size of the save data
 *
 * \return size in bytes
 */
unsigned int get_size_save_data(void);


/*! \brief Uncompress using out special compression function.
 *
 * Characters (lowercase) are compressed to five bits, space to four
 * and all other characters are expanded.
 *
 * \warning The buffer is static to make this compatible with the
 * C64. Do not use free on the string!
 *
 * @return uncompressed string in a *static* buffer
 */
char *uncompress_text(const uint8_t *data);

#endif
