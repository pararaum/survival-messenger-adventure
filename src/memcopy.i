;;; -*- mode: asm -*-
;;; Include file for the memcopy tool-functions.

	.ifndef memcpy_down
	.importzp	memcpy_source
	.importzp	memcpy_destination
	.import		memcpy_down
	.import		memcpy_down_end
	.import		memcpy_up_backwards
	.import		memcpy_up_backwards_end
	.endif
	MEMCPY_DOWN_SIZE = $21
	MEMCPY_UP_BACKWARDS = 50
