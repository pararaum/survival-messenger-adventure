#ifndef GFX_FUNCTION_H
#define GFX_FUNCTION_H

#define GFX_BASE_ADDRESS 0xBE00u

// general functions for memory access
#ifndef peek
  #define peek(addr) (*(unsigned char *)(addr))
#endif
#ifndef poke
  #define poke(addr,val)	(*(unsigned char *)(addr)=(val))
#endif

// turn on graphics mode, set CHROUT routine to print on graphics screen
#define gfx_init() (asm("jsr %w",GFX_BASE_ADDRESS))

// turn off graphics mode, set back CHROUT routing to default
#define gfx_off() (asm("jsr %w+3",GFX_BASE_ADDRESS))

// clear graphic screen
#define gfx_clrscr() (asm("jsr %w+6",GFX_BASE_ADDRESS))

// draw the mobile phone
#define gfx_drawmobile() (asm("jsr %w+21",GFX_BASE_ADDRESS))

// set text cursor to line Y (0..24), column X (0..39)
#define gfx_setcursor(X,Y) (asm("ldx  #%b", X),asm("ldy  #%b", Y),asm("jsr %w+12",GFX_BASE_ADDRESS))

// set text cursor to home position
#define gfx_setcursorhome() (asm("jsr %w+9",GFX_BASE_ADDRESS))

// scroll down on mobile phone
#define gfx_scrolldownmobile() (asm("jsr %w+15",GFX_BASE_ADDRESS))

// scroll down text area
#define gfx_scrolldowntext() (asm("jsr %w+18",GFX_BASE_ADDRESS))

// show image, N between 1 and 15
#define gfx_showimage(N) (__AX__ = (N),asm("jsr %w+24",GFX_BASE_ADDRESS))

// macros for setting border and background color
#define set_bgcolor(C) (__AX__ = (C),asm("sta $D021"))
#define set_bordercolor(C) (__AX__ = (C),asm("sta $D020"))
#define set_textcolor(C) (__AX__ = (C),asm("sta $286"))

// show image, N between 1 and 15
#define input(addr) (__AX__ = (int)(addr),asm("jsr %w+27",GFX_BASE_ADDRESS),__AX__)

#define selectwindow1() (asm("jsr %w+30",GFX_BASE_ADDRESS))

#define selectwindow2() (asm("jsr %w+33",GFX_BASE_ADDRESS))

#define waitkeycursor() (asm("jsr %w+36",GFX_BASE_ADDRESS))

#define load_file(S) (__AX__ = (S),asm("jsr %w+39",GFX_BASE_ADDRESS))

#define disable_loader() (asm("jsr %w+42",GFX_BASE_ADDRESS))

#endif