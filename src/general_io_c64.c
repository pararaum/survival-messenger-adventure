#include <peekpoke.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <stdlib.h>
#include <assert.h>
#include <cbm.h>
#include "general_io.h"
#include "gfx_functions.h"
#include "c64_muzak.h"
#include "c64_mobilemoiety.h"
#include "data_structures.h"

extern void wait_for_key(void);
extern void setup_irq(void);
extern void setup_sprite(void);

/*
** This file contains the general I/O functions for the C64.
**
** The POSIX interface can be found in general_io.c.
*/

#define MAXCOLUMN 26 //!< maximum column for word wrapping.
#define MAXLINE 10 //!< maximum line before waiting for key.
#define BackgroundCol 0
#define ForegroundCol 3 // Cyan foreground.

uint8_t str_25_spaces[] = {0xEE, 0xEE, 0xEE, 0xEE, 0xEE, 0xEE, 0xEE, 0xEE, 0xEE, 0xEE, 0xEE, 0xE0, 0x00};
uint8_t column = 0; //!< The current column we are printing in.
uint8_t line = 0; //!< The current line we are printing in.

#pragma code-name (push, "HIGHONCE")

void gio_init(void) {
  POKE(0xd020, BackgroundCol);
  POKE(0xd021, BackgroundCol);
  column = 0; // Just to be sure.
  line = 0;
  muzak_init();
  setup_irq();
  gio_putchar(ForegroundCol);
  gio_clearscreen();
  // GFX-Routines
  gfx_init();
  gfx_clrscr();
  set_bgcolor(BackgroundCol);
  set_textcolor(ForegroundCol);
  gfx_drawmobile();
  setup_sprite();
}
#pragma code-name (pop)

static void increment_line(void) {
  column = 0;
  ++line;
  if(line >= MAXLINE) {
    line = 0;
    printf("Press any Key!");
    wait_for_key();
    gfx_setcursor(0, 24);
    printf(uncompress_text(str_25_spaces));
    gfx_setcursor(0, 24);
    line = 0;
  }
}

static void increment_column(void) {
  if(++column >= MAXCOLUMN) {
    // We missed a word wrap?
    increment_line();
  }
}

void gio_putchar(char c) {
  switch(c) {
  case '\n':
    gio_nl();
    break;
  case '\f':
    column = 0;
    line = 0;
    putchar('\f');
  default:
    putchar(c);
    increment_column();
  }
}

static void gio_word_wrap_string(const char *s) {
  size_t l;

  if(s != NULL) {
    l = strlen(s);
    if(column + l + 1 >= MAXCOLUMN) { // We need wrapping.
      gio_nl();
    } else {
    }
    printf("%s", s); // Just output.
    column += l;
  }
}

void gio_shutdown(void) {
  disable_loader();
  gfx_off(); 
  gio_nl();
}


void gio_clearscreen(void) {
  gio_putchar('\f');
}


void gio_nl(void) {
  putchar('\n');
  increment_line();
}


void gio_space(void) {
  putchar(' ');
  increment_column();
}


void gio_reset_nl(void) {
  column = 0;
  line = 0;
}


void gio_putstr(const char *s) {
  char *buf = strdup(s);
  size_t len = strlen(s); // Length of the whole string.
  char *ptr;

  if(len == 0) {
    return; // Empty string?
  }
  if(buf == NULL) { // We failed to acquire memory...
    printf("%s", s);
  } else {
    ptr = strtok(buf, " _");
    gio_word_wrap_string(ptr);
    ptr = strtok(NULL, " _");
    while(ptr != NULL) {
      gio_space();
      gio_word_wrap_string(ptr);
      ptr = strtok(NULL, " _");
    }
  }
  if(s[len - 1] == ' ') {
    // We have a space at the end? Then print it to have nicer
    // output...
    gio_space();
  }
  free(buf);
}


void gio_putstrln(const char *s) {
  gio_putstr(s);
  gio_nl();
}


char *gio_readline(char *buf, unsigned int len, const char *prompt) {
  mobile_messagehandler();
  line = 0;
  if(prompt != NULL) {
    /* strtok() is not reentrant and fails therefore when used
       here. We now assume that a prompt does not need word
       wrapping. */
    printf("%s", prompt);
    fflush(stdout);
  }
  input(buf);
  if(strlen(buf) > len) {
    abort();
  }
  gio_nl();
  
  return buf;
}


void gio_show_image(uint8_t i) {
  gfx_showimage(i);
}


void gio_write_savedata(void *data, unsigned int size, const char *name) {
  char fname[32];
  unsigned char *p;
  unsigned char status;

  disable_loader();
  snprintf(fname, sizeof(fname), "@0:%s,seq", name);
  cbm_k_setlfs(2, 8, 1); // Open for writing.
  cbm_k_setnam(fname); // Set the filename.
  cbm_k_open();
  status = cbm_k_readst();
  cbm_k_ckout(2); // Default output is now 2!
  for(p = data; size > 0; --size) {
    cbm_k_bsout(*p++);
  }
  cbm_k_bsout(0x94);
  cbm_k_bsout(0xB7);
  cbm_k_bsout(0x84);
  cbm_k_bsout(0xA0);
  cbm_k_bsout(0x89);
  cbm_k_bsout(0x8e);
  cbm_k_bsout(0xA0);
  cbm_k_bsout(0x94);
  cbm_k_bsout(0xB7);
  cbm_k_bsout(0x84);
  cbm_k_close(2);
  cbm_k_clrch(); // Restore defaults for I/O.
}


bool gio_read_savedata(void *data, unsigned int size, const char *name) {
  char fname[32];
  unsigned char *p;
  unsigned char status = -1;

  snprintf(fname, sizeof(fname), "%s,seq", name);
  disable_loader();
  cbm_k_setlfs(2, 8, 0); // Open for reading.
  cbm_k_setnam(fname); // Set the filename.
  cbm_k_open();
  status = cbm_k_readst();
  cbm_k_chkin(2); // Default input is now 2!
  for(p = (unsigned char *)data; size > 0; --size) {
    *p++ = cbm_k_basin();
    //cbm_k_readst, https://www.c64-wiki.de/wiki/STATUS
    if((status = cbm_k_readst()) != 0) {
      break;
    }
  }
  cbm_k_close(2);
  cbm_k_clrch(); // Restore defaults for I/O.
  return status == 0;
}
