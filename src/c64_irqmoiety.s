	.export _setup_irq
	.export _irqcntr

	irqline = 55

	.import _muzak_play

;;; Mixing C and interrupt code seems to be mightily complicated, see: https://github.com/cc65/wiki/wiki/Interrupt-handlers-in-C.

	CHROUT = $FFD2
	gfx_base_addr = $c200
	selectwindow1 = gfx_base_addr + 30
	selectwindow2 = gfx_base_addr + 33

	.bss
oldirq:	.res	2

	.segment "HIGHONCE"
_setup_irq:
	sei
	lda	$314
	sta	oldirq
	lda	$315
	sta	oldirq+1
        lda     #<irqroutine
        sta     $314
        lda     #>irqroutine
        sta     $315
        lda     #$7f
        sta     $dc0d   ;disable timer interrupts
        sta     $dd0d
        lda     $dc0d   ;by reading this two registers we negate any pending CIA irqs.
        lda     $dd0d
        ;; https://www.c64-wiki.com/wiki/Raster_interrupt
        lda     #$7f
        and     $d011           ; Set MSB of raster to zero
        sta     $d011
        lda     #irqline	; Top of screen plus 100 rasterlines.
        sta     $d012
        ;; http://unusedino.de/ec64/technical/project64/mapping_c64.html
        ; 53274         $D01A          IRQMASK
        lda     #%00000001
        sta     $d01a
	cli
	rts

	.segment "LOWCODE"
irqroutine:
	asl	$d019		; Acknowledge.
;;; 	bit	$d011		; Get MSB of rasterline.
	jsr	irq_run
	jmp	(oldirq)

	.data
_irqcntr:	.byte	0

	.segment	"LOWCODE"
irq_run:
	inc	_irqcntr
	jmp	_muzak_play

