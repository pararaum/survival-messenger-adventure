
	.export gfxblob_begin
	.export gfxblob_end
	.export	_setup_sprite

	.data
sprite_0:
.byte %00000000,%00000010,%00000000
.byte %00001000,%00000010,%00000000
.byte %00001000,%11000011,%11111000
.byte %00000101,%00000100,%00000110
.byte %00000010,%00001000,%00000000
.byte %10000010,%00001000,%00000000
.byte %01100010,%00001000,%00000000
.byte %00011100,%00010000,%00000000
.byte %00000011,%11100000,%00000000
.byte %00000000,%00110000,%00001000
.byte %00000000,%01011000,%00010000
.byte %00000000,%01001100,%00100000
.byte %00000000,%10100110,%01000000
.byte %00110001,%00100011,%10000000
.byte %00001001,%00010000,%00000000
.byte %00000110,%00010000,%00000000
.byte %00000100,%00010000,%00000000
.byte %00001000,%00010010,%00000000
.byte %00010000,%00101100,%00000000
.byte %00000000,%11000000,%00000000
.byte %00000000,%00000000,%00000000

	.segment "gfxblob"
gfxblob_begin:
	.incbin	"gfxtools.prg", 2
gfxblob_end:
	;; 

	.code
_setup_sprite:
	sei
	lda	1
	pha
	lda	#$30
	sta	1
	ldx	#63
@l1:	lda	sprite_0,x
	sta	$dfff-63,x
	dex
	bpl	@l1
	pla
	sta	1
	;; pos
	lda	#$f0
	sta	$d000
	lda	#$39
	sta	$d001
	lda	#0
	sta	$d010		; Clear all MSBs.
	sta	$d003
	sta	$d005
	sta	$d007
	sta	$d009
	sta	$d00b
	sta	$d00d
	sta	$d00f
	lda	#$01		; Enable only the first sprite.
	sta	$d015		; Sprite enable https://sta.c64.org/cbm64mem.html
	lda	#0
	sta	$d017		; X
	sta	$d01d		; Y
	lda	#11
	sta	$d027		; colour
	;; (/ (- #xdfff #xc000 63) 64)
	lda	#127
	sta	$cff8		; Sprite pointer
	cli
	rts
