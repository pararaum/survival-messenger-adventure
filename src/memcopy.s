;;; Simple memory copy operatation from higher to lower memory.
;;; See also: http://w.supervisord.org/source/general/memory_move.html

	.exportzp	memcpy_source
	.exportzp	memcpy_destination
	.export		memcpy_down
	.export		memcpy_down_end
	.export		memcpy_up_backwards
	.export		memcpy_up_backwards_end

	.zeropage
memcpy_source:		.res	2
memcpy_destination:	.res	2

; Move memory down
				;
;;; Input: A/X=size(lo/hi) in bytes
;;; Output: -
;;; Modifies: A/X/Y
; memcpy_source = source start address
; memcpy_destination = destination start address
	.code
memcpy_down:
	pha			; Size low is on the stack.
	ldy	#0
	txa			; Actually "cpx #0".
	beq	@md2		; Less than 256 Bytes?
@md1:	lda	(memcpy_source),Y ; move a page at a time
	sta	(memcpy_destination),Y
	iny
	bne	@md1
	inc	memcpy_source+1
	inc	memcpy_destination+1
	dex
	bne	@md1
@md2:	pla			; Get the low byte of the size.
	beq	@md4		; Is size modules 256 equal to zero?
	tax			; Put remaining bytes into X.
@md3:	lda	(memcpy_source),Y ; move the remaining bytes
	sta	(memcpy_destination),Y
	iny
	dex
	bne	@md3
@md4:	rts
memcpy_down_end:
	memcpy_down_size = memcpy_down_end-memcpy_down
	.include	"memcopy.i"
	.ASSERT memcpy_down_size = MEMCPY_DOWN_SIZE, error, "Size mismatch for memcpy_down_size!"

; Move memory up
;
; memcpy_source = source end address
; memcpy_destination = destination end address
; A/X = SIZE(lo/hi) = number of bytes to move
;;; Modifies: A/X/Y
memcpy_up_backwards:
	pha
	ldy	#0
	txa			; actually cpx #0
	beq	@mu3
@mu1:	lda	(memcpy_source),Y ; handle Y = 0 separately
	sta	(memcpy_destination),Y
	dey
	dec	memcpy_source+1
	dec	memcpy_destination+1
@mu2:	lda	(memcpy_source),Y ; move a page at a time
	sta	(memcpy_destination),Y
	dey
	bne	@mu2
	dex
	bne	@mu1
@mu3:	pla
	beq	@mu5
	tax
	lda	(memcpy_source),Y ; handle Y = 0 separately
	sta	(memcpy_destination),Y
	dey
	dex
	beq	@mu5
	dec	memcpy_source+1
	dec	memcpy_destination+1
@mu4:	lda	(memcpy_source),Y ; move the remaining bytes
	sta	(memcpy_destination),Y
	dey
	dex
	bne	@mu4
@mu5:	rts
memcpy_up_backwards_end:
	memcpy_up_backwards_size = memcpy_up_backwards_end-memcpy_up_backwards
	.ASSERT memcpy_up_backwards_size = MEMCPY_UP_BACKWARDS, error, "Size mismatch for memcpy_up_backwards_size!"
