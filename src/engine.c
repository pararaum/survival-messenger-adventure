#include <string.h>
#include <assert.h>
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#ifdef __C64__
#include <cbm.h>
#include "c64_mobilemoiety.h"
#include "gfx_functions.h"
#endif
#include "data_structures.h"
#include "general_io.h"
#include "qcode.h"

/*! \brief reset the parse objects to unknown
 *
 * The parsed objects are set to unknown so that the parser can set
 * the known objects.
 */
static void reset_parsed_objects(void) {
  verb = PO_UNKNOWN;
  adverb = PO_UNKNOWN;
  obj1 = PO_UNKNOWN;
  adj1 = PO_UNKNOWN;
  obj2 = PO_UNKNOWN;
  adj2 = PO_UNKNOWN;
}


static uint8_t find_adjective(const char *s) {
  uint8_t found = PO_UNKNOWN;
  uint8_t i;

  for(i = 0; i < adjectives_N; ++i) {
    if(strcmp(adjectives[i].adjective, s) == 0) {
      found = adjectives[i].no;
      break;
    }
  }
  return found;
}


/*! \brief find an object by name
 *
 * Search the list of objects and get the object number. It can search
 * for an adjective, too.
 *
 * \param adj adjective number or ADJ_ANY if the first matching object should be found
 * \param s name of the object to search for
 * \return number of object or PO_UNKNOWN
 */
static uint8_t find_object(uint8_t adj, const char *s) {
  uint8_t found = PO_UNKNOWN;
  uint8_t found_carried = PO_UNKNOWN;
  uint8_t found_here = PO_UNKNOWN;
  uint8_t i;

  // Search through list of objects.
  for(i = 0; i < objects_N; ++i) {
    if(strcmp(objects[i].name, s) == 0) {
      // The name of the object matches.
      if((adj != PO_UNKNOWN) && (adj == adj1)) {
	// This is an exact match, we can end the loop now.
	return i;
      } else {
	// Object has not yet been found but the name matches.
	if(qcode_object_carried(i)) {
	  // Prefer a carried object.
	  found_carried = i;
	} else if(qcode_object_here(i)) {
	  // Object is in the same room.
	  found_here = i;
	} else if(found == PO_UNKNOWN) {
	  found = i;
	}
      }
    }
    //printf("%d %d %d %d\n", (int)i, (int)found, (int)found_carried, (int)found_here);
  }
  if(found_carried != PO_UNKNOWN) {
    return found_carried;
  } else if(found_here != PO_UNKNOWN) {
    return found_here;
  }
  return found;
}


/* Get next token and ignore the ignorable words.
 */
char *strtok_with_ignore(void) {
  char *s;

  while((s = strtok(NULL, " \n")) != NULL) {
    if(find_ignorable(s) == NULL) {
      //Not an ignorable.
      break;
    }
  }
  return s;
}


/*! \brief read and parse input into variables
 *
 * This will read a line and parse the input.
 * The global variables for verb, adverb, obj, adj, etc. are set.
 *
 * \return 1 = parse ok, 0 = parse not ok, -1 = EOF
 */
static int8_t read_and_parse(void) {
  char buf[60];
  char *bufptr;
  char *s;
  uint8_t i;

  gio_readline(buf, sizeof(buf), NULL);
#ifndef NDEBUG
  printf("\"%s\".\n", buf);
  if(buf[0] == '^') {
    // Top-notch debugging...
    i = qcode_machine_run(buf + 1);
    printf("qcode_machine_run() = %d\n", i);
    return 0;
  }
#endif
  for(s = buf; *s; ++s) {
    *s = tolower(*s);
  }
  reset_parsed_objects();
  if((bufptr = strtok(buf, " \n")) == NULL) {
    return 0;
  } else {
    // TODO: one could use bsearch as this is implemented in stdlib
    // for cc65. Disadvantage: the first four (or six) need to be
    // movement verbs, finding empty values may be harder...
    // bsearch("asdf", messages, 10, sizeof(const char *), &f);
    for(i = 0; i < verbs_N; ++i) {
      if(strcmp(verbs[i].verb, bufptr) == 0) {
        verb = verbs[i].no;
        break;
      }
    }
    if(verb == PO_UNKNOWN) {
      gio_putstr("I do not know how to: ");
      gio_putstrln(bufptr);
      return 0;
    } else { //verb found
      bufptr = strtok_with_ignore();
      if(bufptr) {
        adj1 = find_adjective(bufptr); // Look if this is an adjective.
        if(adj1 != PO_UNKNOWN) {
          bufptr = strtok_with_ignore(); // Get next token.
        }
      }
      if(bufptr) {
        obj1 = find_object(PO_UNKNOWN, bufptr); // Look if this is an object.
        if(obj1 != PO_UNKNOWN) {
          bufptr = strtok_with_ignore(); // Get next token.
        } else {
          gio_putstr("I do not know what a '");
          gio_putstr(bufptr);
          gio_putstrln("' is.");
          return 0;
        }
      }
      if(bufptr) {
	adj2 = find_adjective(bufptr); // Look if this is an adjective.
        if(adj2 != PO_UNKNOWN) {
          bufptr = strtok_with_ignore(); // Get next token.
        }
      }
      if(bufptr) {
        obj2 = find_object(PO_UNKNOWN, bufptr); // Look if this is an object.
        if(obj2 != PO_UNKNOWN) {
          bufptr = strtok_with_ignore(); // Get next token.
        } else {
          gio_putstr("I do not know what a '");
          gio_putstr(bufptr);
          gio_putstrln("' is.");
          return 0;
        }
      }
    }
  }
  return 1;
}


/*! \brief High Priority Events
 *
 * \return -1 = break loop, 0 = directly continue, 1 = parse on, 2 = restart loop
 */
static int8_t high_priority_events(void) {
  int8_t r = 0;
  QCode_t *q;

  for(q = &high_priority_event_qcode[0]; *q; ++q) {
    if((r = qcode_machine_run(*q)) != 0) {
      return r;
    }
  }
  if(NULL != high_priority_event_actionfun) {
    r = high_priority_event_actionfun();
  }
  return r;
}


/*! \brief Check and do event
 *
 * Find events which match and check their conditions. If the
 * conditions match then perform the actions.
 *
 * \return true = action performed, false = no action performed
 */
bool do_event(void) {
  unsigned int i;
  bool done = false;

  for(i = 0; i < events_N; ++i) {
    const Event_t *p = &(events[i]);
    if(
      ((p->location == EVENT_ANYROOM) || (p->location == here)) &&
      ((p->verb == verb) || (p->verb == EVENT_ANY)) &&
      ((p->adverb == adverb) || (p->adverb == EVENT_MAYBE) || (p->adverb == EVENT_ANY && adverb != PO_UNKNOWN)) &&
      ((p->obj1 == obj1) || (p->obj1 == EVENT_MAYBE) || (p->obj1 == EVENT_ANY && obj1 != PO_UNKNOWN)) &&
      ((p->obj2 == obj2) || (p->obj2 == EVENT_MAYBE) || (p->obj2 == EVENT_ANY && obj2 != PO_UNKNOWN)) &&
      ((p->adj1 == adj1) || (p->adj1 == EVENT_MAYBE) || (p->adj1 == EVENT_ANY && adj1 != PO_UNKNOWN)) &&
      ((p->adj2 == adj2) || (p->adj2 == EVENT_MAYBE) || (p->adj2 == EVENT_ANY && obj1 != PO_UNKNOWN))
      ) {
#ifdef DEBUG
      printf("match event %u\n", i);
#endif
#ifdef DEBUG
      printf("actionfun %p\n", p->actionfun);
#endif
      if(p->actionfun != NULL) {
        done = p->actionfun();
      }
#ifdef DEBUG
      printf("qcode %p\n", p->qcode);
#endif
      if(p->qcode != NULL) {
        done |= qcode_machine_run(p->qcode);
      }
      if(done) {
        return true;
      }
    }
  }
  return false; //Nothing done
}


/*! \brief Move the player
 *
 * This function gets the current verb and moves the player according
 * to this verb, if possible.
 *
 * \return true = movement was ok, false = no movement possible
 */
bool do_movement(void) {
  const struct Location *lp = &locations[here];

  assert(verb <= VERB_MAXMOVE);
  if(lp->exits[verb] == LOC_NOCONN) {
    gio_putstrln("You can not go in that direction!");
  } else {
    here = lp->exits[verb];
    return true;
  }
  return false;
}

/*! \brief Adventure loop
 *
 * Loop which performs the high priority conditions, read the inputs,
 * parses it, event handling and movement. The loop runs until the
 * marker MARKER_GAME_ENDED is true.
 */
void adventure_loop(void) {
  int8_t riret; //!< read input return

  do {
    /* TODO: High priority local events? */
    gio_nl();
    riret = high_priority_events();
    switch(riret) {
    case 0:
#ifndef NDEBUG
      puts("Zero return?");
#endif
      continue;
    case 1:
      break;
    case 2:
      continue;
    case -1:
    default:
      printf("hpc()=%d\n", riret);
      abort();
    }
    qcode_message(MESS_WHATNOW);
    fflush(stdout);
    riret = read_and_parse();
    gio_reset_nl();
#ifndef NDEBUG
    printf("here=%d,verb=%d,adverb=%d,obj1=%d,adj1=%d,obj2=%d,adj2=%d\n", here, verb, adverb, obj1, adj1, obj2, adj2);
#endif
    if(riret > 0) {
      if(!do_event()) {
        // No action performed. Now check for movement verbs.
        if(verb <= VERB_MAXMOVE) {
          if(do_movement()) {
          } else {
          }
        } else {
          qcode_message(MESS_NOUNDERSTANING);
          gio_nl();
        }
      } else {
	gio_nl();
	gio_putstr("OK.");
      }
    }
  } while((riret >= 0) && !get_marker(MARKER_GAME_ENDED));
  /* return adv; */
}

static void welcome(void) {
  const char **s;

  for(s = &welcome_text[0]; *s; ++s) {
    if(s != welcome_text) {
      gio_space();
    }
    gio_putstr(*s);
  }
  gio_nl();
}


int main(void) {
  static void *initial_state;
  unsigned int size = 0;
  char ch;

  gio_init();
#ifndef NDEBUG
  qcode_save_as("initial");
#endif
  clear_all_markers();
  initial_state = generate_save_data(NULL, &size);
  do {
#ifdef __C64__
    gfx_showimage(55);
#endif
    welcome();
    adventure_loop();
#ifndef NDEBUG
    qcode_save_as("game.data");
#endif
    gio_putstr("\nPlay again? ");
#ifdef __C64__
    mobile_off(); // Mobile off!
    while((ch = cbm_k_getin()) == 0) {}
#else
    ch = getchar();
#endif
    gio_nl();
    if((ch == 'y') || (ch == 'Y')) {
      size = 0;
    } else {
      size = 1;
    }
    interpret_save_data(initial_state);
    clear_all_markers();
  } while(size == 0);
  free(initial_state);
  gio_shutdown();
#ifdef __C64__
  asm("jmp 64738");
#endif
  return 0;
}

