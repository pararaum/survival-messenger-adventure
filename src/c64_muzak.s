
	.export	_muzak_init
	.export _muzak_play
	.export _muzak_sid
	.export _mobile_sound


	.segment "MUZAK"
_muzak_sid:
	;;  	.incbin "beach-03.sid", $7c+$2
 	.incbin "beach-04.sid", $7c+$2

sfx_initial:
	.incbin	"powerup01.bin"


sfx_00:	.scope
	;-config
TONE	= $c0
TONE1	= TONE+0
TONE2	= TONE+12
;-
	.byte $00	;attack / decay
	.byte $f9	;sustain / release
	.byte $08	;pulse width in reverse order, no pulsewidth modulation
	;wavetable: note/waveform pairs
	;values $01-$81 are waveforms, values $82-$DF are absolute notes D-0 - B-7
	.byte $c0,$41
	.byte TONE1
	.byte TONE1
	.byte TONE2
	.byte TONE2
	.byte TONE1
	.byte TONE1
	.byte TONE2
	.byte TONE2
	.byte TONE1
	.byte TONE1
	.byte TONE2
	.byte TONE2
	;endmarker
	.byte $00
	.endscope
	;; ⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡

sfx_01:	.scope
;-config
TONE	= $c0
TONE1	= TONE+0
TONE2	= TONE+4
TONE3	= TONE+7
TONE4	= TONE+12
;-
	.byte $00	;attack / decay
	.byte $f9	;sustain / release
	.byte $08	;pulse width in reverse order, no pulsewidth modulation
	;wavetable: note/waveform pairs
	;values $01-$81 are waveforms, values $82-$DF are absolute notes D-0 - B-7
	.byte $c0,$41
	.byte TONE1
	.byte TONE1
	.byte TONE1
	.byte TONE1
	.byte TONE2
	.byte TONE2
	.byte TONE2
	.byte TONE2
	.byte TONE3
	.byte TONE3
	.byte TONE3
	.byte TONE3
	.byte TONE4
	.byte TONE4
	.byte TONE4
	.byte TONE4
	;endmarker
	.byte $00
	.endscope
	;; ⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡

sfx_02:	.scope
;-config
TONE	= $c0
TONE1	= TONE+12
TONE2	= TONE+0
;-
	.byte $00	;attack / decay
	.byte $f9	;sustain / release
	.byte $08	;pulse width in reverse order, no pulsewidth modulation
	;wavetable: note/waveform pairs
	;values $01-$81 are waveforms, values $82-$DF are absolute notes D-0 - B-7
	.byte $c0,$41
	.byte TONE1
	.byte TONE1
	.byte TONE1
	.byte TONE1
	.byte TONE1
	.byte TONE1
	.byte TONE1
	.byte TONE1
	.byte TONE2
	.byte TONE2
	.byte TONE2
	.byte TONE2
	.byte TONE2
	.byte TONE2
	.byte TONE2
	.byte TONE2
	;endmarker
	.byte $00
	.endscope
	;; ⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡

sfx_03:	.scope
;-config
TONE	= $c0
TONE1	= TONE+12
TONE2	= TONE+7
TONE3	= TONE+4
TONE4	= TONE+0
;-
	.byte $00	;attack / decay
	.byte $f9	;sustain / release
	.byte $08	;pulse width in reverse order, no pulsewidth modulation
	;wavetable: note/waveform pairs
	;values $01-$81 are waveforms, values $82-$DF are absolute notes D-0 - B-7
	.byte $c0,$41
	.byte TONE1
	.byte TONE1
	.byte TONE1
	.byte TONE1
	.byte TONE2
	.byte TONE2
	.byte TONE2
	.byte TONE2
	.byte TONE3
	.byte TONE3
	.byte TONE3
	.byte TONE3
	.byte TONE4
	.byte TONE4
	.byte TONE4
	.byte TONE4
	;endmarker
	.byte $00
	.endscope
	;; ⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡

sfx_04:	.scope

TONE	= $b0+12
TONE00	= TONE+0
TONE01	= TONE+1
TONE02	= TONE+2
TONE03	= TONE+3
TONE04	= TONE+4
TONE05	= TONE+5
TONE06	= TONE+6
TONE07	= TONE+7
TONE08	= TONE+8
TONE09	= TONE+9
TONE10	= TONE+10
TONE11	= TONE+11
TONE12	= TONE+12

DUR_8	= 3
DUR_4	= DUR_8*2
;-

	.byte $00	;attack / decay
	.byte $f0	;sustain / release
	.byte $f8	;pulse width in reverse order, no pulsewidth modulation
	
	;wavetable: note/waveform pairs
	;values $01-$81 are waveforms, values $82-$DF are absolute notes D-0 - B-7
	.byte $c0,$11

	.repeat DUR_4+DUR_8
	.byte TONE00
	.byte TONE00+12
	.endrepeat

	.repeat DUR_8
	.byte TONE02
	.byte TONE02+12
	.endrepeat

	.repeat DUR_4
	.byte TONE04
	.byte TONE04+12
	.endrepeat

	.repeat DUR_4
	.byte TONE07
	.byte TONE07+12
	.endrepeat

	.repeat DUR_4
	.byte TONE12
	.byte TONE12+12
	.endrepeat

	.repeat DUR_4
	.byte TONE07
	.byte TONE07+12
	.endrepeat

	.repeat DUR_4
	.byte TONE12
	.byte TONE12+12
	.endrepeat
	
	;endmarker
	.byte $00

	.endscope
	;; ⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡


sfx_05:	.scope
TONE	= $b0
TONE00	= TONE+0
TONE01	= TONE+1
TONE02	= TONE+2
TONE03	= TONE+3
TONE04	= TONE+4
TONE05	= TONE+5
TONE06	= TONE+6
TONE07	= TONE+7
TONE08	= TONE+8
TONE09	= TONE+9
TONE10	= TONE+10
TONE11	= TONE+11
TONE12	= TONE+12

DUR_8	= 3
DUR_4	= DUR_8*2
WAVEFORM	= $11
;-
	.byte $00	;attack / decay
	.byte $f0	;sustain / release
	.byte $f8	;pulse width in reverse order, no pulsewidth modulation
	
	;wavetable: note/waveform pairs
	;values $01-$81 are waveforms, values $82-$DF are absolute notes D-0 - B-7
	.byte $c0,$11

	.repeat DUR_4+DUR_8
	.byte TONE12
	.byte TONE12+12
	.endrepeat

	.repeat DUR_8
	.byte TONE11
	.byte TONE11+12
	.endrepeat

	.repeat DUR_8
	.byte TONE10
	.byte TONE10+12
	.endrepeat

	.repeat DUR_8
	.byte TONE09
	.byte TONE09+12
	.endrepeat

	.repeat DUR_4+DUR_8
	.byte TONE08
	.byte TONE08+12
	.endrepeat

	.repeat DUR_8
	.byte TONE07
	.byte TONE07+12
	.endrepeat

	.repeat DUR_8
	.byte TONE06
	.byte TONE06+12
	.endrepeat

	.repeat DUR_8
	.byte TONE05
	.byte TONE05+12
	.endrepeat

	.repeat DUR_4+DUR_8
	.byte TONE04
	.byte TONE04+12
	.endrepeat

	.repeat DUR_8
	.byte TONE03
	.byte TONE03+12
	.endrepeat

	.repeat DUR_8
	.byte TONE02
	.byte TONE02+12
	.endrepeat

	.repeat DUR_8
	.byte TONE01
	.byte TONE01+12
	.endrepeat

	.repeat DUR_4+DUR_4
	.byte TONE00
	.byte TONE00+12
	.endrepeat

	;endmarker
	.byte $00
	.endscope
	;; ⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡⇡


	
	.define SfxTable	sfx_initial,sfx_00,sfx_01,sfx_02,sfx_03,sfx_00,sfx_01,sfx_02,sfx_03,sfx_04,sfx_05
sfxTableLO:	.lobytes	SfxTable
sfxTableHI:	.hibytes	SfxTable

;;; 	void mobile_sound(int sfxno)
;;; Input: A/X sound effect number, if too large it will be adjusted.
;;; Output: -
;;; Modifies: A/X/Y
;;; Actually we directly ignore the high byte... (X)
_mobile_sound:
	cmp	#sfxTableHI-sfxTableLO ; Number of entries in the table aka sound effects.
	bcc	@cont		       ; OK, load address and continue
	;; Carry is set now, so subtract.
	sbc	#sfxTableHI-sfxTableLO
	bcs	_mobile_sound
@cont:
	tax			; Put sound-effect value into X.
	lda	sfxTableLO,x
	ldy	sfxTableHI,x
	ldx	#14		; Channel 3
	jmp	_muzak_sid+6	; Effect!

	.code
_muzak_init:
	lda	#$00
	jmp	_muzak_sid

_muzak_play:
	jmp	_muzak_sid+3
