#ifndef __C64_MOBILEMOIETY_H_2020
#define __C64_MOBILEMOIETY_H_2020
#include <stdbool.h>

#define MOBILEPHONEMARKER 200

void mobile_messagehandler(void);
void mobile_sound(int sfxno);
void mobile_off(void);
#endif
