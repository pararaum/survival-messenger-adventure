#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "general_io.h"

/*
** This file contains the general I/O functions that are platform
** independent. They should only contain POSIX functions so that they
** may be compiled on a variety of systems. Of course, only a simple
** text interface is possible.
**
** The C64 interface can be found in general_io_c64.c.
*/

void gio_show_image(uint8_t i) {
  (void)(i); // Suppress unused variable warning.
}


void gio_putchar(char c) {
  putchar(c);
}


void gio_clearscreen(void) {
  putchar('\f');
}


void gio_reset_nl(void) {
  gio_nl();
}


void gio_nl(void) {
  putchar('\n');
}


void gio_space(void) {
  putchar(' ');
}


void gio_putstr(const char *s) {
  char *p;
  char *c;

  c = strdup(s);
  for(p = c; *p; ++p) {
    if(*p == '_') {
      *p = ' ';
    }
  }
  printf("%s", c);
  free(c);
}


void gio_putstrln(const char *s) {
  gio_putstr(s);
  gio_nl();
}


char *gio_readline(char *buf, unsigned int len, const char *prompt) {
  if(prompt != NULL) {
    gio_putstr(prompt);
    fflush(stdout);
  }
  if(fgets(buf, len - 1, stdin) == NULL) {
    return NULL;
  } else {
    // Nothing, as we need upper and lowercase letters for the QCode.
  }
  return buf;
}

void gio_init(void) {
}


void gio_shutdown(void) {
  gio_nl();
}

void gio_write_savedata(void *data, unsigned int size, const char *name) {
  FILE *f;
  size_t written;

  f = fopen(name, "wb+");
  if(f) {
    written = fwrite(data, 1, size, f);
    if(written < size) {
      fprintf(stderr, "Only %u bytes written.\n", (unsigned int)written);
    }
    fclose(f);
  }
}

bool gio_read_savedata(void *data, unsigned int size, const char *name) {
  FILE *f;
  size_t read;

  f = fopen(name, "rb");
  if(f) {
    read = fread(data, 1, size, f);
    if(read < size) {
      fprintf(stderr, "Only %u bytes read.\n", (unsigned int)read);
      return false;
    }
    fclose(f);
    return true;
  }
  return false;
}
