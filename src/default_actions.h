#ifndef __DEFAULT_ACTIONS_H_2020__
#define __DEFAULT_ACTIONS_H_2020__
#include <stdbool.h>

/*! \file default_actions.h
 *
 * This file contains default actions common to all type of
 * adventures. They can be used to reduce the burden of programmin
 * them all the time.
 */

/*! \brief TAKE an object
 *
 * The object defined in obj is "taken" which means it is moved into
 * the player's possession. If it cannot be found appropriate messages
 * are printed.
 *
 * This function will return true even if the object could not be
 * picked up!
 *
 * \param obj object number 
 * \return true = action was performed, else something went wrong 
 */
bool action_take_object(unsigned int obj);

/*! \brief TAKE object one
 *
 * The object defined in obj1 is "taken" which means it is moved into
 * the player's possession. If it cannot be found appropriate messages
 * are printed.
 *
 * This function will return true even if the object could not be
 * picked up!
 *
 * \return true = action was performed, else something went wrong 
 */
bool action_take_object1(void);

/*! \brief Print inventory
 *
 * \return true = action was performed, else something went wrong 
 */
bool action_inventory(void);

/*! \brief Drop an object
 *
 * Drops the object if it is carried, if not an error is printed. 
 *
 * \return true = action was performed, else something went wrong 
 */
bool action_drop_object1(void);

/*! \brief Default high priority event
 *
 * The default high priority event is to print a long description if
 * the room is first visited and a short description else.
 *
 * \return true = action was performed, else something went wrong 
 */
bool action_default_high_pri_event(void);
#endif
