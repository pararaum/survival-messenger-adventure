#include "data_structures.h"
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#define SAVEGAMEHEADER "\1\4GD"

struct AdventureSaveLoad {
  char header[4];
  uint8_t here; //!< position of player
  uint8_t verb; //!< current verb
  uint8_t adverb; //!< current adverb
  uint8_t obj1; //!< first object or PO_UNKNOWN
  uint8_t adj1; //!< first adjective or PO_UNKNOWN
  uint8_t obj2; //!< second object or PO_UNKNOWN
  uint8_t adj2; //!< second adjective or PO_UNKNOWN
  uint8_t markers[256 / 8]; //!< Storage area for markers.
  uint8_t markers_room_visited[256 / 8]; //!< Storage area for visited locations.
  uint8_t counters[256]; //!< Storage area for the counters.
  uint8_t object_positions[256]; // !< Storage are for the current object positions.
};


unsigned int get_size_save_data(void) {
  return sizeof(struct AdventureSaveLoad);
}


void *generate_save_data(void *_ptr, unsigned int *size) {
  unsigned int i;
  struct AdventureSaveLoad *ptr = _ptr;

  *size = sizeof(struct AdventureSaveLoad);
  if(ptr == NULL) {
    ptr = calloc(sizeof(struct AdventureSaveLoad), 1);
  }
  if(ptr != NULL) {
    strncpy(&ptr->header[0], SAVEGAMEHEADER, 4);
    ptr->here = here;
    ptr->verb = verb;
    ptr->adverb = adverb;
    ptr->obj1 = obj1;
    ptr->adj1 = adj1;
    ptr->obj2 = obj2;
    ptr->adj2 = adj2;
    memcpy(&ptr->markers[0], &markers[0], sizeof(markers));
    memcpy(&ptr->markers_room_visited[0], &markers_room_visited[0], sizeof(markers_room_visited));
    memcpy(&ptr->counters[0], &counters[0], sizeof(counters));
    for(i = 0; i < objects_N; ++i) {
      ptr->object_positions[i] = objects[i].pos;
    }
  }
  return ptr;
}


bool interpret_save_data(void *data) {
  struct AdventureSaveLoad *ptr = data;
  uint8_t i;

  if(strncmp(ptr->header, SAVEGAMEHEADER, 4) != 0) {
    fprintf(stderr, "Game-data header is broken!\n");
    return false;
  }
  here = ptr->here;
  verb = ptr->verb;
  adverb = ptr->adverb;
  obj1 = ptr->obj1;
  adj1 = ptr->adj1;
  obj2 = ptr->obj2;
  adj2 = ptr->adj2;
  memcpy(&markers[0], &ptr->markers[0], sizeof(markers));
  memcpy(&markers_room_visited[0], &ptr->markers_room_visited[0], sizeof(markers_room_visited));
  memcpy(&counters[0], &ptr->counters[0], sizeof(counters));
  for(i = 0; i < objects_N; ++i) {
    objects[i].pos = ptr->object_positions[i];
  }
  return true;
}
