#include <assert.h>
#include "data_structures.h"

#pragma code-name (push, "LOWCODE")

uint8_t c64_get_image_no(uint8_t loc) {
  assert(loc < locations_N);
  return locations[loc].image;
};
