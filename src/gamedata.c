#include <stddef.h>
#include <assert.h>
#include "data_structures.h"
#include "default_actions.h"
#include "general_io.h"
#include "qcode.h"
#ifdef __C64__
/* We need this include for special actions which happen when the
** mobile phone is used. This is only available in the C64
** version. The command line version does not have this feature.
*/
#include "c64_mobilemoiety.h"
#endif

uint8_t here = 3; // Starting room.

#ifdef __C64__
#pragma rodata-name (push, "HIGHMEM")
#endif
const Verb_t verbs[] = {
  { "east", 0 },
  { "e", 0 },
  { "north", 1 },
  { "n", 1 },
  { "west", 2 },
  { "w", 2 },
  { "south", 3 },
  { "s", 3 },
  { "up", 4 },
  { "down", 5 },
  { "get", 6 },
  { "take", 6 },
  { "examine", 7 },
  { "look", 7 },
  { "exits", 8 },
  { "inventory", 9 },
  { "inv", 9 },
  { "drop", 10 },
  { "climb", 11 },
  { "turn", 12 },
  { "go", 13 },
  { "enter", 13 },
  { "talk", 14 },
  { "use", 15 },
  { "give", 16 },
  { "knock", 17 },
  // too generic?  { "repair", 18 },
  { "chew", 19 },
  { "open", 20 },
  { "close", 21 },
  { "eat", 22 },
  { "read", 23 },
  { "swim", 24 },
  { "put", 25 },
  { "pull", 26 },
  { "save", 27 },
  { "load", 28 },
  { "rub", 29 },
  { "quit", 100 },
  { "debug", 254 }
};

const Adjective_t adjectives[] = {
  { "medical", 0 },
  { "med", 0	 },
  { "silver", 1	 },
  { "old", 2	 },
  { "on", 3	 },
  { "off", 4	 },
  { "lit", 5	 },
  { "careful", 6 },
  { "carefully", 6 },
  { "used", 7	 },
  { "oak", 8	 },
  { "leaky", 9   },
  { "cherry", 10 },
  { "maple", 11  },
  { "blue", 12	},
  { "calm", 13	},
  { "tranquil", 14 },
  { "oily", 15 },
  { "greasy", 16 },
  { "dead", 17 }
};


const Ignorable_t ignorables[] = {
  { "in" },
  { "the" },
  { "to" },
  { "with" },
  { "into" },
  { NULL }
};
#ifdef __C64__
#pragma rodata-name (pop)
#endif



bool action_battery_in_mobile(void) {
  if(!qcode_object_available(0)) {
    gio_putstrln("You do not have a phone!");
  } else if(!qcode_object_available(10)) {
    gio_putstrln("You do not have a battery!");
  } else {
    qcode_message(14);
    qcode_bring_object(10, OBJ_NOROOM);
    qcode_exchange_object(0, 11);
#ifdef __C64__
    set_marker(MOBILEPHONEMARKER, 1);
#endif
  }
  return true;
}


// EVENTS ------------------------------------------------------------------
const Event_t events[] = {
  // ⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖
  // First of all, the local low-priority events:
  // ⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖
  // Turn lamp on (only if in cave).
  { 27, 12, EVENT_NOTP, 6, EVENT_NOTP, EVENT_NOTP, 3, NULL, "O1 Oc D ( 9 m f ) y~ 6 7 x 33 m f" },
  // Turn on lamp (only if in cave).
  { 27, 12, EVENT_NOTP, 6, 3, EVENT_NOTP, EVENT_NOTP, NULL, "O1 Oc D ( 9 m f ) y~ 6 7 x 33 m f" },
  // If in the cave and it is dark always print that it is too dark! But the verbs quit, load are allowed.
  { 27, EVENT_ANY, EVENT_MAYBE, EVENT_MAYBE, EVENT_MAYBE, EVENT_MAYBE, EVENT_MAYBE, NULL, "V 28 = V 100 = 7 Oc | | ( \'It_is_too_dark!_I_can_not_do_anything! f ) y~" },
  // ⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖
  // Normal events:
  // ⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖
  // climb carefully mountain
  { EVENT_ANYROOM, 11, EVENT_NOTP, 13, EVENT_NOTP, EVENT_NOTP, 6, NULL, "O1 Oa ( 20 m f ) y~ 33 h f" },
  { EVENT_ANYROOM, 11, EVENT_NOTP, 13, 6, EVENT_NOTP, EVENT_NOTP, NULL, "O1 Oa ( 20 m f ) y~ 33 h f" },
  { EVENT_ANYROOM, 11, 6, 13, EVENT_NOTP, EVENT_NOTP, EVENT_NOTP, NULL, "O1 Oa ( 20 m f ) y~ 33 h f" },
  // climb mountain
  { EVENT_ANYROOM, 11, EVENT_NOTP, 13, EVENT_NOTP, EVENT_NOTP, EVENT_NOTP, NULL, "O1 Oa ( 20 m f ) y~ 21 m f" },
  // Enter hut.
  { EVENT_ANYROOM, 13, EVENT_NOTP, 12, EVENT_NOTP, EVENT_NOTP, EVENT_NOTP, NULL, "31 H = ( 32 h f ) y? 19 m f" },
  // Use battery in phone.
  { EVENT_ANYROOM, 15, EVENT_NOTP, 10, EVENT_NOTP, 0, EVENT_NOTP, &action_battery_in_mobile, NULL },
  // Use phone with battery.
  { EVENT_ANYROOM, 15, EVENT_NOTP, 0, EVENT_NOTP, 10, EVENT_NOTP, &action_battery_in_mobile, NULL },
  // Put battery into phone.
  { EVENT_ANYROOM, 25, EVENT_NOTP, 10, EVENT_NOTP, 0, EVENT_NOTP, &action_battery_in_mobile, NULL },
  // Use battery in phone (with battery).
  { EVENT_ANYROOM, 15, EVENT_NOTP, 10, EVENT_NOTP, 11, EVENT_NOTP, NULL, "16 m f" },
  // Examine sand=9: finding battery.
  { EVENT_ANYROOM, 7, EVENT_NOTP, 9, EVENT_NOTP, EVENT_NOTP, EVENT_NOTP, NULL, "0 S ( 1 0 s 254 10 b 13 m f ) y~ 18 m f" },
  // examine door at the lighthouse
  { EVENT_ANYROOM, 7, EVENT_NOTP, 47, EVENT_NOTP, EVENT_NOTP, EVENT_NOTP, NULL, "O1 Oa ( O1 do 5 S ( \'Somebody_left_the_door_unlocked. f ) y? \'The_door_is_locked. f ) y?" },
  // #### First solution?
  // Examine sand=35: finding woodplanks.
  { EVENT_ANYROOM, 7, EVENT_NOTP, 35, EVENT_NOTP, EVENT_NOTP, EVENT_NOTP, NULL, "\'While_playing_in_the_sand_you_find_some_woodplanks. 35 36 x H 21 b f" },
  // Handle climbing: if the palm is here print that you failed to climb.
  { EVENT_ANYROOM, 11, EVENT_NOTP, 4, EVENT_NOTP, EVENT_NOTP, EVENT_NOTP, NULL, "H O1 Op = ( 6 m f ) y~ 7 m f" },
  // Use the same for the tree. (climb tree)
  { EVENT_ANYROOM, 11, EVENT_NOTP, 5, EVENT_NOTP, EVENT_NOTP, EVENT_NOTP, NULL, "H O1 Op = ( 6 m f ) y~ 7 m f" },
  // use woodplanks rowboat verb=15,adverb=255,obj1=21,adj1=255,obj2=15,adj2=3
  { EVENT_ANYROOM, 15, EVENT_NOTP, 21, EVENT_NOTP, 37, EVENT_NOTP, NULL, "21 Oc ( \'You_do_not_have_the_woodplanks. f ) y~ 37 Oc ( \'You_do_not_have_the_rowboat. f ) y~ \'The_woodplanks_fit_barely_into_the_holes._Now,_I_need_some_sealant. 37 38 x 255 21 b f" },
  // use bubblegum (unchewed) with leaky rowboat (w/o woodplanks)
  { EVENT_ANYROOM, 15, EVENT_NOTP, 22, EVENT_NOTP, 37, EVENT_MAYBE, NULL, "O1 Oc ( 28 m w O1 dn p. f ) y~ O2 Oc ( 28 m w O2 dn p. f ) y~ \'The_bubblegum_brick_is_of_no_use._Perhaps_I_could_chew_it? f" },
  // use bubblegum (unchewed) with leaky rowboat (w woodplanks)
  { EVENT_ANYROOM, 15, EVENT_NOTP, 22, EVENT_NOTP, 38, EVENT_MAYBE, NULL, "O1 Oc ( 28 m w O1 dn p. f ) y~ O2 Oc ( 28 m w O2 dn p. f ) y~ \'The_bubblegum_brick_is_of_no_use._Perhaps_I_could_chew_it? f" },
  // use bubblegum with rowboat (the one without woodplanks)
  { EVENT_ANYROOM, 15, EVENT_NOTP, 23, EVENT_NOTP, 37, EVENT_MAYBE, NULL, "23 Oc ( \'Which bubblegum? f ) y~ O2 Oc ( 37 m f ) y~ \'The_holes_are_too_big_for_the_sealant. f" },
  // use bubblegum with rowboat (the one with woodplanks)
  { EVENT_ANYROOM, 15, EVENT_NOTP, 23, EVENT_NOTP, 38, EVENT_MAYBE, NULL, "23 Oc ( \'Which bubblegum? f ) y~ O2 Oc ( 37 m f ) y~ 38 39 x 255 23 b f" },
  // knock door
  { EVENT_ANYROOM, 17, EVENT_NOTP, 19, EVENT_NOTP, EVENT_NOTP, EVENT_NOTP, NULL, "1 C 0 = ( 1 1 c 40 m f ) y? 1 C 1 = ( 2 1 c 41 m f ) y? 1 C 2 = ( 3 1 c \'Go_away! f ) y? 1 C 3 = ( 4 1 c 42 m 22 H b f ) y? 43 m f" },
  // knock maple door
  { EVENT_ANYROOM, 17, EVENT_NOTP, 19, 11, EVENT_NOTP, EVENT_NOTP, NULL, "1 C 0 = ( 1 1 c 40 m f ) y? 1 C 1 = ( 2 1 c 41 m f ) y? 1 C 2 = ( 3 1 c \'Go_away! f ) y? 1 C 3 = ( 4 1 c 42 m 22 H b f ) y? 43 m f" },
  // chew bubblegum
  { EVENT_ANYROOM, 19, EVENT_NOTP, 22, EVENT_NOTP, EVENT_NOTP, EVENT_NOTP, NULL, "O1 Oa ( \'You_have_no_bubblegum! f ) y~ 22 23 x 46 m f" },
  // chew used bublegum.
  { EVENT_ANYROOM, 19, EVENT_NOTP, 23, EVENT_NOTP, EVENT_NOTP, EVENT_NOTP, NULL, "\'Errr..._The_bubblegum_was_already_chewed. f" },
  // use leaky boat with river
  { EVENT_ANYROOM, 15, EVENT_NOTP, 37, EVENT_NOTP, 40, EVENT_NOTP, NULL, "40 Oa ( \'There_is_no_river_here. f ) y~ O1 Oc ( 37 m f ) y~ \'It_would_be_prudent_to_fix_the_rowboat_first. f" },
  { EVENT_ANYROOM, 15, EVENT_NOTP, 38, EVENT_NOTP, 40, EVENT_NOTP, NULL, "40 Oa ( \'There_is_no_river_here. f ) y~ O1 Oc ( 37 m f ) y~ \'It_would_be_prudent_to_fix_the_rowboat_first. f" },
  // use rowboat with rivers (here=25)
  { EVENT_ANYROOM, 15, EVENT_NOTP, 39, EVENT_NOTP, 40, EVENT_NOTP, NULL, "40 Oa ( \'There_is_no_river_here. f ) y~ O1 Oc ( 37 m f ) y~ \'The_river_drags_you_into_the_sea._There,_you_are_rescued. 1 255 s 50 # f" },
  // use rivers with rowboat
  { EVENT_ANYROOM, 15, EVENT_NOTP, 40, EVENT_NOTP, 39, EVENT_NOTP, NULL, "40 Oa ( \'There_is_no_river_here. f ) y~ O1 Oc ( 37 m f ) y~ \'The_river_drags_you_into_the_sea._There,_you_are_rescued. 1 255 s 50 # f" },
  // ⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖
  // Leave the island via the lighthouse.
  // Look sea if marker 1 is reset, room=0, find bat. Sets marker 1.
  { 0, 7, EVENT_NOTP, 44, EVENT_NOTP, EVENT_NOTP, EVENT_NOTP, NULL, "1 S ( \'Something_long_and_made_out_of_wood_is_in_the_sea. 1 1 s H 25 b f ) y~" },
  // use woodplanks with bridge
  { 19, 15, EVENT_NOTP, 21, EVENT_NOTP, 28, EVENT_NOTP, NULL, "21 Oc ( \'I_do_not_have_woodplanks_with_me! f ) y~ 255 21 b 1 2 s f" },
  // examine bridge (when it has been fixed)
  { 19, 7, EVENT_NOTP, 28, EVENT_NOTP, EVENT_NOTP, EVENT_NOTP, NULL, "2 S ( 34 m f ) y?" },
  // hint: open/knock cherry door, then the girl talks to you.
  { EVENT_ANYROOM, 20, EVENT_NOTP, 20, EVENT_NOTP, EVENT_NOTP, EVENT_NOTP, NULL, "29 m f" },
  { EVENT_ANYROOM, 20, EVENT_NOTP, 20, 10, EVENT_NOTP, EVENT_NOTP, NULL, "29 m f" },
  { EVENT_ANYROOM, 17, EVENT_NOTP, 20, EVENT_NOTP, EVENT_NOTP, EVENT_NOTP, NULL, "29 m f" },
  { EVENT_ANYROOM, 17, EVENT_NOTP, 20, 10, EVENT_NOTP, EVENT_NOTP, NULL, "29 m f" },
  // use coconut with bat OR use bat with coconut
  { EVENT_ANYROOM, 15, EVENT_NOTP, 26, EVENT_NOTP, 25, EVENT_NOTP, NULL, "O1 Oa O2 Oa & ( 22 m f ) y~ 23 m 26 27 x f" },
  { EVENT_ANYROOM, 15, EVENT_NOTP, 25, EVENT_NOTP, 26, EVENT_NOTP, NULL, "O1 Oa O2 Oa & ( 22 m f ) y~ 23 m 26 27 x f" },
  // open coconut, if both items are available
  { EVENT_ANYROOM, 20, EVENT_NOTP, 26, EVENT_NOTP, EVENT_NOTP, EVENT_NOTP, NULL, "O1 Oa 25 Oa & ( 22 m f ) y~ 23 m 26 27 x f" },
  // give (closed) coconut to (old) man
  { EVENT_ANYROOM, 16, EVENT_NOTP, 26, EVENT_NOTP, 8, EVENT_NOTP, NULL, "O1 Oc ( 24 m f ) y~ O2 Oa ( \'Which_old_man? f ) y~ \'The_old_man_does_not_have_teeth_which_are_good_enough_to_crack_open_a_coconut. f" },
  { EVENT_ANYROOM, 16, EVENT_NOTP, 26, EVENT_NOTP, 8, 2, NULL, "O1 Oc ( 24 m f ) y~ O2 Oa ( \'Which_old_man? f ) y~ \'The_old_man_does_not_have_teeth_which_are_good_enough_to_crack_open_a_coconut. f" },
  // give open coconut to (old) man
  { EVENT_ANYROOM, 16, EVENT_NOTP, 27, EVENT_NOTP, 8, EVENT_NOTP, NULL, "O1 Oc ( 24 m f ) y~ O2 Oa ( \'Which_old_man? f ) y~ 255 27 b 254 45 b \'The_old_man_gives_you_a_key. f" },
  { EVENT_ANYROOM, 16, EVENT_NOTP, 27, EVENT_NOTP, 8, 2, NULL, "O1 Oc ( 24 m f ) y~ O2 Oa ( \'Which_old_man? f ) y~ 255 27 b 254 45 b \'The_old_man_gives_you_a_key. f" },
  // give banana to monkey, get the screwdriver
  { EVENT_ANYROOM, 16, EVENT_NOTP, 30, EVENT_NOTP, 31, EVENT_NOTP, NULL, "O1 Oc O2 Oa & ( \'The_monkey_takes_the_banana_and_runs_away!_A_classic_action. H 32 b 255 30 b 255 31 b f ) y?" },
  // examine trashcan
  { EVENT_ANYROOM, 7, EVENT_NOTP, 33, EVENT_NOTP, EVENT_NOTP, EVENT_NOTP, NULL, "O1 Oa 3 S ! & ( \'When_opening_the_trashcan_something_fell_down. H 46 b 1 3 s f ) y?" },
  // read book
  { EVENT_ANYROOM, 23, EVENT_NOTP, 46, EVENT_NOTP, EVENT_NOTP, EVENT_NOTP, NULL, "O1 Oc ( \'First_of_all_I_should_have_a_book. f ) y~ 35 m 1 4 s f" },
  // open door (at the lighthouse)
  { 30, 20, EVENT_NOTP, 47, EVENT_NOTP, EVENT_NOTP, EVENT_NOTP, NULL, "45 Oc ( \'I_need_a_key! f ) y~ 255 45 b 1 5 s \'The_door_opens_but_the_key_is_stuck_in_the_door. f" },
  // use key door (at the lighthouse)
  { 30, 15, EVENT_NOTP, 45, EVENT_NOTP, 47, EVENT_NOTP, NULL, "45 Oc ( \'I_need_a_key! f ) y~ 255 45 b 1 5 s \'The_door_opens_but_the_key_is_stuck_in_the_door. f" },
  // enter lighthouse
  { 30, 13, EVENT_NOTP, 29, EVENT_NOTP, EVENT_NOTP, EVENT_NOTP, NULL, "5 S ( \'The_door_is_still_locked. f ) y~ 45 h f" },
  // use door
  { 30, 15, EVENT_NOTP, 47, EVENT_NOTP, EVENT_NOTP, EVENT_NOTP, NULL, "5 S ( \'The_door_is_still_locked. f ) y~ 45 h f" },
  // up in the lighthouse store-room.
  { 45, 4, EVENT_NOTP, EVENT_NOTP, EVENT_NOTP, EVENT_NOTP, EVENT_NOTP, NULL, "46 h f" },
  // use stairs (in store room).
  { 45, 15, EVENT_NOTP, 48, EVENT_NOTP, EVENT_NOTP, EVENT_NOTP, NULL, "46 h f" },
  // go stairs (in store room).
  { 45, 13, EVENT_NOTP, 48, EVENT_NOTP, EVENT_NOTP, EVENT_NOTP, NULL, "46 h f" },
  // down in the lighthouse lightroom.
  { 46, 5, EVENT_NOTP, EVENT_NOTP, EVENT_NOTP, EVENT_NOTP, EVENT_NOTP, NULL, "45 h f" },
  // use stairs (in lightrooom).
  { 46, 15, EVENT_NOTP, 49, EVENT_NOTP, EVENT_NOTP, EVENT_NOTP, NULL, "45 h f" },
  // go stairs (in lightrooom).
  { 46, 13, EVENT_NOTP, 49, EVENT_NOTP, EVENT_NOTP, EVENT_NOTP, NULL, "45 h f" },
  // use screwdriver with switchbox
  /*
   * Check if marker 4 is set, if not print the message about reading the book.
   * Is object 51 in the "no room" room (aka destroyed)? Print a message about destroying things.
   */
  { 45, 15, EVENT_NOTP, 32, EVENT_NOTP, 34, EVENT_NOTP, NULL, "4 S ( 50 m f ) y~ 51 Op 255 = ( 54 m f ) y? 51 m 1 255 s 54 # f" },
  // ⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖
  // Leave the island via the bonfire.
  // Open lenses
  { 46, 20, EVENT_NOTP, 51, EVENT_NOTP, EVENT_NOTP, EVENT_NOTP, NULL, "53 m 51 52 x f" },
  // Knock on the door of Greasy Mat.
  { 21, 17, EVENT_NOTP, 18, EVENT_NOTP, EVENT_NOTP, EVENT_NOTP, NULL, "59 Oa ( 47 m f ) y? 48 m H 59 b f" },
  { 21, 20, EVENT_NOTP, 18, EVENT_NOTP, EVENT_NOTP, EVENT_NOTP, NULL, "59 Oa ( 47 m f ) y? 48 m H 59 b f" },
  // talk to mat or talk to greasy mat (Greasy Mat)
  { 21, 14, EVENT_NOTP, 59, EVENT_NOTP, EVENT_NOTP, EVENT_NOTP, NULL, "6 S ( \'I_told_you:_Bring_a_container! f ) y? 1 6 s 26 m f" },
  { 21, 14, EVENT_NOTP, 59, 16, EVENT_NOTP, EVENT_NOTP, NULL, "6 S ( \'I_told_you:_Bring_a_container! f ) y? 1 6 s 26 m f" },
  // give can to mat (Greasy Mat)
  { 21, 16, EVENT_NOTP, 57, EVENT_NOTP, 59, EVENT_NOTP, NULL, "255 59 b 57 58 x \'That_will_do,_here_is_my_finest_self-distilled_oil. f" },
  { 21, 16, EVENT_NOTP, 57, EVENT_NOTP, 59, 16, NULL, "255 59 b 57 58 x \'That_will_do,_here_is_my_finest_self-distilled_oil. f" },
  // give (filled) can to mat (Greasy Mat)
  { 21, 16, EVENT_NOTP, 58, EVENT_NOTP, 59, EVENT_NOTP, NULL, "\'Greasy_Mat_refuses_to_take_his_refuse_back. f" },
  { 21, 16, EVENT_NOTP, 58, EVENT_NOTP, 59, 16, NULL, "\'Greasy_Mat_refuses_to_take_his_refuse_back. f" },
  // use saw bush; here=16,verb=15,adverb=255,obj1=53,adj1=255,obj2=54,adj2=3
  { 16, 15, EVENT_NOTP, 53, EVENT_NOTP, 54, EVENT_NOTP, NULL, "53 Oc 54 Oa & ( \'Wow,_this_is_a_really_cool_saw._Easy_to_get_some_twigs_off. H 56 b 54 55 x f ) y?" },
  // go stones (on mountain top)
  { 33, 13, EVENT_NOTP, 60, EVENT_NOTP, EVENT_NOTP, EVENT_NOTP, NULL, "\'Standing_inside_the_circle_feels_really_weird._I_better_stand_outside. f" },
  // drop twigs (on mountain top)
  { 33, 10, EVENT_NOTP, 56, EVENT_NOTP, EVENT_NOTP, EVENT_NOTP, NULL, "\'Probably_it_is_best_to_put_them_inside_the_circle. H 56 b f" },
  // use twigs stones
  { 33, 15, EVENT_NOTP, 56, EVENT_NOTP, 60, EVENT_NOTP, NULL, "H 56 b f" },
  // use stones twigs
  { 33, 15, EVENT_NOTP, 60, EVENT_NOTP, 56, EVENT_NOTP, NULL, "H 56 b f" },
  // use can  twigs
  { 33, 15, EVENT_NOTP, 58, EVENT_NOTP, 56, EVENT_NOTP, NULL, "56 Oc ( \'I_carry_the_twigs,_should_I_pour_the_oil_over_myself? f ) y? 56 Oa ( \'There_are_no_twigs_here. ) y~ 56 61 x 255 58 b f" },
  { 33, 6, EVENT_NOTP, 61, EVENT_NOTP, EVENT_NOTP, EVENT_NOTP, NULL, "\'I_am_not_touching_that! f" },
  // use lens oily twigs
  { 33, 15, EVENT_NOTP, 52, EVENT_NOTP, 61, EVENT_NOTP, NULL, "52 Oc ( \'I_do_not_have_a_lens. f ) y~ 61 Oa ( \'Which_twigs? f ) y~ 27 m 1 255 s 53 # f" },
  { 33, 15, EVENT_NOTP, 52, EVENT_NOTP, 61, 15, NULL, "52 Oc ( \'I_do_not_have_a_lens. f ) y~ 61 Oa ( \'Which_twigs? f ) y~ 27 m 1 255 s 53 # f" },
  // ⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖
  // enter hole (only in room 26)
  { 26, 13, EVENT_NOTP, 15, EVENT_NOTP, EVENT_NOTP, EVENT_NOTP, NULL, "\'You_climb_down_the_rabbits_hole. 27 h f" },
  { // take
    EVENT_ANYROOM, 6, EVENT_NOTP, EVENT_NOTP, EVENT_NOTP, EVENT_NOTP, EVENT_NOTP, NULL, "4 m" },
  // Take sand.
  { EVENT_ANYROOM, 6, EVENT_NOTP, 9, EVENT_NOTP, EVENT_NOTP, EVENT_NOTP, NULL, "15 m f" },
  // ⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖
  // Hints
  // ⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖
  // ⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖
  // Local (catch all) events.
  // ⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖
  { 19, 3, EVENT_NOTP, EVENT_NOTP, EVENT_NOTP, EVENT_NOTP, EVENT_NOTP, NULL, "2 S ( \'I_am_not_going_onto_that_broken_thing._Fix_it_first! f ) y~" },
  // take trashcan in room=20.
  { 20, 6, EVENT_NOTP, 33, EVENT_NOTP, EVENT_NOTP, EVENT_NOTP, NULL, "38 m f" },
  // room=25: swim river
  { 25, 24, EVENT_NOTP, 40, EVENT_NOTP, EVENT_NOTP, EVENT_NOTP, NULL, "31 m f" },
  // room=42: swim lake
  { 42, 24, EVENT_NOTP, 62, EVENT_NOTP, EVENT_NOTP, EVENT_NOTP, NULL, "45 m f" },
  // room=32: use can with lake
  { 42, 15, EVENT_NOTP, 57, EVENT_NOTP, 62, EVENT_NOTP, NULL, "\'I_tried_to_fill_the_can_but_an_olm_jumped_at_me_so_I_spilled_all_the_water. f" },
  // room=20: knock oak door
  { 20, 17, EVENT_NOTP, 17, EVENT_MAYBE, EVENT_NOTP, EVENT_NOTP, NULL, "52 m f" },
  // climb dead tree, room=17
  { 17, 11, EVENT_NOTP, 64, EVENT_MAYBE, EVENT_NOTP, EVENT_NOTP, NULL, "7 m f" },
  // ⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖
  // Catch all events.
  // ⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖⁖
  // put mask on
  { EVENT_ANYROOM, 25, EVENT_NOTP, 3, EVENT_NOTP, EVENT_NOTP, 3, NULL, "O1 Oc ( 9 m f ) y~  44 m f" },
  // use mask
  { EVENT_ANYROOM, 15, EVENT_NOTP, 3, EVENT_NOTP, EVENT_NOTP, EVENT_NOTP, NULL, "O1 Oc ( 9 m f ) y~  44 m f" },
  // swim sea
  { EVENT_ANYROOM, 24, EVENT_NOTP, 24, EVENT_NOTP, EVENT_NOTP, EVENT_NOTP, NULL, "32 m f" },
  { EVENT_ANYROOM, 24, EVENT_NOTP, 41, EVENT_MAYBE, EVENT_NOTP, EVENT_NOTP, NULL, "32 m f" },
  { EVENT_ANYROOM, 24, EVENT_NOTP, 42, EVENT_MAYBE, EVENT_NOTP, EVENT_NOTP, NULL, "\'I_am_blue,_da_ba_dee... f" },
  { EVENT_ANYROOM, 24, EVENT_NOTP, 43, EVENT_MAYBE, EVENT_NOTP, EVENT_NOTP, NULL, "32 m f" },
  { EVENT_ANYROOM, 24, EVENT_NOTP, 44, EVENT_NOTP, EVENT_NOTP, EVENT_NOTP, NULL, "32 m f" },
  // swim <obj1>
  { EVENT_ANYROOM, 24, EVENT_NOTP, EVENT_ANY, EVENT_NOTP, EVENT_NOTP, EVENT_NOTP, NULL, "\'I_throw_myself_on_the_ground_and_make_swim_movements. f" },
  // Take <obj1>.
  { EVENT_ANYROOM, 6, EVENT_NOTP, EVENT_ANY, EVENT_MAYBE, EVENT_NOTP, EVENT_NOTP, &action_take_object1, NULL },
  // examine kit (obj=2)
  { EVENT_ANYROOM, 7, EVENT_NOTP, 2, EVENT_NOTP, EVENT_NOTP, EVENT_NOTP, NULL, "O1 Oa ( O1 do w \'You_find_a_face_mask_in_the_kit! 1 2 x 3 t f ) y? \'What_kit? f" },
  // open medical kit (obj=2)
  { EVENT_ANYROOM, 20, EVENT_NOTP, 2, 0, EVENT_NOTP, EVENT_NOTP, NULL, "O1 Oa ( O1 do w \'You_find_a_face_mask_in_the_kit! 1 2 x 3 t f ) y? \'What_kit? f" },
  // open kit (obj=2)
  { EVENT_ANYROOM, 20, EVENT_NOTP, 2, EVENT_NOTP, EVENT_NOTP, EVENT_NOTP, NULL, "O1 Oa ( O1 do w \'You_find_a_face_mask_in_the_kit! 1 2 x 3 t f ) y? \'What_kit? f" },
  // open kit (obj=1) (this is the examine kit)
  { EVENT_ANYROOM, 20, EVENT_NOTP, 1, EVENT_NOTP, EVENT_NOTP, EVENT_NOTP, NULL, "O1 Oa ( 9 m f ) y~ 39 m 255 1 b f" },
  // examine sea
  { EVENT_ANYROOM, 7, EVENT_NOTP, 24, EVENT_NOTP, EVENT_NOTP, EVENT_NOTP, NULL, "O1 do f" },
  // examine <obj1>
  { EVENT_ANYROOM, 7, EVENT_NOTP, EVENT_ANY, EVENT_NOTP, EVENT_NOTP, EVENT_NOTP, NULL, "O1 Oa ( O1 do f ) y? 8 m f" },
  // examine <adj1> <obj1>
  { EVENT_ANYROOM, 7, EVENT_NOTP, EVENT_ANY, EVENT_ANY, /* adj1 is present */ EVENT_NOTP, EVENT_NOTP, NULL, "O1 Oa O1 Ao A1 = & ( O1 do f ) y? 8 m f" /* Is the first object available and is adj1 equal to obj1's adjective then describe object one. */ },
  // examine OR look
  { EVENT_ANYROOM, 7, EVENT_NOTP, EVENT_NOTP, EVENT_NOTP, EVENT_NOTP, EVENT_NOTP, NULL, "H D dr n v f" },
  // exits
  { EVENT_ANYROOM, 8, EVENT_NOTP, EVENT_NOTP, EVENT_NOTP, EVENT_NOTP, EVENT_NOTP, NULL, "H e f" },
  { EVENT_ANYROOM, 9, EVENT_NOTP, EVENT_NOTP, EVENT_NOTP, EVENT_NOTP, EVENT_NOTP, &action_inventory, NULL },
  // drop phone
  { EVENT_ANYROOM, 10, EVENT_NOTP, 11, EVENT_NOTP, EVENT_NOTP, EVENT_NOTP, NULL, "17 m f" },
  // drop <obj1>
  { EVENT_ANYROOM, 10, EVENT_NOTP, EVENT_ANY, EVENT_MAYBE, EVENT_NOTP, EVENT_NOTP, &action_drop_object1, NULL },
  // rub lamp (as a joke).
  { EVENT_ANYROOM, 29, EVENT_NOTP, 6, EVENT_NOTP, EVENT_NOTP, EVENT_NOTP, NULL, "O1 Oc ( 9 m f ) y~ 49 m f" },
  {
    EVENT_ANYROOM,
    100,
    EVENT_NOTP,
    EVENT_NOTP,
    EVENT_NOTP,
    EVENT_NOTP,
    EVENT_NOTP,
    NULL,
    "1 255 s f"
  },
  {
    EVENT_ANYROOM,
    100,
    EVENT_NOTP,
    EVENT_ANY,
    EVENT_NOTP,
    EVENT_NOTP,
    EVENT_NOTP,
    NULL,
    "5 m n f"
  },
  // Turn lamp on.
  { EVENT_ANYROOM, 12, EVENT_NOTP, 6, 3, EVENT_NOTP, EVENT_NOTP, NULL, "O1 Oc ( 9 m f ) y~ 6 7 x f" },
  // Turn on lamp.
  { EVENT_ANYROOM, 12, EVENT_NOTP, 6, EVENT_NOTP, EVENT_NOTP, 3, NULL, "O1 Oc ( 9 m f ) y~ 6 7 x f" },
  // use lamp.
  { EVENT_ANYROOM, 15, EVENT_NOTP, 7, EVENT_NOTP, EVENT_NOTP, EVENT_NOTP, NULL, "O1 Oc ( 9 m f ) y~ 6 7 x f" },
  // Turn (lit) lamp on.
  { EVENT_ANYROOM, 12, EVENT_NOTP, 7, 3, EVENT_NOTP, EVENT_NOTP, NULL, "30 m f" },
  // Turn on (lit) lamp.
  { EVENT_ANYROOM, 12, EVENT_NOTP, 7, EVENT_NOTP, EVENT_NOTP, 3, NULL, "30 m f" },
  // Talk man.
  { EVENT_ANYROOM, 14, EVENT_NOTP, 8, EVENT_NOTP, EVENT_NOTP, EVENT_NOTP, NULL, "0 C 0 = ( 1 0 c 10 m f ) y? 0 C 1 + D 0 c 4 > ( 11 m f ) y? 12 m 1 255 s 51 # f" },
  // Talk old man.
  { EVENT_ANYROOM, 14, EVENT_NOTP, 8, 2, EVENT_NOTP, EVENT_NOTP, NULL, "0 C 0 = ( 1 0 c 10 m f ) y? 0 C 1 + D 0 c 4 > ( 11 m f ) y? 12 m 1 255 s 51 # f" },
  // talk to monkey
  { 16, 14, EVENT_NOTP, 31, EVENT_NOTP, EVENT_NOTP, EVENT_NOTP, NULL, "O1 Oa \'Not_a_meaningful_conversation. 30 Oa ( \'But_it_is_pointing_at_the_banana. ) y? f" },
  { EVENT_ANYROOM, 14, EVENT_NOTP, 31, EVENT_NOTP, EVENT_NOTP, EVENT_NOTP, NULL, "\'You_definitely_need_therapy. f" },
  // save
  { EVENT_ANYROOM, 27, EVENT_NOTP, EVENT_NOTP, EVENT_NOTP, EVENT_NOTP, EVENT_NOTP, &qcode_save, NULL },
  // load
  { EVENT_ANYROOM, 28, EVENT_NOTP, EVENT_NOTP, EVENT_NOTP, EVENT_NOTP, EVENT_NOTP, &qcode_load, NULL },
  // debug
  { EVENT_ANYROOM, 254, EVENT_NOTP, EVENT_NOTP, EVENT_NOTP, EVENT_NOTP, EVENT_NOTP, NULL, "'oh_la_la_la 1 5 s f" }
};


/*
 * Markers
 * =======
 *
 * 0: Battery already found?
 * 1: Examined sea in room=0, makes bat visible.
 * 2: bridge has been fixed?
 * 3: trashcan has been examined?
 * 4: book read?
 * 5: door to the lighthouse opened?
 * 6: talked to Greasy Mat?
 * 200: Has the phone been turned on?
 * 255: Games has ended? 
 */

/*
 * Counters
 * ========
 *
 * 0: How many times has been talked to the grumpy old man?
 * 1: Counts knockings on the door in village 22 (V3).
 * 2: Rounds without mask in village.
 * 3: How many rounds in the dark hole (cave, room=27)?
 *
 */

const char *welcome_text[] = {
  "You wake up with a cramp starting in your left toe and sand in your mouth.",
  "What has happened you can not remember.",
  "Where am I?",
  "Wind brings a salty breeze and you stand up spitting out the sand.",
  "There is an island and nobody else here.",
  "The sun burns on your head and it will not answer your questions.",
  NULL
};


static int8_t game_hpc(void) {
  int8_t ret = 0;
  counterval_t x;

#if !defined(NDEBUG) && defined(__C64__)
  unsigned int size;
  generate_save_data((void*)0xb00, &size);
#endif
  assert(here < locations_N);
  // Now complex HPC.
  switch(here) {
  case 49:
    ret = 2;
    gio_show_image(locations[here].image);
    qcode_describe_long(here);
    set_marker(255, true);
    break;
  case 27: // Are you in the dark cave and have no lit lamp?
    // "H 27 =  7 Oc ! & ( 36 m n f ) y?",
    if(!qcode_object_carried(7)) {
      x = qcode_get_counter(3); // How many times in the dark hole?
      if(x > 6) {
	qcode_set_counter(3, 0);
	gio_putstrln("After stumbling in the dark for some time, you luckily find the exit.");
	here = 26; // Move player to the outside.
	return 2;
      } else {
	qcode_set_counter(3, x + 1);
	gio_show_image(28);
	qcode_message(36);
	gio_nl();
      }
      ret = 1;
    } else {
      gio_show_image(locations[here].image);
      ret = action_default_high_pri_event();
    }
    break;
  default:
    gio_show_image(locations[here].image); // Do not forget to add this line in other local HPC.
    ret = action_default_high_pri_event();
  };
  return ret;
};

action_function_hpc_t high_priority_event_actionfun = &game_hpc;

QCode_t high_priority_event_qcode[] = {
  //  "H 49 = ( H dr 1 255 s 2 ) y?",
  // In one of the villages without mask? Increase counter
  "H 20 = H 21 = H 22 = H 23 = | | | ( 3 Oc ( 2 C 1 + 2 c ) y~ ) y?",
  // Counter two in [2..5]? Then print I feel dizzy.
  "2 C 2 < 2 C 6 > & ( \'I_feel_dizzy. n ) y? ",
  // If the counter is greate than 7 kill the player!
  "2 C 7 < ( 25 m 1 255 s 52 # 2 ) y?",
  NULL
};

unsigned int verbs_N = sizeof(verbs) / sizeof(Verb_t);
unsigned int adjectives_N = sizeof(adjectives) / sizeof(Adjective_t);
unsigned int events_N = sizeof(events) / sizeof(Event_t);
